(* * Begin *)
(* ** package *)

(* 
BeginPackage["l2func`"];
*)
(* ** usage *)

OP::usage="linear operator in Hilbert space";
CIP::usage="complex inner product in Hilbert space";
CNorm::usage="comple norm."
AtR::usage="AtR[r][a]: calculate radial coordinate representation for a";

CST::usage="OP[CST[c]]: multiplication of constant c.";
DD::usage="OP[DD[n]][u]: gives n th derivative of function u";
RM::usage="OP[RM[n]][u]: gives multicative of r^m to function u";

LinearComb::usage="LinearComb[{us, cs}] gives linear combination of us whose coefficients are cs";

ExpectNear::usage="ExpectNear[calc,expect,label, eps] launch a unit test for continuous value";
UnitTestCIP::usage="UnitTestCIP[u1, u2, eps] launch a unit test for complex inner product of u1 and u2."

CNormalized::usage="Normalized[a]: normalized l2 function a";
STO::usage="STO[c,n,z]: slater type determinant";
GTO::usage="GTO[c,n,z]: gauss type orbital";

(* ** begin *)
(* 
Begin["l2func`"];
*)
(* * HilbertSpace *)
(* ** operator *)
OP[a_+b_][f_]:=OP[a][f] + OP[b][f]
OP[a_*b_][f_]:=OP[a][OP[b][f]]

OP[c_*b_][f_]:=c*OP[b][f]/;NumberQ[c]
OP[a_][c_*f_]:=c*OP[a][f]/;NumberQ[c]
OP[a_][f_ + g_]:= OP[a][f] + OP[a][g]

(* ** inner product *)


CIP[0, _] :=0
CIP[a_, 0]:=0

CIP[a_,b_+c_]:=CIP[a,b]+CIP[a,c]
CIP[a_+b_,c_]:=CIP[a,c]+CIP[b,c]
CIP[c_*a_, b_]:= c * CIP[a,b] /; NumberQ[c]
CIP[a_, c_ * b_]:= c * CIP[a,b] /; NumberQ[c]

(* ** linear combination *)

LinearComb[{us_, cs_}] := Apply[Plus, MapThread[
	Function[{u,c}, OP[CST[c]][u]], {us, cs}]]

(* ** inner product test*)

machineEps:=10^(-MachinePrecision)
ExpectNear[calc_, expe_, label_, 
	       eps_:10* machineEps]:=Block[
  {success, msg},
  
  success = Abs[calc-expe]<eps;
  
  If[success===True,
     Print["[ SUCCESS ] " <> label],
     Print["[ FAILED  ] " <> label];
     Print["+calc: "<>ToString[calc]];
     Print["+expe: "<>ToString[expe]];
     Print["+-----------------:"];]]

UnitTestCIP[u1_, u2_, label_, eps_:10*MachinePrecision]:=Block[
   {calc, expe, r},
   calc = CIP[u1, u2];
   expe = Integrate[AtR[r][u1]*AtR[r][u2], {r,0,Infinity}];
   ExpectNear[calc, expe, label, eps]]

(* ** norm *)

CNorm[u_] := CIP[u, u];

(* ** radial representation *)

AtR[r_][a_+b_]:=AtR[r][a]+AtR[r][b]

(* *  STO/GTO  *)
(* ** zero*)

STO[0, __]:=0
GTO[0, __]:=0


(* ** integration*)
(* *** STO*)

(* integration of r^n e^{-z1 r} e^{-z2 r} in L2+ space *)
integ[STO, STO][n_, z1_, z2_]:= With[{z=z1+z2},
				     Gamma[1+n]/(z^(1+n))]


(* *** GTO*)

(* integration of r^n e^{-z1 r^2} e^{-z2 r^2} in L2+ space *)
integ[GTO, GTO][n_, z1_, z2_]:= With[
	{ z = z1+z2},
	1/2 * z^(-(1/2) - n/2) * Gamma[(1+n)/2]]


(* *** STO-GTO*)

(* integration of r^n e^{-z1 r^2} e^{-z2 r} in L2+ space *)
integ[GTO, STO][n_,z1_, z2_]:= integ[STO, GTO][n, z2, z1]

integ[STO, GTO][0_, zs_, zg_]:=(E^(as^2/(4 ag)) Sqrt[Pi] Erfc[as/(2 Sqrt[ag])])/(2 Sqrt[ag])

integ[STO, GTO][1, as_, ag_] := 
 1/(2 ag) - (as E^(as^2/(4 ag)) Sqrt[\[Pi]] Erfc[as/(2 Sqrt[ag])])/(
  4 ag^(3/2))

integ[STO, GTO][2, as_, 
		     ag_] := (-2 Sqrt[ag] as + (2 ag + as^2) E^(as^2/(4 ag)) Sqrt[\[Pi]]
    Erfc[as/(2 Sqrt[ag])])/(8 ag^(5/2))

integ[STO, GTO][3, as_, ag_] := (4 ag + as^2)/(8 ag^3) - (
  as (6 ag + as^2) E^(as^2/(4 ag)) Sqrt[\[Pi]]
    Erfc[as/(2 Sqrt[ag])])/(16 ag^(7/2))

integ[STO, GTO][4, as_, ag_] := (-2 Sqrt[ag]
    as (10 ag + as^2) + (12 ag^2 + 12 ag as^2 + as^4) E^(as^2/(4 ag))
    Sqrt[\[Pi]] Erfc[as/(2 Sqrt[ag])])/(32 ag^(9/2))

integ[STO, GTO][5, as_, ag_] := 
 1/(64 ag^(
   11/2)) (2 Sqrt[ag] (2 ag + as^2) (16 ag + as^2) - 
    as (60 ag^2 + 20 ag as^2 + as^4) E^(as^2/(4 ag)) Sqrt[\[Pi]]
      Erfc[as/(2 Sqrt[ag])])

integ[STO, GTO][6, as_, ag_] := 
 1/(128 ag^(
   13/2)) (-2 Sqrt[ag]
      as (6 ag + as^2) (22 ag + as^2) + (120 ag^3 + 180 ag^2 as^2 + 
       30 ag as^4 + as^6) E^(as^2/(4 ag)) Sqrt[\[Pi]]
      Erfc[as/(2 Sqrt[ag])])



(* ** normalized*)

CNormalized[AO_][n_,z_]:= With[
	{ nterm = 1 / Sqrt[integ[AO,AO][2*n, z, z]]},
	AO[ nterm, n, z]]


(* ** inner product *)

CIP[AO1_[c1_,n1_,z1_], AO2_[c2_,n2_,z2_]]:= With[
	{ term = c1 * c2,
	  f = integ[AO1, AO2]
	},
	term * f[n1+n2, z1, z2]]


(* ** operator *)

OP[CST[d_]][AO_[c_, n_, z_]]:= AO[c * d, n, z]
OP[RM[m_]][AO_[c_, n_, z_]]:= AO[c, n+m, z]
OP[DD[2]][STO[c_, n_, z_]]:= STO[c * n * (n-1), n-2, z] + 
			     STO[-2*z*c*n, n-1, z] + 
			     STO[z*z*c, n, z]
OP[DD[2]][GTO[c_,n_,z_]]:= GTO[c * n * (n - 1 ), n-2, z] + 
			   GTO[c * (-2 * z) * (2*n+1), n, z] +
			   GTO[c * 4z z, n+2, z]

(* **  r representation *)

AtR[r_][STO[c_,n_,z_]]:= c r^n Exp[-z r]
AtR[r_][GTO[c_,n_,z_]]:= c r^n Exp[-z r^2]



(* * End *)
(* 
End[];
EndPackage[];
*)


