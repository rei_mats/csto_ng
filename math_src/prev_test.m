<< "l2func.m";
P[a_] := Block[{}, Print[""]; Print[a]]
P[lbl_, a_] := Block[{}, Print[""]; Print[lbl]; Print[a]]

c0s = {1.+0.1I, 2.+0.2I};
z0s = {3-0.3I,  4-0.4I};

sto = CNormalized[STO][1,1];
sys = Stong[Log, Log][sto, 1, {c0s, z0s}];
objf= StongObjectiveFunc[sys];
cons= StongConstraints[sys];
x0s = StongInitGuess[sys];

Print["none, none"]
P[sys];
P[objf];
P[cons];
P[x0s];

P["Log, Constraints"];
sys  = Stong[Log, "Constraints"][sto, 1, {c0s, z0s}];
objf = StongObjectiveFunc[sys];
cons = StongConstraints[sys];
x0s  = StongInitGuess[sys];
P[sys];
P[objf];
P[cons];
P[x0s];

P["Log, None"];
sys  = Stong[Log, None][sto, 1, {c0s, {0.15, 0.85}}];
objf = StongObjectiveFunc[sys];
cons = StongConstraints[sys];
x0s  = StongInitGuess[sys];
P[sys];
P[objf];
P[cons];
P[x0s];

(* 
General::ivar: 0.15 is not a valid variable.
General::ivar: 0.15 is not a valid variable.
General::ivar: 0.85 is not a valid variable.
*)


(* 
sys = Stong[{"num"->2, "nGTO"->1, "nSTO"->1, "zSTO"->1,
	   "rcs"->rcs,"rzs"->rzs,"ics"->ics,"izs"->izs}];

rep = StongReplace[{{1.+0.1I,2.+0.2I}, {3+0.3I,4+0.4I}}, {rcs,rzs,ics,izs}];
initGuess = StongInitGuess[{{1.+0.1I,2.+0.2I}, {3+0.3I,4+0.4I}}, {rcs,rzs,ics,izs}];

dist= StongObjectiveFunc[sys];

P[sys];
P[initGuess];
P[dist];
P[dist/.rep];
P[rep];



*)
