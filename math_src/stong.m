(* *  Begin    *)
(* ** Package*)
(* 
BeginPackage["stong`"];
*)

(* ** Utils*)
 
IConjugate::usage="IConjugate[z] gives the complex conjugate of the complex number z.";
IRe::usage="IRe[z] gives the real part of the complex number z.";
IIm::usage="IIm[z] gives the imaginary part of the complex number z.";

NumRe::usage="NumRe[z]. Numerical evaluation of real part of the complex number z.";
NumIm::usage="NumIm[z] gives numerical evaluation of imaginary part of the complex number z.";

BindBlock::usage="BindBlock[vs,v0s,block]";

ApproxRational::usage="ApproxRational[d, n] gives approximate rational of floating number d.";
ForceN::usage="ForceN[x, p] gives floating point x with p precision.";
(* 
ExpectNear::usage="ExpectNear[calc,expect,label, eps] launch a unit test for continuous value";
*)
	      

(* ** Hilbert Space, to be removed *)

(* 
OP::usage="linear operator in Hilbert space";
CIP::usage="complex inner product in Hilbert space";
CNorm::usage="comple norm."
AtR::usage="AtR[r][a]: calculate radial coordinate representation for a";

(* LCAO::usage="LCAO[{c1,u1}, {c2,u2},...] means a linear model whose coefficients are{c_i} and basis functions are {u_i}";
 *)

CST::usage="OP[CST[c]]: multiplication of constant c.";
DD::usage="OP[DD[n]][u]: gives n th derivative of function u";
RM::usage="OP[RM[n]][u]: gives multicative of r^m to function u";

UnitTestCIP::usage="UnitTestCIP[u1, u2, eps] launch a unit test for complex inner product of u1 and u2."

*)

(* ** STO/GTO, to be removed*)
(* 
CNormalized::usage="Normalized[a]: normalized l2 function a";
STO::usage="STO[c,n,z]: slater type determinant";
GTO::usage="GTO[c,n,z]: gauss type orbital";
*)

(* ** SQP*)

FindMinimumSQP::usage="FindMinimumSQP[{f, cons}, {{x,x0}, ...}] gives the constraints optimization using SQP method with simple Hessian";
		      

(* ** STO-NG*)

stongConstraints::usage="stongConstraints";
StongConstraints::usage="StongConstraints";

Stong::usage="StoNG[{\"num\":numGto, \"nGto\":nGto, \"nSto\"->nSto, \"zSto\"->zSto, \"z\"->z,\"c\"->c}] calculate {STO, STO-NG}";

StongShowResFindMinimum::usage="StongShowResFindMinimum[pre, resFindMinimum] gives user frienly output from FindMinimum result 'resFindMinimum'.";

StongFindMin::usage="find orbital exponent and coefficient for STO-NG expansion.";

StongFindMinimumLog::usage="find orbital exponent and coefficient for STO-NG expansion.";

(* ** Result Analysis*)
GetThetasZetas::usage="GetThetasZetas[res_] gives a list {ts, zss}, where res is the result of StongFindMinimumForStos, ts is a list of argument and zss is a list of orbital exponents of each GTOs";

GetZetasReIm::usage="GetZetasReIm[res_] gives a list whose element is pair of real part and imaginary part of GTO zeta.";
GetThetaZetaRe::usage="GetThetaZetaRe[res_] gives a list whose element is pair of argument of STO zeta and real part of GTO zeta";
GetThetaZetaIm::usage="GetThetaZetaIm[res_] gives a list whose element is pair of argument of STO zeta and imag part of GTO zeta";

GetRadialFunctions::usage="GetRadialFunctions[res, r, n] gives a list of radial functions {Re STO, Im STO, Re STONG Im STONG}."

(* ** begin*)
(* 
Begin["stong`"];
*)

(* *  Utils    *)
(* ** complex number*)

IConjugate[z_]:=z/.Complex[a_, b_]:>Complex[a,-b]
IRe[z_]:= (z + IConjugate[z]) / 2;
IIm[z_]:= (z - IConjugate[z]) / (2I);

NumRe[z_]:= Re[z]/;NumberQ[z]
NumRe/: D[NumRe[f_], x__] := NumRe[D[f, x]]

NumIm[z_]:= Im[z]/;NumberQ[z]
NumIm/: D[NumIm[f_], x__] := NumIm[D[f, x]]


(* ** bind variables*)

BindBlock[vs_, v0s_, block_] := Block[
	vs, vs = v0s; ReleaseHold[block ]]


(* ** floating point*)

ApproxRational[d_, n_Integer]:=With[{ m=10^n}, Round[d m]/m]

ForceN[d_,prec_] := With[{ prec0 = Precision[d]},
			 If[ prec == prec0,
			     d,
			     If[ prec < prec0,
				 N[d, prec],
				 N[ApproxRational[d, Round[prec]+1], prec]]]]

(* ** unite test for complex number*)

(*
machineEps:=10^(-MachinePrecision)
ExpectNear[calc_, expe_, label_, 
	       eps_:10* machineEps]:=Block[
  {success, msg},
  
  success = Abs[calc-expe]<eps;
  
  If[success===True,
     Print["[ SUCCESS ] " <> label],
     Print["[ FAILED  ] " <> label];
     Print["+calc: "<>ToString[calc]];
     Print["+expe: "<>ToString[expe]];
     Print["+-----------------:"];]]
*)
		   
(* *  Hilbert Space, to be removed  *)
(* ** LCAO*) 

LCAO[] := 0

(* ** operator*)
(* 
OP[a_][0] := 0

OP[a_+b_][f_]:=OP[a][f] + OP[b][f]
OP[a_*b_][f_]:=OP[a][OP[b][f]]

OP[c_*b_][f_]:=c*OP[b][f]/;NumberQ[c]
OP[a_][c_*f_]:=c*OP[a][f]/;NumberQ[c]
OP[a_][f_ + g_]:= OP[a][f] + OP[a][g]
*)

(* ** complex inner product*)

(*
 
UnitTestCIP[u1_, u2_, label_, eps_:10*MachinePrecision]:=Block[
   {calc, expe, r},
   calc = CIP[u1, u2];
   expe = Integrate[AtR[r][u1]*AtR[r][u2], {r,0,Infinity}];
   ExpectNear[calc, expe, label, eps]]

CIP[0, _] :=0
CIP[a_, 0]:=0

CIP[a_,b_+c_]:=CIP[a,b]+CIP[a,c]
CIP[a_+b_,c_]:=CIP[a,c]+CIP[b,c]
CIP[c_*a_, b_]:= c * CIP[a,b] /; NumberQ[c]
CIP[a_, c_ * b_]:= c * CIP[a,b] /; NumberQ[c]

CNorm[u_] := CIP[u, u];
*)

(* ** radial coordinate represent*)

 (* 
AtR[r_][a_+b_]:=AtR[r][a]+AtR[r][b]
*)

(* *  STO/GTO, to be removed  *)
(* ** zero*)

	  (* 
STO[0, __]:=0
GTO[0, __]:=0
*)

(* ** integration*)
(* *** STO*)

(* integration of r^n e^{-z1 r} e^{-z2 r} in L2+ space *)
integ[STO, STO][n_, z1_, z2_]:= With[{z=z1+z2},
				     Gamma[1+n]/(z^(1+n))]


(* *** GTO*)

(* integration of r^n e^{-z1 r^2} e^{-z2 r^2} in L2+ space *)
integ[GTO, GTO][n_, z1_, z2_]:= With[
	{ z = z1+z2},
	1/2 * z^(-(1/2) - n/2) * Gamma[(1+n)/2]]


(* *** STO-GTO*)

(* integration of r^n e^{-z1 r^2} e^{-z2 r} in L2+ space *)
integ[GTO, STO][n_,z1_, z2_]:= integ[STO, GTO][n, z2, z1]

integ[STO, GTO][0_, zs_, zg_]:=(E^(as^2/(4 ag)) Sqrt[Pi] Erfc[as/(2 Sqrt[ag])])/(2 Sqrt[ag])

integ[STO, GTO][1, as_, ag_] := 
 1/(2 ag) - (as E^(as^2/(4 ag)) Sqrt[\[Pi]] Erfc[as/(2 Sqrt[ag])])/(
  4 ag^(3/2))

integ[STO, GTO][2, as_, 
		     ag_] := (-2 Sqrt[ag] as + (2 ag + as^2) E^(as^2/(4 ag)) Sqrt[\[Pi]]
    Erfc[as/(2 Sqrt[ag])])/(8 ag^(5/2))

integ[STO, GTO][3, as_, ag_] := (4 ag + as^2)/(8 ag^3) - (
  as (6 ag + as^2) E^(as^2/(4 ag)) Sqrt[\[Pi]]
    Erfc[as/(2 Sqrt[ag])])/(16 ag^(7/2))

integ[STO, GTO][4, as_, ag_] := (-2 Sqrt[ag]
    as (10 ag + as^2) + (12 ag^2 + 12 ag as^2 + as^4) E^(as^2/(4 ag))
    Sqrt[\[Pi]] Erfc[as/(2 Sqrt[ag])])/(32 ag^(9/2))

integ[STO, GTO][5, as_, ag_] := 
 1/(64 ag^(
   11/2)) (2 Sqrt[ag] (2 ag + as^2) (16 ag + as^2) - 
    as (60 ag^2 + 20 ag as^2 + as^4) E^(as^2/(4 ag)) Sqrt[\[Pi]]
      Erfc[as/(2 Sqrt[ag])])

integ[STO, GTO][6, as_, ag_] := 
 1/(128 ag^(
   13/2)) (-2 Sqrt[ag]
      as (6 ag + as^2) (22 ag + as^2) + (120 ag^3 + 180 ag^2 as^2 + 
       30 ag as^4 + as^6) E^(as^2/(4 ag)) Sqrt[\[Pi]]
      Erfc[as/(2 Sqrt[ag])])



(* ** normalized*)
(* 
CNormalized[AO_][n_,z_]:= With[
	{ nterm = 1 / Sqrt[integ[AO,AO][2*n, z, z]]},
	AO[ nterm, n, z]]
*)

(* ** inner product *)
(* 
CIP[AO1_[c1_,n1_,z1_], AO2_[c2_,n2_,z2_]]:= With[
	{ term = c1 * c2,
	  f = integ[AO1, AO2]
	},
	term * f[n1+n2, z1, z2]]
*)

(* ** operator *)

(* 
OP[CST[d_]][AO_[c_, n_, z_]]:= AO[c * d, n, z]
OP[RM[m_]][AO_[c_, n_, z_]]:= AO[c, n+m, z]
OP[DD[2]][STO[c_, n_, z_]]:= STO[c * n * (n-1), n-2, z] + 
			     STO[-2*z*c*n, n-1, z] + 
			     STO[z*z*c, n, z]
OP[DD[2]][GTO[c_,n_,z_]]:= GTO[c * n * (n - 1 ), n-2, z] + 
			   GTO[c * (-2 * z) * (2*n+1), n, z] +
			   GTO[c * 4z z, n+2, z]
*)
(* ** r representation *)
(* 
AtR[r_][STO[c_,n_,z_]]:= c r^n Exp[-z r]
AtR[r_][GTO[c_,n_,z_]]:= c r^n Exp[-z r^2]
*)
(* *  SQP      *)
(* ** KKT matrix and right hand vector*)
(* *** doc *)
(*
   f : objective function.
   fc: constraints functions
   xs: variables of these functions.
   ls: Lagrangian multipliers.

   gf / gfc: gradient of f/fc.
   hf / hfc: hessian of f/fc.
   gL / hL : output.
 *)

(* *** main*)

SQPMatVec[f_, fc_, xs_, ls_] := Module[
  {gf, hf, gfc, hfc, hL, gL},
  
  gf = Table[D[f, v], {v, xs}];
  hf = Table[D[f, v, u], {v, xs}, {u, xs}];
  gfc = Table[D[c, u], {c, fc}, {u, xs}];
  hfc = Table[D[c, u, v], {c, fc}, {u, xs}, {v, xs}];
  
  gL = Join[gf, fc];
  hL = ArrayFlatten[{
     {hf - Dot[ls, hfc], Transpose[-gfc]},
     {gfc, Table[0, {i, Length[ls]}, {j, Length[ls]}]}
     }];
  {gL, hL}]


(* ** one step*)
(* *** docs*)
(*
   lambdaAdd == True  => update Lagrange multiplier by adding (Newton mode).
   lambdaAdd == False => update Lagrange multiplier by replace (SQP mode).
 *)

(* *** option*)
Options[SQPOneStep] = {
	WorkingPrecision -> MachinePrecision,
	StepMonitor -> None,
	EvaluationMonitor -> None };


(* *** Tags*)

SQPOneStep::lineareq="Error is detected in linear solver.";
SQPOneStep::toolarge="the norm of update vector is too large";
                     

(* *** main*)

SQPOneStep[lambdaAdd_, xs_, ls_, f_, gL_, hL_, OptionsPattern[]][
	{_, _, inx0s_, inl0s_}] := Block[
	{ x0s, l0s, wp, f0, gL0, hL0},

	wp = OptionValue[WorkingPrecision];
	x0s = ForceN[#, wp]& /@ inx0s;
	l0s = ForceN[#, wp]& /@ inl0s;

	(* compute objective function, gradient,and hessian*)
	{f0, gL0, hL0} = BindBlock[
		Join[xs,ls],   (* symbols for binds *)
		Join[x0s, l0s],  (* values for symbols *)
		Hold[ OptionValue[StepMonitor];
		      OptionValue[EvaluationMonitor];
		      Map[ N[#, wp]&, {f, gL, hL}]]];

	(* update and return *)
	Block[ { succ, dvs, n, x1s, l1s },
	       succ = Check[ dvs = LinearSolve[hL0, gL0];
			     True,
			     Message[SQPOneStep::lineareq];
			     False];
           If[Max[Abs/@dvs] > 10^5,
              Message[SQPOneStep::toolarge];
              succ = False];
	       n = Length[xs];
	       x1s = x0s - dvs[[1 ;; n]];
	       l1s = If[lambdaAdd,
			l0s - dvs[[n+1 ;; -1]],
			-dvs[[n+1 ;; -1]]];
	       { succ, f0, x1s, l1s}]]
	       

(* ** main*)
(* *** option*)

Options[FindMinimumSQP] = {
	"LambdaAdd" -> True,
	WorkingPrecision -> MachinePrecision,
	PrecisionGoal -> Automatic,
	AccuracyGoal -> Automatic,
	StepMonitor -> None,
	EvaluationMonitor -> None,
	MaxIterations -> 100};


(* *** main*)

FindMinimumSQP[{f_, cons_}, guess_, OptionsPattern[]] := Module[
  {x, L, gL, hL, numxs, numcons, xs, ls, fc, lambda, oneStep, initGuess, 
   test, wp, aGoal, pGoal},
  
  (* calculation condition*)
  wp = OptionValue[WorkingPrecision];
  {aGoal, pGoal} = Table[ With[{o = OptionValue[a]},
			       If[o === Automatic, wp/2, o]], 
			  {a, {AccuracyGoal, PrecisionGoal}}];
  
  (* symbols*)
  numxs = Length[xs]; numcons = Length[cons];
  ls = Table[Unique[], {i, numcons}];
  xs = First /@ guess; 
  fc = Table[eq /. Equal[x_, 0] -> x, {eq, cons}];
  {gL, hL} = SQPMatVec[f, fc, xs, ls]; 
  
  (*calculation setting*)
  oneStep = SQPOneStep[OptionValue["LambdaAdd"], xs, ls, f, gL, hL,
			  WorkingPrecision ->wp,
			  StepMonitor :> OptionValue[StepMonitor],
			  EvaluationMonitor :> OptionValue[EvaluationMonitor]];
  initGuess = {True, 0, Last /@ guess, ConstantArray[0, numcons]};
  test[{succ0_, f0_, x0_, l0_}, {succ1_, f1_, x1_, l1_}] := Block[
	  { dx },
	  dx = Max[Map[Abs, x1 - x0]];
	  And[ succ1,
	       dx > N[Max[10^(-aGoal), 10^(-pGoal)*Max[Abs /@ x0]]]]];
  
  (*computation*)
  Block[{succ0, f0, x0, l0},
	{succ0, f0, x0, l0} = NestWhile[oneStep, initGuess, test, 2, 
					OptionValue[MaxIterations]];
	{ f0, 
	  MapThread[(#1 -> #2) &, {xs, x0}] }]]


(* *  Mixed FindMinimum*)
(* ** Options *)

Options[FindMinimumMix] = {
        WorkingPrecision -> MachinePrecision,
	    PrecisionGoal -> Automatic,
	    AccuracyGoal -> Automatic,
	    StepMonitor -> None,
	    EvaluationMonitor -> None,
	    MaxIterations -> 500} ;

(* ** main *)

FindMinimumMix[{f_, cons_}, guess_, OptionsPattern[]]:= Block[
        {xs,
         x0s, f0, res0,
         x1s, f1, res1, guess1},

        {xs, x0s} = Transpose[guess];
        
        {f0, res0} = FindMinimum[{f, cons}, guess,
                                 WorkingPrecision->OptionValue[WorkingPrecision],
                                 PrecisionGoal->OptionValue[PrecisionGoal],
                                 AccuracyGoal->OptionValue[AccuracyGoal],
                                 StepMonitor:>OptionValue[StepMonitor],
                                 EvaluationMonitor:>OptionValue[EvaluationMonitor],
                                 MaxIterations->OptionValue[MaxIterations]];

        x1s = Table[x/.res0, {x, xs}];
        guess1 = Transpose[{xs, x1s}];
        {f1, res1} = FindMinimumSQP[{f, cons}, guess1,
                                    "LambdaAdd"->False,
                                    WorkingPrecision->OptionValue[WorkingPrecision],
                                    PrecisionGoal->OptionValue[PrecisionGoal],
                                    AccuracyGoal->OptionValue[AccuracyGoal],
                                    StepMonitor:>OptionValue[StepMonitor],
                                    EvaluationMonitor:>OptionValue[EvaluationMonitor],
                                    MaxIterations->OptionValue[MaxIterations]];
        {f1, res1}]

(* *  STO-NG   *)
(* ** Coordinate *)
(* *** doc*)
(*
 coordinate used for STO-nG calculations.
 CoordReg is conversion from real and imaginary
 part of orbital exponent to log scale if necessary.
 CoordInv is inversion of CoordReg.
 *)


(* *** main*)

ReCoordReg[_][z_] := Re[z];
ReCoordReg[Log][z_] := Log[Re[z]]
ImCoordReg[_][z_] := Im[z];
ImCoordReg[Log][z_] := Log[-Im[z]]

CoordInv[_, _][x_, y_]:= x + I y
CoordInv[Log, _][x_, y_]:= Exp[x] + I y
CoordInv[_, Log][x_, y_]:= x -I Exp[y]
CoordInv[Log,Log][x_,y_]:= Exp[x] - I Exp[y]


(* ** precalculation *)

Stong[rzType_, izType_][sto_, nGTO_ , {c0s_, z0s_}] := Module[
	{ num = Length[c0s], symC1, symC2, symZ1, symZ2, zs, cs,
	gtos, initGuess},

	(* create unique symbols for real and imag part of coef and zeta.*)
	{symC1, symC2, symZ1, symZ2} = Table[Table[Unique[],
						   {i, num}], {j, 4}];
	cs = MapThread[#1 + I #2&, {symC1, symC2}];
	zs = MapThread[CoordInv[rzType, izType],
		       {symZ1, symZ2}];

	(* formulate linear combination of GTOs and initial guess for FindMinimum*)
	gtos= MapThread[OP[CST[#1]][CNormalized[GTO][nGTO, #2]]&,
			  {cs, zs}];
	initGuess = List[ MapThread[{#1, #2}&, {symC1, Re/@c0s}],
			  MapThread[{#1, #2}&, {
				  symZ1, Map[ReCoordReg[rzType], z0s]}],
			  MapThread[{#1, #2}&, {symC2, Im/@c0s}],  
			  MapThread[{#1, #2}&, {
				  symZ2, Map[ImCoordReg[izType], z0s]}]];

	(* return calculation systems *)
	List[ sto, gtos, initGuess, {rzType, izType},  {cs, zs}]]


(* ** numerical evaluate*)

StongNumericalEvaluate[pre:{sto_, gtos_, initGuess_, __}, func_]:=Block[
  {rep},
  rep = MapThread[Rule, 
		  Transpose[Flatten[Join@@Transpose[initGuess], {1}]]];
  func/.rep]
	      

(* ** distance2*)

StongObjectiveFunc[{sto_, gs_, rest___}, amp_:1]:=With[
	{ num = Length[gs],
	  csto = IConjugate[sto],
	  cgs  = Map[IConjugate, gs] },
	NumRe@Plus[ amp*amp*CIP[csto, sto],
		    Plus@@Table[-2*amp*CIP[csto, g], {g, gs}],
		    Plus@@MapThread[CIP[#1,#2]&, {cgs, gs}],
		    Plus@@Flatten[Table[2CIP[cgs[[i]], gs[[j]]], {i,num}, {j,i-1}]]]]

(* ** distance2 with normalized*)
(* *** doc*)
(* 
cnormalized STO-NG is defined by

nTerm = 1 / \sqrt{(\sum_i c_i g_i, \sum_i c_i g_i)} \sum_i c_i g_i

*)
(* *** code*)

StongDistance2WithNormalized[{sto_, gs_, rest___}]:=Block[
  { num, nml, csto, nGs, cnGs},
  num   = Length[gs];
  nml = 1 / Sqrt[CIP[Plus@@gs, Plus@@gs]];
  csto = IConjugate[sto];
  nGs = Table[OP[CST[nml]][g], {g, gs}];
  cnGs = Map[IConjugate, nGs];

  NumRe[Plus[ CIP[csto, sto],
	      Plus@@Table[-2 CIP[csto, g], {g, nGs}],
	      Plus@@MapThread[CIP[#1,#2]&, {cnGs, nGs}],
	      Plus@@Flatten[Table[2CIP[cnGs[[i]], nGs[[j]]], 
				  {i,num}, {j,i-1}]]]]]

(* *** code with C1=1*)

StongDistance2WithNormalizedC1Eq1[{sto_, gs_, iGuess_, rest___}]:=Block[
  { num, nml, csto, nGs, cnGs},
  num   = Length[gs];
  nml = 1 / Sqrt[CIP[Plus@@gs, Plus@@gs]];
  csto = IConjugate[sto];
  nGs = Table[OP[CST[nml]][g], {g, gs}] /. {iGuess[[1,1,1]]->1, iGuess[[3,1,1]]->0};
  cnGs = Map[IConjugate, nGs];

  NumRe[Plus[ CIP[csto, sto],
	      Plus@@Table[-2 CIP[csto, g], {g, nGs}],
	      Plus@@MapThread[CIP[#1,#2]&, {cnGs, nGs}],
	      Plus@@Flatten[Table[2CIP[cnGs[[i]], nGs[[j]]], 
				  {i,num}, {j,i-1}]]]]]


(* ** constrations *)
(* *** option *)

(* 
stongConstraints::fail="unsupported constraints: `1`";
*)

(* *** minor *)

stongConstraints["cNorm"][sto_, gs_, rest___]:= With[
  {
    cnorm2 = CIP[Plus@@gs, Plus@@gs]
  },
  { NumRe[1 - cnorm2] == 0,
    NumIm[cnorm2]     == 0}]
stongConstraints["RePlus"][_, _, {_, rzs_, rest___}, ___] := 
	Table[ r >= 0, {r, First/@rzs}]
stongConstraints["ImMinus"][_, _, {_, _, _ ,izs__}, ___] := 
	Table[ i <= 0, {i, First/@izs}]
stongConstraints[OP[op_]][sto_, gs_, rest___] := With[
  { opSto = CIP[sto, OP[op][sto]],	
    opGto = CIP[Plus@@gs, OP[op][Plus@@gs]]
  },
  { NumRe[ opSto - opGto] == 0,
    NumIm[ opSto - opGto] == 0}]

(* 
stongConstraints[x_][___]:=Message[stongConstraints::fail, x]
*)

(* *** major*)	
StongConstraints[{sto_, gs_, {rcs_, rzs_, ics_, izs_}, 
		  {rzType_, izType_}, _}, opt_:{"cNorm"}]:= Block[
  {  consList = Join[opt,
		     If[rzType==="Constraints", {"RePlus"}, {}],
		     If[izType==="Constraints", {"RePlus"}, {}]]
  },
  Flatten[Table[ stongConstraints[c][sto, gs, {rcs, rzs, ics, izs}]
		,{c, consList}]]]

(* *** old*)
StongConstraintsOld[{sto_, gs_, {rcs_, rzs_, ics_, izs_}, 
		  {rzType_, izType_}, _}, opt_:{"cnorm"}]:= Block[
  {
    cnorm2 = CIP[Plus@@gs, Plus@@gs]
  },
  Join[{ NumRe[ 1 - cnorm2] == 0,
	 NumIm[cnorm2]      == 0},
       If[rzType === "Constraints",
	  Table[r >= 0, {r, First/@rzs}],
	  {}],
       If[izType === "Constraints",
	  Table[i <= 0, {i, First/@izs}],
	  {}]]]

(* ** initial guess*)

StongInitGuess[{_, _, initGuess_, __}] := Apply[Join, initGuess]


(* ** Show Result of FindMinimum*)

StongShowResFindMinimum[{_, _, _, _, {cs_, zs_}}, {f1_, rep1_}]:=Block[
   {},
   {f1, 
    {"coefs"->cs/.rep1,
     "zetas"->zs/.rep1}}]

(* ** Monitor *)

StongStepMonitor[{_, _, { rcs_, rzs_, ics_, izs_}, __}, None] := None
StongStepMonitor[{_, _, { rcs_, rzs_, ics_, izs_}, __}, stepFunc_] := Block[
	{ cs = First/@rcs + I First/@ics,
	  zs = First/@rzs + I First/@izs },
	stepFunc[cs, zs]]

(* ** Main *)
(* *** options *)

Options[StongFindMinimum] =  {
	"Amplitude"->1,
        WorkingPrecision->MachinePrecision,
        MaxIterations->500,
        "MonitorFunction" -> None,
	"ReZeta"->None, "ImZeta"->None,
	"Constraints"->{"cNorm"},
	Method->"InteriorPoint"};

(* *** tags*)

StongFindMinimum::fail="(zSTO,nSTO,nGTO)=(`1`,`2`,`3`) calculation failed.";

(* *** main*)

StongFindMinimum[args_, OptionsPattern[]] := Module[
	{maxnum, wp, monitorFunc, nGTO, zSTO, c0s, z0s, amp, 
	 rzType, izType, findMinimum,
	 sys, objFunc, cons, initGuess, resObjFunc, resRep},

	(* read input datas*)
	{nSTO, zSTO, nGTO, c0s, z0s} = Table[ k /. args, { k,
			{"nSTO", "zSTO", "nGTO", "c0s", "z0s"}}];
	{maxnum, wp, monitorFunc, rzType, izType, amp} = Table[
            OptionValue[o],
		    { o, {MaxIterations, WorkingPrecision, 
			  "MonitorFunction", "ReZeta", "ImZeta", "Amplitude"}}];
				  
	findMinimum = Switch[ OptionValue[Method],
			      "InteriorPoint", FindMinimum,
			      "Newton", FindMinimumSQP[##, "LambdaAdd"->True]&,
			      "SQP", FindMinimumSQP[##, "LambdaAdd"->False]&,
                  "Mix", FindMinimumMix,
			       _    , None];

	(* objective function, constraints and initial guess*)
	sys = With[{ sto = CNormalized[STO][nSTO, N[zSTO, wp]] },
		   Stong[rzType, izType][sto, nGTO, {c0s, z0s}]];
	objFunc = StongObjectiveFunc[sys, amp];
	cons    = StongConstraints[sys, OptionValue["Constraints"]];
	iGuess  = StongInitGuess[sys];

	(* Calculations*)
	success = Check[ 
		{resObjFunc, resRep} = findMinimum[
			{objFunc, cons}, iGuess,
			MaxIterations->maxnum,
			StepMonitor:>StongStepMonitor[sys, monitorFunc],
			WorkingPrecision->wp];
		True,
		Message[StongFindMinimum::fail, zSTO, nSTO, nGTO];
		False];	

	(* return calculation result*)
	If[success, 
	   { "success"-> success,
	     "zSTO"->("zSTO"/.args), "dist2"->resObjFunc,
	     "cs" -> sys[[-1,1]] /. resRep, "zs" -> sys[[-1,2]] /. resRep },
       { "success"-> success, "zSTO"->("zSTO"/.args), "dist2"->resObjFunc }]]

(* ** Main for ratio*)
(* *** options*)

Options[StongFindMinimumRatio] = {
	"Amplitude"->1,
        WorkingPrecision->MachinePrecision,
        MaxIterations->500,
        "MonitorFunction" -> None,
	"ReZeta"->None, "ImZeta"->None,
	Method->{"Newton", "StepControl"->"TrustRegion"} }

(* *** tags*)

StongFindMinimumRatio::fail="(zSTO,nSTO,nGTO)=(`1`,`2`,`3`) calculation failed.";


(* *** main*)

StongFindMinimumRatio[args_, OptionsPattern[]] := Module[
  { 
    nSTO, zSTO, nGTO, c0s, z0s, 
    maxnum, wp, monitorFunc, rzType, izType,
    sys, objFunc, iGuess,
    success,
    resObjFunc, resRep
  },
  
  (* read variables*)
  { nSTO, zSTO, nGTO, c0s, z0s } = 
  Table[ k/.args, {k, {"nSTO", "zSTO", "nGTO", "c0s", "z0s"}}];

  {maxnum, wp, monitorFunc, rzType, izType} = 
  Table[ OptionValue[o],
	 { o, {MaxIterations, WorkingPrecision, 
	       "MonitorFunction", "ReZeta", "ImZeta"}}];


  (* objective functions and initial guess *)
  sys = With[{ sto = CNormalized[STO][nSTO, N[zSTO, wp]] },
		   Stong[rzType, izType][sto, nGTO, {c0s, z0s}]];
  objFunc = StongDistance2WithNormalized[sys];
  iGuess  = StongInitGuess[sys];  

  
  (* Calculation *)
  success = Check[{resObjFunc, resRep} = FindMinimum[
	  objFunc, iGuess,
	  MaxIterations->maxnum,
	  StepMonitor:>StongStepMonitor[sys, monitorFunc],
	  WorkingPrecision->wp];
		  True,
		  Message[StongFindMinimum::fail, zSTO, nSTO, nGTO];
		  False];  

  (* return *)
  If[success,
     { "success"-> success,
       "zSTO"->("zSTO"/.args), "dist2"->resObjFunc,
       "cs" -> sys[[-1,1]] /. resRep, "zs" -> sys[[-1,2]] /. resRep },
     { "success"-> success, "zSTO"->("zSTO"/.args), "dist2"->resObjFunc }]]

(* ** Main only coeff *)
(* *** options *)
Options[StongFindMinimumOnlyCoef] =  {
	"Amplitude"->1,
        WorkingPrecision->MachinePrecision,
        MaxIterations->500,
        "MonitorFunction" -> None,
	"ReZeta"->None, "ImZeta"->None,
	"Constraints"->{"cNorm"},
	Method->"InteriorPoint"};
(* *** tags *)

StongFindMinimum::fail="(zSTO,nSTO,nGTO)=(`1`,`2`,`3`) calculation failed.";

(* *** main *)
StongFindMinimumOnlyCoef[args_, OptionsPattern[]] := Module[
	{maxnum, wp, monitorFunc, nGTO, zSTO, c0s, z0s, amp, 
	 rzType, izType, findMinimum,num,
	 sys, objFunc, cons, initGuess, resObjFunc, resRep,
	 repZs, objFuncCs,    consCs, initGuessCs },

	(* read input datas*)
	{nSTO, zSTO, nGTO, c0s, z0s} = Table[ k /. args, { k,
			{"nSTO", "zSTO", "nGTO", "c0s", "z0s"}}];
	{maxnum, wp, monitorFunc, rzType, izType, amp} = Table[
            OptionValue[o],
		    { o, {MaxIterations, WorkingPrecision, 
			  "MonitorFunction", "ReZeta", "ImZeta", "Amplitude"}}];
				  
	findMinimum = Switch[ OptionValue[Method],
			      "InteriorPoint", FindMinimum,
			      "Newton", FindMinimumSQP[##, "LambdaAdd"->True]&,
			      "SQP", FindMinimumSQP[##, "LambdaAdd"->False]&,
                  "Mix", FindMinimumMix,
			       _    , None];

	(* objective function, constraints and initial guess*)
	sys = With[{ sto = CNormalized[STO][nSTO, N[zSTO, wp]] },
		   Stong[rzType, izType][sto, nGTO, {c0s, z0s}]];
	objFunc = StongObjectiveFunc[sys, amp];
	cons    = StongConstraints[sys, OptionValue["Constraints"]];
	iGuess  = StongInitGuess[sys];

	(* restriction for coeffs *)
	num = Length[z0s];
	repZs = Map[#[[1]]->#[[2]]&, 
			     Join[iGuess[[num+1;;2*num]], 
				  iGuess[[3*num+1;;4*num]]]];
	objFuncCs = objFunc/.repZs;
	consCs    = cons/.repZs;
	initGuessCs = Join[iGuess[[1;;num]],
			   iGuess[[2*num+1;;3*num]]];

	(* Calculations*)
	success = Check[ 
		{resObjFunc, resRep} = findMinimum[
			{objFuncCs, consCs}, initGuessCs,
			MaxIterations->maxnum,
			StepMonitor:>StongStepMonitor[sys, monitorFunc],
			WorkingPrecision->wp];
		True,
		Message[StongFindMinimumOnlyCoef::fail, zSTO, nSTO, nGTO];
		False];	

	(* return calculation result*)
	If[success, 
	   { "success"-> success,
	     "zSTO"->("zSTO"/.args), "dist2"->resObjFunc,
	     "cs" -> sys[[-1,1]]/.resRep, "zs" -> z0s},
	   { "success"-> success, "zSTO"->("zSTO"/.args), "dist2"->resObjFunc }]]

(* ** FindMinimumForRange*)
(* *** options *)

Options[StongFindMinimumForStos] = { 
	WorkingPrecision->MachinePrecision,
        Method -> "InteriorPoint",
	"Amplitude"->1,
	MaxIterations->500,
	"ReZeta"->None, "ImZeta"->None,
	"SaveFile"->None };

(* *** main old*)
(* 
StongFindMinimumForStosOld[args_, OptionsPattern[]]:= Module[
	{ zStoList = "zStoList"/.args,
	  c0s      = "c0s"/.args,
	  z0s      = "z0s"/.args,
	  nGTO     = "nGTO"/.args,
	  nSTO     = "nSTO"/.args,
      findMin, f, x0},

	(* optimization*)
	findMin[method_][{"success"->True,"zSTO"->zSto0_, "dist2"->_
	                 ,"cs"->cs_,"zs"->zs_}, zSto1_]:=
        Block[{ nOld, nNew, csNew, args2
              },
          nOld = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto0, zSto0]];
		  nNew = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto1, zSto1]];
		  csNew = If[Abs[Arg[nNew / nOld]] < Pi/2, cs, -cs];
		  args2 = {"num"->Length[c0s], "nGTO"->nGTO,
			       "nSTO"->nSTO,       "zSTO"->zSto1,
			       "z0s"->zs,          "c0s"->csNew};			       
		  StongFindMinimum[args2,
				   WorkingPrecision->OptionValue[WorkingPrecision],
				   Method->method,
				   "ReZeta"->OptionValue["ReZeta"],
				   "ImZeta"->OptionValue["ImZeta"] ]];
    
    (* successfull step*)
    f[arg:{"success"->True, __}, a_] := With[{
            method = OptionValue[Method]},
            If[method == "Mix",
               findMin["InteriorPoint"][findMin["Newton"][arg, a], a],
               findMin[method][arg, a]]]
    
	(* failed step*)
	f[{"success"->False, ___}, _]:= Sequence[];
	f[Sequence[], _]:= Sequence[];

	(* initial value*)
	x0 = {"success"->True, "zSTO" ->zStoList[[1]],
	      "dist2"->None, "cs"->c0s, "zs"->z0s};

	(* computation and return*)
	FoldList[f, x0, zStoList][[2;;-1]]]
*)

(* *** main *)

StongFindMinimumForStos[args_, OptionsPattern[]] := Module[ 
  { zStoList = "zStoList"/.args,
    c0s      = "c0s"/.args,
    z0s      = "z0s"/.args,
    nGTO     = "nGTO"/.args,
    nSTO     = "nSTO"/.args,
    saveFile = OptionValue["SaveFile"],
    stongFindMin = StongFindMinimum,
    f, x0},

  (* successfull step*)
  f[{"success"->True,"zSTO"->zSto0_, "dist2"->_
    ,"cs"->cs_,"zs"->zs_}, zSto1_]:= Block[
      { nOld, nNew, csNew, args2, res},
      nOld = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto0, zSto0]];
      nNew = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto1, zSto1]];
      csNew = If[Abs[Arg[nNew / nOld]] < Pi/2, cs, -cs];
      args2 = {"num"->Length[c0s],
	       "nGTO"->nGTO,
	       "nSTO"->nSTO,
	       "zSTO"->zSto1,
	       "z0s"->zs,
	       "c0s"->csNew};
      res = stongFindMin[
	      args2,
	      "Amplitude"->OptionValue["Amplitude"],
	      WorkingPrecision->OptionValue[WorkingPrecision],
	      Method->OptionValue[Method],
	      MaxIterations->OptionValue[MaxIterations],
	      "ReZeta"->OptionValue["ReZeta"],
	      "ImZeta"->OptionValue["ImZeta"] ];
      If[Not[saveFile === None],
	 PutAppend[res, saveFile]];
      res ];

  (* failed step*)
  f[{"success"->False, ___}, _]:= Sequence[];
  f[Sequence[], _]:= Sequence[];

  (* initial value*)
  x0 = {"success"->True, "zSTO" ->zStoList[[1]],
	"dist2"->None, "cs"->c0s, "zs"->z0s};

  (* computation and return*)
  If[saveFile === None,
     FoldList[f, x0, zStoList][[2;;-1]],
     Put[{}, saveFile]; 	
     Fold[f, x0, zStoList][[2;;-1]]]]

(* *** main, support ratio*)

Options[StongFindMinimumForStosWithSupportRatio] = { 
	"ObjFunc"->"Constraints",
	WorkingPrecision->MachinePrecision,
        Method -> "InteriorPoint",
	"Amplitude"->1,
	MaxIterations->500,
	"ReZeta"->None, "ImZeta"->None,
	"SaveFile"->None };
StongFindMinimumForStosWithSupportRatio[args_, OptionsPattern[]] := Module[ 
  { zStoList = "zStoList"/.args,
    c0s      = "c0s"/.args,
    z0s      = "z0s"/.args,
    nGTO     = "nGTO"/.args,
    nSTO     = "nSTO"/.args,
    saveFile = OptionValue["SaveFile"],
    stongFindMin = If[OptionValue["ObjFunc"] == "Constraints",
		      StongFindMinimum, StongFindMinimumRatio],
    f, x0},

  (* successfull step*)
  f[{"success"->True,"zSTO"->zSto0_, "dist2"->_
    ,"cs"->cs_,"zs"->zs_}, zSto1_]:= Block[
      { nOld, nNew, csNew, args2, res},
      nOld = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto0, zSto0]];
      nNew = 1 / Sqrt[integ[STO,STO][2*nSTO, zSto1, zSto1]];
      csNew = If[Abs[Arg[nNew / nOld]] < Pi/2, cs, -cs];
      args2 = {"num"->Length[c0s],
	       "nGTO"->nGTO,
	       "nSTO"->nSTO,
	       "zSTO"->zSto1,
	       "z0s"->zs,
	       "c0s"->csNew};
      res = stongFindMin[
	      args2,
	      "Amplitude"->OptionValue["Amplitude"],
	      WorkingPrecision->OptionValue[WorkingPrecision],
	      Method->OptionValue[Method],
	      MaxIterations->OptionValue[MaxIterations],
	      "ReZeta"->OptionValue["ReZeta"],
	      "ImZeta"->OptionValue["ImZeta"] ];
      If[Not[saveFile === None],
	 PutAppend[res, saveFile]];
      res ];

  (* failed step*)
  f[{"success"->False, ___}, _]:= Sequence[];
  f[Sequence[], _]:= Sequence[];

  (* initial value*)
  x0 = {"success"->True, "zSTO" ->zStoList[[1]],
	"dist2"->None, "cs"->c0s, "zs"->z0s};

  (* computation and return*)
  If[saveFile === None,
     FoldList[f, x0, zStoList][[2;;-1]],
     Put[{}, saveFile]; 	
     Fold[f, x0, zStoList][[2;;-1]]]]

(* ** Restart*)
(* *** doc*)
(* 
   read a result from FindMinimumForRange function and
   restart calculation from most new sucss data.
*)

(* *** util *)

ConvertFromOldData[oldData_] := 
 Table[{"success" -> True, "zSTO" -> d[[2]], "dist2" -> d[[3]], 
   "cs" -> d[[4]], "zs" -> d[[5]]}, {d, oldData[[2]][[2 ;; -1]]}]

(* *** main*)

StongFindMinimumForStosRestart[prevRes_, dTheta_, 
				      {nSto_, nGto_}, opts___]:=
	StongFindMinimumForStosRestart[prevRes, {dTheta, Pi*89/180},
				       {nSto, nGto}, opts]

StongFindMinimumForStosRestart[prevRes_, {dTheta_, maxTheta_}, 
			       {nSto_, nGto_}, opts___] := Module[
  {newestSuccessResult, zSTO, cs, zs, zStoList},

  (* get newest suceessful result*)
  newestSuccessResult = Last[Select[prevRes, "success"/.#&]];
  
  (* extract information*)
  zSTO = "zSTO"/.newestSuccessResult;
  cs   = "cs"/.newestSuccessResult;
  zs   = "zs"/.newestSuccessResult;

  (* rebuild input data*)
  zStoList = With[{ t0 = -Arg[zSTO]},
		  Table[ Exp[-I t ], {t, t0, maxTheta, dTheta}]];
  With[{ arg = { "zStoList"->zStoList,
		 "c0s"->cs,
		 "z0s"->zs,
		 "nGTO"->nSto,
		 "nSTO"->nGto}},
       StongFindMinimumForStos[arg, opts]]]

(* *** only coef*)

StongFlipSign[nSto_, zSto0_, zSto1_, cs_] := With[{
   nOld = 1/Sqrt[integ[STO, STO][2*nSto, zSto0, zSto0]],
   nNew = 1/Sqrt[integ[STO, STO][2*nSto, zSto1, zSto1]]},
  If[Abs[Arg[nNew/nOld]] < Pi/2, cs, -cs]]

StongRestartOnlyCoef[{dataOld__, True}, rest___] :=  
	StongRestartOnlyCoef[ConvertFromOldData[dataOld], 
			     rest]

StongRestartOnlyCoef[dataUnconsList_, {nSto_, nGto_}, {opts___}, 
  "SaveFile" -> saveFile_] := Module[
  {f, x0},
  
  (*one step*)  
  f[{"success" -> True, 
     "zSTO" -> zSto0_, 
     "dist2" -> _, 
     "cs" -> cs_, 
     "zs" -> zs_}, 
    dataUncons_] := Block[{
	    zSto1, args2, res},
    zSto1 = "zSTO" /. dataUncons;
    args2 = {"nGTO" -> nGto,
      "nSTO" -> nSto,
      "zSTO" -> ("zSTO" /. dataUncons),
      "z0s" -> "zs" /. dataUncons,
      "c0s" -> StongFlipSign[nSto, zSto0, zSto1, cs]};
    res = StongFindMinimumOnlyCoef[args2, opts];
    PutAppend[res, saveFile];
    res];
  f[{"success" -> False, ___}, _] := Sequence[];
  f[Sequence[], _] := Sequence[];
  
  (*initial guess*)
  x0 = {"success" -> True,
	"zSTO" -> ("zSTO" /. dataUnconsList[[1]]),
	"dist2" -> None,
	"cs" -> ("cs" /. dataUnconsList[[1]]),
	"zs" -> ("zs" /. dataUnconsList[[1]])};
  
  (*Computation*)
  Put[saveFile];
  Fold[f, x0, dataUnconsList]]

(* ** supporter*)
(* *** 1s *)

StongFindMinimumForStos1s[ dTheta_, c0s_, z0s_, opts___] := With[
        { args = {"zStoList" -> Table[Exp[-I Pi i / 180], {i, 0, 89, dTheta}],
                  "nGTO" -> 1, "nSTO" -> 1, "c0s" -> c0s, "z0s" -> z0s} },
        StongFindMinimumForStos[args, opts]]

StongFindMinimumForStosTwo1s[dTheta_, opts___] := StongFindMinimumForStos1s[
        dTheta, { 0.68, 0.43}, { 0.15, 0.85}, opts]

StongFindMinimumForStosFour1s[dTheta_, opts___] := StongFindMinimumForStos1s[
        dTheta,
        { 0.2916173,0.5328422,0.2601414,0.05675211}, 
        {0.08801783, 0.2652005, 0.9546111, 5.216826}, opts]

StongFindMinimumForStosSix1s[dTheta_, opts___] := StongFindMinimumForStos1s[
        dTheta,
        { 0.13033406, 0.41649147, 0.37056273, 0.16853826, 0.049361481, 0.0091635938},
        { 0.06510954, 0.1580884, 0.40709891, 1.1850565, 4.2359158, 23.103033},
        opts]

StongFindMinimumForStosEight1s[dTheta_, opts___] := StongFindMinimumForStos1s[
        dTheta,
        {0.0615911410, 0.289265739, 0.377716777, 0.0386906709, 0.252257848, 0.113383080, 0.0101663591, 0.00182370874},
        { 0.052940630, 0.114110930, 0.2509528368, 4.3439473433, 0.586147451, 1.496071129, 15.51296253, 84.57815630},
        opts]

StongFindMinimumForStosTen1s[dTheta_, opts___] := StongFindMinimumForStos1s[
        dTheta,
        {0.03056309, 0.1928421,0.3352598,0.2939314,0.1723784,0.07832303,0.02951686,0.009349312,0.002388516,0.0004246811}, 
        { 0.045260,0.09022142,0.18017900,0.3725194,0.81061149569,1.8873144224,4.811717, 13.964627270,49.859495648,271.81382206},
        opts]

(* *** 3d *)

StongFindMinimumForStos3d[ dTheta_, c0s_, z0s_, opts___] := With[
        { args = {"zStoList" -> Table[Exp[-I Pi i / 180], {i, 0, 89, dTheta}],
                  "nGTO" -> 3, "nSTO" -> 3, "c0s" -> c0s, "z0s" -> z0s} },
        StongFindMinimumForStos[args, opts]]


StongFindMinimumForStosTen3d[dTheta_, opts___] := StongFindMinimumForStos3d[
        dTheta,
	{
		0.013894385895885511041, 0.128661309710192435369, 
		0.311323735143115952398, 0.3443980469396025005028, 
		0.225550230864927170001, 0.1010667526455426603772, 
		0.033521727085346984055, 0.008448556782467633302, 
		0.001561950914187207597, 0.000179527445601998439},
	{
		0.029249726257036236115, 0.049454972978029061441, 
		0.0824557874391766351360, 0.1396169754497786213867, 
		0.2435826472758530671689, 0.4434567167604209368699, 
		0.8560208092255193475438, 1.7966636748628097907842, 
		4.3034023918510209244068, 13.3693806213032469906752},
        opts]
(* *  STO-NG penalty*)
(* ** cost function*)

StongCostFunc[arg:{sto_, gtos_, __}, wOpList_] := Block[
   {stong, dist2, penalties},

   stong = Apply[Plus, gtos];
   dist2 = StongObjectiveFunc[arg];
   penalties = Table[With[
	   { w   = wOp[[1]], 
	     eleGto = CIP[stong,  wOp[[2]][stong]],
	     eleSto = CIP[sto, wOp[[2]]@sto] },
	   w * NumRe[(eleGto-eleSto) * IConjugate[eleGto-eleSto]]]
		    ,{wOp, wOpList}];
   dist2 + Apply[Plus, penalties]]

(* *  End      *)
(* 
End[]
EndPackage[]
*)

