<< "l2func.m";
P[a_] := Block[{}, Print[""]; Print[a]]
P[lbl_, a_] := Block[{}, Print[""]; Print[lbl]; Print[a]]
stepPrint[cs_, zs_] := Print[zs]

Print[];
Print["========== (None, None) ============="];
Print["SQP:"];

Print[StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
			 "c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			"ReZeta"->None, "ImZeta"->None,
			"MonitorFunction"->stepPrint,
			Method->"SQP" ]];

Print[];
Print["Newton:"];
Print[StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
			 "c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			"ReZeta"->None, "ImZeta"->None,
			"MonitorFunction"->stepPrint,
			Method->"Newton" ]];
Print[];
Print["InteriorPoint:"];
Print[StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
			 "c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			"ReZeta"->None, "ImZeta"->None,			
			"MonitorFunction" -> stepPrint,
			Method->"InteriorPoint" ]];


Print[];
Print[];
Print["========== (Log, None) ============="];

P["SQP (Log, None)"
 , StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
		      "c0s"->{0.5, 0.5}, "z0s"->{0.15, 0.85} },
		     "ReZeta"->Log, "ImZeta"->None,
		     Method->"SQP" ]];


P["SQP (Log, None)"
 , StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
		      "c0s"->{0.5, 0.5}, "z0s"->{0.15, 0.85} },
		     "ReZeta"->Log, "ImZeta"->None,
		     "MonitorFunction" -> stepPrint,
		     Method->"SQP" ]];

P["Interior Point(None, None) ", 
  StongFindMinimum[ {"nSTO"->1, "zSTO"->1, "nGTO"->1,
			  "c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
		    "MonitorFunction" -> stepPrint,
		    Method->"InteriorPoint" ]];



