<< "stong.m";

func = x^2 + y^2;
cons = {x+y-1==0};
guess= {{x, 0}, {y, 0}};

Print["FindMinimumSQP"];
resSQP = FindMinimumSQP[{func, cons}, guess,
                        StepMonitor:>Print["x,y:",x,y]];
Print[resSQP];
Print[];
Print["Mix"];
resMix = FindMinimumMix[{func, cons}, guess,
                        EvaluationMonitor:>Print["x,y: ", x,y]];
Print[resMix];

Print[];
Print["STONG"];
Print["SQP"];
z0s = { 0.06510954, 0.1580884, 0.40709891, 1.1850565, 4.2359158, 23.103033};
c0s = { 0.13033406, 0.41649147, 0.37056273, 0.16853826, 0.049361481, 0.0091635938};
resSQP = StongFindMinimum[
        { "num"->6,
		  "nGTO"->1,
		  "nSTO"->1,
		  "zSTO"->1,
		  "z0s"->z0s,
		  "c0s"->c0s},
         Method->"SQP"];
Print[resSQP];

Print["Mix"];
resMix = StongFindMinimum[
        { "num"->6,
		  "nGTO"->1,
		  "nSTO"->1,
		  "zSTO"->1,
		  "z0s"->z0s,
		  "c0s"->c0s},
         Method->"Mix"];
Print[resMix];


