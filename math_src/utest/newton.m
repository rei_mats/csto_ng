<< "../l2func.m" ;


res = BindBlock[{x, y}, {1, 2}, Hold[Print[x]; x + y]];
Print[res];

Print[""];
Print["find minimum test"]

res = FindMinimumSQP[{ y^2 + x^2, {x+y == 0}},
		     {{x, -1}, {y, 1}},
		     StepMonitor:>Print[x]];

Print[res];


