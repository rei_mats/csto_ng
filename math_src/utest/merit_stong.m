(* * include *)

<< "../stong.m";


(* * matrix *)

pre = Stong[None, None][CNormalized[STO][2, 1.1-0.1I], 
	    1, {{0.5, 0.5}, {0.2, 0.8}}];
{sto, gtos, guess, x, y} = pre;
stong = Apply[Plus, gtos];
cost = StongCostFunc[pre, {{2.1, OP[CST[3.1]]}}];
dist2 = StongObjectiveFunc[pre];

Print[StongNumericalEvaluate[pre, cost]];

ExpectNear[ StongNumericalEvaluate[pre, cost],
	    StongNumericalEvaluate[pre, dist2] +
	    StongNumericalEvaluate[pre, 2.1*3.1*3.1*Power[Abs[CNorm[sto]-CNorm[stong]],2]],
	    "cost1", 
	    0.0000001];

(* find min*)

pre = Stong[None, None][CNormalized[STO][2, 1.1-0.0], 
	    1, {{0.5, 0.5}, {0.16, 0.85}}];
cost = StongCostFunc[pre, {{0.01, OP[CST[1.0]]}}];
guess= StongInitGuess[pre];
resFindMinimum = FindMinimum[cost, guess, Method->"Newton", MaxIterations->500];
Print[StongShowResFindMinimum[pre, resFindMinimum]];

