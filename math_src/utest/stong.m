<< "../l2func.m";

res = StongFindMinimum[{
	"nGTO" -> 1,
	"nSTO" -> 1,
	"zSTO" -> 1,
	"z0s" -> {2/10, 8/10},
	"c0s" -> {1/2,  1/2}}];
Print[res]
