<< "../stong.m";

s1 = CNormalized[STO][2, 1.0-0.3I];
s2 = CNormalized[STO][3, 0.2-0.4I];
g1 = CNormalized[GTO][1, 1.0-0.1I];
g2 = CNormalized[GTO][2, 0.3-0.1I];

UnitTestCIP[s1,            s2, "s1_s2"];
UnitTestCIP[OP[DD[2]][s1], s2, "DD2_s1_s2"];

r0 = 23/10;
atR = AtR[r0];
ExpectNear[AtR[r0][OP[RM[2]][s1]], 
	   r0*r0*AtR[r0][s1],
	   "RM2 sto"];

DD2=OP[DD[2]];
ExpectNear[D[AtR[xxx][s1], {xxx,2}]/.xxx->r0,
	   AtR[r0][DD2@s1],
	   "D2D sto"];
ExpectNear[D[AtR[xxx][g1], {xxx, 2}]/.xxx->r0,
	   AtR[r0][DD2[g1]],
	   "D2 gto"];

(* 
If[Abs[calc - expe] < 0.000000001,
   Print["Success"],
   Print["Failed"]];
*)


