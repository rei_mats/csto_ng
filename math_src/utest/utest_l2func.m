(* ** sto *)

UTestCIP[a_, b_]:=Block[
	{ inte, calc, expe, eps, r, res},
	inte = AtR[r][a] * AtR[r][b];
	expe = NIntegrate[inte, {r,0,Infinity}];
	calc = CIP[a, b];
	eps = 10^(-9);
	res = If[Abs[calc] < eps,
		 Abs[expe] < eps,
		 Abs[(expe-calc)/expe] < eps];
	Assert[res];
	If[Not[res],Print[a]];
	If[Not[res],Print[b]];
	If[Not[res],Print[expe]];
	If[Not[res],Print[calc]];
	If[Not[res],Print[Abs[(calc-expe)/expe]]]
	res]
	

g1=CNormalized[GTO][2,1.1]
g2=CNormalized[GTO][1,1.2]
UTestInnerProduct[CIP, g1, g2]

s1 = OP[CST[1.2]][CNormalized[STO][1, 1.1-0.2I]] +
     OP[CST[-1.1]][CNormalized[STO][2, 0.2-0.4I]]
s2 = CNormalized[STO][2, 1.3]
UTestCIP[s1, s2]
UTestCIP[s1, g1]







