<< "../l2func.m";

res = StongFindMinimum[{
	"nGTO"->1,
	"nSTO"->1,
	"zSTO"->1,
	"z0s"->{2/10, 9/10},
	"c0s"->{1/2,  10/2}}, MaxIterations->3];
Print["test1:"];
Print[res];


res = StongFindMinimumLog[
	{"nGTO"->1,
	 "nSTO"->1,
	 "zSTO"->1,
	 "z0s"->{2/10, 8/10},
	 "c0s"->{1/2,   1/2}}];
Print[""];
Print["test2"];
Print[res];

res = StongFindMinimumForStos[
	{ "zStoList"->{1, 1-0.1I},
	  "c0s"->{1/2,  1/2},
	  "z0s"->{2/10, 8/10},
	  "nGTO"->1,
	  "nSTO"->1}];
Print[""];
Print["test3:"];
Print[res];

res = StongFindMinimumForStos[
	{ "Log"->True,
	  "zStoList"->{1, 1-0.1I},
	  "c0s"->{1/2,  1/2},
	  "z0s"->{2/10, 8/10},
	  "nGTO"->1,
	  "nSTO"->1}];
Print[""];
Print["test4:"];
Print[res];

   
