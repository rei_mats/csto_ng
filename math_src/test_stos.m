<< "stong.m";

zStoList = Table[Exp[-I Pi i/180], {i, 0, 1}];

Print[];
Print["SQP"];
Print[StongFindMinimumForStos[ {"nSTO"->1, "nGTO"->1, "zStoList"->zStoList,
				"c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			       "ReZeta"->None, 
			       "ImZeta"->None,
			       Method->"SQP",
			       "SaveFile"->"sqp.dat" ]];
(* 
Print[];
Print["IP"];
Print[StongFindMinimumForStos[ {"nSTO"->1, "nGTO"->1, "zStoList"->zStoList,
				"c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			       "ReZeta"->None, "ImZeta"->None,
			       Method->"InteriorPoint" ]];

Print[];
Print["SQP (0-40)"];
zStoList = Table[Exp[-I Pi i/180], {i, 0, 40, 1}];
Print[StongFindMinimumForStos[ {"nSTO"->1, "nGTO"->1, "zStoList"->zStoList,
				"c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			       "ReZeta"->None, "ImZeta"->None,
			       Method->"SQP" ]];


Print[];
Print["SQP with large width"];
zStoList = Table[Exp[-I Pi i/180], {i, 0, 60, 5}];
Print[StongFindMinimumForStos[ {"nSTO"->1, "nGTO"->1, "zStoList"->zStoList,
				"c0s"->{0.68, 0.43}, "z0s"->{0.15, 0.85} },
			       "ReZeta"->None, "ImZeta"->None,
			       Method->"SQP" ]];
*)


