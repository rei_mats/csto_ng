# *  Begin     

from int import *
import unittest
from sympy import N, pi, integrate, Symbol, exp, oo, re, im
from test_utils import *

import scipy.integrate as sp_int
from scipy import Inf


# *  Utils     
# ** int_sto

def analytic_int_sto(n, z):
    r = Symbol('r')
    sto = r**n * exp(-z*r)
    res = integrate(sto, (r,0,oo))
    return res

    
# ** int gto

def analytic_int_gto(n, z):
    r = Symbol('r')
    gto = r**n * exp(-z*r*r)
    res = integrate(gto, (r,0,oo))
    return res

# ** int sto gto

def numerical_int_sto_gto(n, zs, zg, prec = None):
#    r = Symbol('r')
#    sg = (r**n) * exp(-zg * r*r -zs * r)
#    return N(integrate(sg, (r,0,oo)), prec)

    def lambda_sg(x):
        return x**n * exp(-zg * x * x - zs* x)

    def re_sg(x):
        return re(lambda_sg(x))
    def im_sg(x):
        return im(lambda_sg(x))
    re_res = sp_int.quad(re_sg, 0, Inf)
    im_res = sp_int.quad(im_sg, 0, Inf)
    return complex(re_res[0], im_res[0])
        
    """        
    if(isinstance(zs, complex) or isinstance(zg, complex)):
        def re_sg(x):
            return re(lambda_sg(x))
        def im_sg(x):
            return im(lambda_sg(x))
        re_res = sp_int.quad(re_sg, 0, Inf)
        im_res = sp_int.quad(im_sg, 0, Inf)
        return complex(re_res[0], im_res[0])
        
    else:
        quad_res = sp_int.quad(lambda_sg, 0, Inf)
        return quad_res[0]
    """
    
    
# *  TestClass 
# ** begin

class IntClassTestCase(unittest.TestCase):
    def setUp(self):
        pass
        
    def tearDown(self):
        pass
        

# ** utils
# *** sto

    def _test_int_sto(self, n, zz, prec=None):
        if(prec):
            z = N(zz, prec)
            eps = 0.5**(prec-1)
        else:
            z = zz
            eps = 10.0**(-10)
        expe = int_sto(n, z)
        calc = analytic_int_sto(n, z)

        assertNear(self, expe, calc, eps_for(prec))


# *** gto
        
    def _test_int_gto(self, n, zz, prec=None):
        z   = N(zz, prec)
        calc = int_gto(n, z, prec)
        expe = analytic_int_gto(n, z)

        assertNear(self, expe, calc, eps_for(prec))


# *** sto_gto

    def _test_int_sto_gto(self, n, zzs, zzg, prec=None):

        zs   = N(zzs, prec)
        zg   = N(zzg, prec)
        
        calc = int_sto_gto(n, zs, zg, prec)
        expe = numerical_int_sto_gto(n, zs, zg, prec)

        assertNear(self, expe, calc, eps_for(prec))
        

# ** test
        
    def testFactorial(self):
        self.assertEqual(1, d_factorial(0))
        self.assertEqual(1, d_factorial(1))
        self.assertEqual(2, d_factorial(2))
        self.assertEqual(3, d_factorial(3))
        self.assertEqual(8, d_factorial(4))
        self.assertEqual(15, d_factorial(5))
        self.assertEqual(48, d_factorial(6))

        
    def testStoInt(self):
        self._test_int_sto(1, 1.1)
        self._test_int_sto(2, 2.1, 50)
        self._test_int_sto(1, 2-2j)
        self._test_int_sto(4, 2-0.2j, 50)


    def testGtoInt(self):
        self._test_int_gto(1, 1.1)
        self._test_int_gto(2, 1.1, 50)
        self._test_int_gto(3, 1.1, 100)
        self._test_int_gto(4, 0.2-0.01j)
        self._test_int_gto(5, 0.3-1.1j, 50)

    def testStoGtoInt(self):

        self._test_int_sto_gto(0, 1.1, 1.2)
        self._test_int_sto_gto(1, 1.1, 1.2)
        self._test_int_sto_gto(2, 1.1, 1.2)
        self._test_int_sto_gto(3, 1.1, 1.2)
        self._test_int_sto_gto(4, 1.1, 1.2)
        self._test_int_sto_gto(5, 1.1, 1.2)
        self._test_int_sto_gto(6, 1.1, 1.2)

        
        self._test_int_sto_gto(0, 1.1-0.2j, 1.2)
        self._test_int_sto_gto(1, 1.1-0.2j, 1.2)
        self._test_int_sto_gto(2, 1.1-0.2j, 1.2, 50)
        self._test_int_sto_gto(3, 1.1-0.2j, 1.2, 50)  
        self._test_int_sto_gto(4, 1.1-0.2j, 1.2, 50)


# *  Main      

if __name__ == "__main__":
    unittest.main()
