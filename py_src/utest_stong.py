# *  Begin    

import unittest
from test_utils import *
from stong import *
import time

# *  TestCase 
# ** begin

class StongTestCase(unittest.TestCase):
    def setUp(self):
        self.xs = [0, 2, 1, 1, 1.2, 3, 1, 0, 0, 0, 0.2, 0]
        self.stong = STO_NG(3, 2, 1+2j, 2)
        self.stong.set_cs([0+1j, 2+0j,     1+0j])
        self.stong.set_zs([1+0j, 1.2+0.2j,1.3+0j ])

    def tearDown(self):
        pass


# ** distance2
# *** member method

    def test_distance2(self):
        
        timer = MonoTimer("distance2_member")
        
        v, dv = self.stong.distance2()
        calc = v
        expe = 82.8333 # obtained with mathematica
        eps  =  0.0001
        assertNear(self, expe, calc, eps)

        expe =  [11.4004, 11.5965, 10.5577, 
                 5.60916, -10.3742, -3.30187,
                 3.26823, 4.97534, 4.45947,
                 1.95181, 7.17376, 2.62035]
        calc = dv
        for (e, c) in zip(expe, calc):
            assertNear(self, e, c, eps)

        timer.end_and_print()


# *** external function

    def test_distance2_external(self):

        timer = MonoTimer("distance2_external")

        v,dv = stong_objective_function(self.xs, self.stong, True)
        h    = 0.0001

        for i in range(3*4):
            xps    = self.xs[:]  # copy list
            xms    = self.xs[:]  # copy list
            xps[i] += h
            xms[i] -= h
            vp, dvp = stong_objective_function(xps, self.stong)
            vm, dvm = stong_objective_function(xms, self.stong)
            expe = (vp - vm) / (2 * h)
            calc = dv[i]
            assertNear(self, expe, calc, h)

        timer.end_and_print()


# ** cnorm equal 1

    def test_abs2_dif_cnorm2(self):

        timer = MonoTimer("abs2_dif_cnorm2")
        
        v,dv = stong_constraint_function(self.xs, self.stong)
        h    = 0.0001

        for i in range(3*4):
            xps    = self.xs[:]  # copy list
            xms    = self.xs[:]  # copy list
            xps[i] += h
            xms[i] -= h
            vp, dvp = stong_constraint_function(xps, self.stong)
            vm, dvm = stong_constraint_function(xms, self.stong)
            expe = (vp - vm) / (2 * h)
            calc = dv[i]
            assertNear(self, expe, calc, h)

        timer.end_and_print()

        
# *  Main     

if __name__ == "__main__":
    unittest.main()


