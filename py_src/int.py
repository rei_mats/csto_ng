# *  Begin      

from math import factorial
from sympy import pi, sqrt, erfc, exp, N
import math


# *  functions  
# ** d_factorial

def d_factorial(n):
    if (n == 0):
        return 1
    if (n <= 2):
        return n
    else:
        return n * d_factorial(n-2)
    

# ** int_sto
        
def int_sto(n, z, prec=None):
    return N(z ** (-1-n) * factorial(n), prec)


# ** int_gto

def int_gto(n, z, prec=None):
    if(n % 2 == 0):
        nn = n / 2
        acc = 1
        acc*= d_factorial(2*nn-1)
        acc*= N(sqrt(pi), prec)
        acc/= 2**(nn+1) * sqrt(z) ** (2*nn+1)
        return N(acc, prec)
    else:
        nn = (n-1)/2
        res= factorial(nn) / (2 * z ** (nn+1))
        return N(res, prec)


# ** int sto and gto

def int_sto_gto(n, zs, zg, prec=None):
    sqrt_v = N(sqrt(zg), prec)
    z =      N(zs / (2 * sqrt_v), prec)
    zz=      N(zs * zs / ( 4 * zg), prec)
    erfc_v = N(erfc(z), prec)
    exp_v  = N(exp(zz), prec)
    pi_v =   N(pi, prec)
    t1 = exp_v * erfc_v * sqrt(pi_v)
        
    if(  n == 0):
        return N(t1 / (2 * sqrt_v), prec)
    elif(n == 1):
        return N(1/(2*zg) - t1 * zs/ (4 * zg * sqrt_v), prec)
    elif(n == 2):
        return N((-2*sqrt_v*zs + (2*zg+zs**2) * t1)/ (8*sqrt_v**5),prec)
    elif(n == 3):
        t2 = (4 * zg + zs**2) / (8 * zg**3)
        t3 = zs * (6 * zg + zs**2) * t1
        t4 = N(16 * sqrt_v ** 7, prec)
        return t2 - t3 / t4
    elif(n == 4):
        t2 = N(-2*sqrt_v * zs * (10 * zg + zs**2), prec)
        t3 = N((12*zg**2 + 12*zg*zs**2 + zs**4) * t1, prec)
        t4 = N(32*sqrt_v**9, prec)
        return N((t2 + t3) / t4, prec)
    elif(n == 5):
        t2 = 2 * sqrt_v * (2 * zg + zs**2) * (16 * zg + zs**2)
        t3 = -zs * (60 * zg**2 + 20 * zg * zs**2 + zs**4)
        t4 = 64 * sqrt_v**11
        return N( (t2 + t3 * t1) / t4, prec)
    elif(n == 6):
        t2 = -2 * sqrt_v * zs * (6 * zg + zs**2) * (22 * zg + zs**2)
        t3 = 120 * zg**3 + 180 * zg**2 * zs**2 + 30 * zg * zs**4 + zs**6
        t5 = 128 * sqrt_v**13
        return N( (t2 + t3 * t1) / t5, prec)
    else:
        print "Error:int.py:int_sto_gto, inputted n is not supported"
        print "n = " + str(n)
        exit(1)
        


