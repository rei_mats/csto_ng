# *  Begin     


from numpy import dot, array, hstack
from sympy import N, re, im
import scipy
import scipy.optimize as opt
import time

from ao import GTO, STO, IP, IP_d0d1
import int
from test_utils import MonoTimer


# *  Utils     
# ** dot product

def tri_dot(a, X, b):
    return dot(a, dot(X, b))


# ** complex jacobian

def complex_jacb_to_real_jacb(df):
    # df/dx = dz/dx df/dz + dz*/dx df/dz*
    #       = df/dz + df/dz*
    #       = 2Re[df/dz]
    # df/dy = dz/dy df/dz + dz*/dy df/dz*
    #       = i df/dz - i df/dz*
    #       = -2Im[df/dz]
    return [2 * re(d) for d in df] + [-2 * im(d) for d in df]
    

# *  Class     
# ** begin

class STO_NG:
    """Slater type orbital expanded by GTO"""


# ** constructor

    def __init__(self, numGto, nGto, zSto, nSto, prec=None):
        self.prec = prec
        self.numGto    = numGto
        self.nGto = nGto
        self.cs        = []
        self.gtos      = []
        self.conj_cs   = []
        self.conj_gtos = []
        
        for n in range(numGto):
            self.cs.append(1+0j)
            self.conj_cs.append(1+0j)
            self.gtos.append(GTO(nGto,1-0j))
            self.conj_gtos.append(GTO(nGto, 1-0j))

        self.sto      = STO(nSto, zSto)
        self.conj_sto = self.sto.conjugate()
        

# ** accessor

    def set_cs(self, cs):
        self.cs = [N(c, self.prec) for c in cs]
        self.conj_cs = [c.conjugate() for c in self.cs]

    def set_zs(self, zs):
        gs = [GTO(self.nGto, N(z, self.prec)) for z in zs]
        self.gtos      = gs
        self.conj_gtos = [g.conjugate() for g in gs]
        

# ** objective function
# *** distance2

    def distance2(self):
        # mu = ci or zi
        # d_mu<s-g, s-g> = -<s-g, d_mu g>
        # d_ci<s-g, s-g> = -<s,gi> + sum_j cj^*<gj,gi>
        # d_zi<s-g, s-g> = -ci <s,gi'> + sum_j cj^*ci<gj,gi'>

        # store
        c_gs = self.conj_gtos
        gs   = self.gtos
        cs   = self.cs
        c_cs = self.conj_cs

        # compute necessary matrix
        g0_g0 = array([[IP(i, j)      for j in gs] for i in c_gs])
        g0_g1 = array([[IP_d0d1(i, j) for j in gs] for i in c_gs])
        s_g0  = array([ IP(     self.conj_sto, i) for i in gs])
        s_g1  = array([ IP_d0d1(self.conj_sto, i) for i in gs])
        ss    = IP(self.conj_sto, self.sto)
        
        # compute norm
        c_gg_c = tri_dot(c_cs, g0_g0, cs)
        sg_c   = dot(s_g0, cs)
        dist2  = ss - 2 * re(sg_c) + c_gg_c

        # compute jacobian
        dc_dist2 = -s_g0        + dot(c_cs, g0_g0) 
        dz_dist2 = -(cs * s_g1) + dot(c_cs, g0_g1) * cs
        d_dist2  = array(complex_jacb_to_real_jacb(hstack((dc_dist2, dz_dist2))))

        # return
        def N_prec(x):
            return N(re(x), self.prec)
        return N_prec(dist2), map(N_prec, d_dist2)
        

# *** cnorm equal 1

    def abs2_dif_cnorm2(self):
        # dif      = (1 - (g,g))(1-(g*,g*))
        # d_mu dif =  -2*(g, d_mu g) (1 - (g*,g*))
        # d_ci dif =  -2*(g, gi) (1 - (g*,g*))
        # d_zi dif =  -2*(g, gi')ci (1 - (g*,g*))

        # store
        gs   = self.gtos
        cs   = self.cs
#        g0_g0 = [[IP(     i, j) for j in gs] for i in gs]
#        g0_g1 = [[IP_d0d1(i, j) for j in gs] for i in gs]
        g0_g0 = array([[IP(     i, j) for j in gs] for i in gs])
        g0_g1 = array([[IP_d0d1(i, j) for j in gs] for i in gs])

        # matrix
        c_gg_c = tri_dot(cs, g0_g0, cs)
        dif   = 1 - c_gg_c

        # jacobian
        d_ci = -2 * dot(cs, g0_g0) *      dif.conjugate()
        d_zi = -2 * dot(cs, g0_g1) * cs * dif.conjugate()
        d_dif = complex_jacb_to_real_jacb(hstack((d_ci, d_zi)))

        # return
        def N_prec(x):
            return N(re(x), self.prec)
        return N_prec(abs(dif)**2), map(N_prec, d_dif)
        
        
        
# *  Objective Function 
# ** set variable

def stong_set_variables(xs, stong, time_measure = False):

    if(time_measure):
        timer = MonoTimer("stong_set_variable")

    num = stong.numGto
    r_cs = xs[0    :1*num]
    r_zs = xs[1*num:2*num]
    i_cs = xs[2*num:3*num]
    i_zs = xs[3*num:]

    cs = [complex(r,i) for (r,i) in zip(r_cs, i_cs)]
    zs = [complex(r,i) for (r,i) in zip(r_zs, i_zs)]

    stong.set_cs(cs)
    stong.set_zs(zs)

    if(time_measure):
        timer.end_and_print()
    

# ** objective function

def stong_objective_function(xs, *args):

    if(len(args) == 0):
        print "stong_objective_function need at least one args"
        print "and it must be stong object"
        exit(1)
    elif(len(args) == 1):
        time_measure = False
    else:
        time_measure = args[1]

    if(time_measure):
        timer = MonoTimer("stong_objective_function")
        
    stong = args[0]
#    stong_set_variables(xs, stong, time_measure)
    stong_set_variables(xs, stong)

    if(time_measure):
        timer.end_and_print()

    func,jacb = stong.distance2()
    print func
    return func, jacb
    

# ** constrait function

def stong_constraint_function(xs, *args):
    stong = args[0]
    stong_set_variables(xs, stong)
    return stong.abs2_dif_cnorm2()

def stong_constraint_functionn_func(xs, *args):
    stong = args[0]
    stong_set_variables(xs, stong)
    f, df = stong.abs2_dif_cnorm2()
    return f

def stong_constraint_functionn_jacb(xs, *args):
    stong = args[0]
    stong_set_variables(xs, stong)
    return stong.abs2_dif_cnorm2()[1]    


# *  Main       

if(__name__ == "__main__"):

    #xs = array([0.678, 0.430, 0.152, 0.85, 0.0, 0.0, 0.0, 0.0])
#    xs = [0.678, 0.430, 0.152, 0.85, 0.0, 0.0, 0.0, 0.0]
    xs = [ 3.0563099585085784694617614389146e-02,
           1.9284210319013242168405145192621e-01,
           3.3525981600483858598342327596165e-01,
           2.9393144313939930412561381199641e-01,
           1.7237844894242919746702320825092e-01,
           7.8323035729579973975867413783945e-02,
           2.9516866258886379718764624874654e-02,
           9.3493121435555789040372146451962e-03,
           2.3885162479696687136185519472147e-03,
           4.2468112542941989825877036465446e-04,
           4.5260926344619282081937584545374e-02,
           9.0221425134484151706972727956614e-02,
           1.8017905240137490517904576209621e-01,
           3.7251949091192387109113176125857e-01,
           8.1061149569144440263664061048876e-01,
           1.8873144224037270584047189779649e+00,
           4.8117173073848102431405834831257e+00,
           1.3964627270218432514104057021134e+01,
           4.9859495648874206676473528751902e+01,
           2.7181382206208894869571589001028e+02,
           0, 0, 0, 0, 0,
           0, 0, 0, 0, 0,
           0, 0, 0, 0, 0,
           0, 0, 0, 0, 0]
    
    stong = STO_NG(len(xs)/4, 1, 1.0-0.01j, 1, None)
    cons=({'type': 'eq', 
           'fun':stong_constraint_functionn_func,
           'jac':stong_constraint_functionn_jacb,
           'args':(stong,)}, )
    optres = opt.minimize(stong_objective_function, 
                          xs, 
                          args=(stong,), 
                          jac=True,
                          method = 'SLSQP', 
                          constraints=cons,
                          options={'disp': True})

#    optres = opt.fmin_l_bfgs_b(stong_objective_function, xs, args=(stong,))

    print optres
                      

    
    
