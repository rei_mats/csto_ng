# *  Begin  
# ** import

from int import int_sto, int_gto, int_sto_gto
from sympy import sqrt, exp, N


# *  STO    

class STO:
    """Slater type orbital"""
    def __init__(self, n, z, prec=None):
        self.n = n
        self.z = z
        self.n_term = N(1 / sqrt(int_sto(2*n, 2*z)), prec)


    def at_r(self, r):
        res = self.n_term
        res*= r ** self.n
        res*= exp(-self.z * r)
        return res


    def conjugate(self):
        s = STO(self.n, self.z.conjugate())
        return s
        

# *  GTO    

class GTO:
    """Gauss type orbital"""
    def __init__(self, n, z, prec=None):
        self.n = n
        self.z = z
        if(prec):
            self.n_term = N(1 / sqrt(int_gto(2*n, 2*z)), prec)
        else:
            self.n_term = 1 / sqrt(int_gto(2*n, 2*z))

    def at_r(self, r):
        res = self.n_term
        res*= r ** self.n
        res*= exp(-self.z * r * r)
        return res

    def conjugate(self):
        g = GTO(self.n, self.z.conjugate())
        return g


# *  LCAO   

class LCAO:
    """linear combination of AO"""
    def __init__(self):
        self.coef_ao_list = []

    
    def add(self, coef_ao):
        self.coef_ao_list.append(coef_ao)

    def get_coef_i(self, i):
        return self.coef_ao_list[i][0]
    
    def get_ao_i(selfk, i):
        return self.coef_ao_list[i][1]

    def set_coef_i(self, i, c):
        self.coef_ao_list[i][0] = c

    def set_zeta_i(self, i, z):
        self.coef_ao_list[i][1].z = z

    def at_r(self, r):
        def coef_ao_at_r(coef_ao):
            c,ao = coef_ao
            return c * ao.at_r(r)
        xs = map(coef_ao_at_r, self.coef_ao_list)
        return sum(xs)


# *  Inner product 
# ** inner product

def IP(a, b, prec=None):

    if ( isinstance(a, STO) and isinstance(b, STO)):
        prim = int_sto(a.n + b.n, a.z + b.z, prec)
    elif(isinstance(a, GTO) and isinstance(b, GTO)):
        prim = int_gto(a.n + b.n, a.z + b.z, prec)
    elif(isinstance(a, STO) and isinstance(b, GTO)):
        prim = int_sto_gto(a.n + b.n, a.z, b.z, prec)
    elif(isinstance(a, GTO) and isinstance(b, STO)):        
        prim = int_sto_gto(a.n + b.n, b.z, a.z, prec)
    else:
        print "Error:ao.py:IP, this type is not implemented"
        exit(1)
    return N(a.n_term * b.n_term * prim, prec)

# ** inner product fro derivative basis
    
def IP_d0d1(a, b, prec=None):
    
    t0 = (0.25 + b.n * 0.5) / b.z * b.n_term
    t1 = -b.n_term
    
    if( isinstance(a, GTO) and isinstance(b, GTO)):
        acc = t0 * int_gto(a.n + b.n, a.z + b.z)
        acc+= t1 * int_gto(a.n + b.n + 2, a.z + b.z)
    elif(isinstance(a, STO) and isinstance(b, GTO)):
        acc = t0 * int_sto_gto(a.n + b.n,     a.z, b.z)
        acc+= t1 * int_sto_gto(a.n + b.n + 2, a.z, b.z)
    else:
        print "Error:ao.py:IP_d0d1, this type is not implemented."
        exit(1)

    return a.n_term * acc


