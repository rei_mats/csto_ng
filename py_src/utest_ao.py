# *  Begin     

from ao import *
import unittest
from sympy import *
from test_utils import *
    

# *  TestCase  
# ** begin

class AoClassTestCase(unittest.TestCase):
    def setUp(self):
        pass
    

    def tearDown(self):
        pass


# ** utils
# *** STO::at_r

    def _test_sto_at_r(self, n, zz, rr, prec=None):
        if(prec):
            z = N(zz, prec)
            eps = 0.5**(prec-1)
        else:
            z = zz
            eps = 10.0**(-10)

        sto_obj = STO(n, z, prec)
        r = Symbol('r')
        sto_sym = sto_obj.at_r(r)

        expe = sto_sym.subs([(r, rr)])
        calc = sto_obj.at_r(rr)
        
        assertNear(self, expe, calc, eps)

        
# *** GTO::at_r

    def _test_gto_at_r(self, n, zz, rr, prec=None):
        if(prec):
            z = N(zz, prec)
            eps = 0.5**(prec-1)
        else:
            z = zz
            eps = 10.0**(-10)
        r = Symbol('r')
        gto_obj = GTO(n, z, prec)
        gto_sym = gto_obj.at_r(r)

        expe = gto_sym.subs([(r, rr)])
        calc = gto_obj.at_r(rr)
        assertNear(self, expe, calc, eps)


# *** inner product

    def _test_ip(self, fa, na, za, fb, nb, zb, prec=None):

        a = fa(na, N(za, prec))
        b = fb(nb, N(zb, prec))
        
        r = Symbol('r')
        ar = a.at_r(r)
        br = b.at_r(r)

        calc = IP(a, b, prec)
        expe = N(integrate(ar * br, (r, 0, oo)), prec)
        eps  = eps_for(prec)
        
        assertNear(self, expe, calc, eps)

# *** deriv (finite difference)

    def _test_deriv(self, fa, na, za, fb, nb, zb, h, eps, prec=None):
        a  = fa(na, za)
        b  = fb(nb, zb)
        bp = fb(nb, zb + h)
        bm = fb(nb, zb - h)
        
        v0 = IP(a, b)
        vp = IP(a, bp)
        vm = IP(a, bm)

        calc = IP_d0d1(a, b, prec)
        expe = (vp - vm) / (2 * h)

        assertNear(self, expe, calc, eps)

        
# ** test
# *** at r

    def test_at_r(self):
        self._test_sto_at_r(2, 1.1, 1.1)
        self._test_sto_at_r(1, 1.1, 1.1, 50)
        self._test_sto_at_r(3, 0.2-0.3j, 1.1, 50)
        self._test_gto_at_r(2, 1.1, 1.1)
        self._test_gto_at_r(1, 0.2-0.3j, 1.2, 50)
        self._test_gto_at_r(3, 1.1, 4.3, 50)
        
        lcao = LCAO()
        lcao.add((1, GTO(1, 1.1)))
        lcao.add((2, GTO(1, 1.2)))
#        print lcao.at_r(1.3)


# *** inner product

    def test_inner_product(self):

        self._test_ip(STO, 1, 1.2, STO, 2, 0.2)
        self._test_ip(STO, 1, 1.2, STO, 2, 0.2, 50)
        self._test_ip(STO, 1, 1.2,  STO, 1, 0.2, 50)

        self._test_ip(GTO, 1, 1.2,      GTO, 2, 0.2)
        self._test_ip(GTO, 1, 1.2,      GTO, 2, 0.2, 50)
        self._test_ip(GTO, 1, 1.2-0.2j, GTO, 1, 0.2)

        self._test_ip(STO, 1, 1.2,      GTO, 2, 0.2)
        self._test_ip(STO, 1, 1.2,      GTO, 2, 0.2, 50)

        self._test_ip(GTO, 1, 1.2,      STO, 2, 0.2)
        self._test_ip(GTO, 1, 1.2,      STO, 2, 0.2, 50)


# *** differential inner product

    def test_deriv_inner_prod(self):
        self._test_deriv(STO, 1, 1.2, GTO, 1, 0.2, 0.000001, 0.000001)
        self._test_deriv(GTO, 1, 1.2, GTO, 1, 0.2, 0.000001, 0.000001)

        self._test_deriv(STO, 1, 1.2, 
                         GTO, 1, 0.2 - 0.1j, 
                         0.000001, 0.000001)    
        self._test_deriv(GTO, 1, 1.2-0.1j, 
                         GTO, 1, 0.2-0.2j, 
                         0.000001, 0.000001)
        
        self._test_deriv(STO, 1, 1.2-0.1j, 
                         GTO, 1, 0.2-0.2j, 
                         0.000001, 0.000001, 50)        
        self._test_deriv(GTO, 1, 1.2-0.1j, 
                         GTO, 1, 0.2-0.2j, 
                         0.000001, 0.000001, 50)

        

# *  Main      

if __name__ == "__main__":
    unittest.main()
        


