def assertNear(utest, expe, calc, eps):

    dist = abs(expe - calc)

    if(dist > eps):
        print "expe and calc are too different"
        print "expe:" + str(expe)
        print "calc:" + str(calc)
    utest.assertTrue(dist < eps)


def eps_for(prec = None):
    if(prec):
        return 0.5**(prec-1)
    else:
        return 10.0 ** (-10)

import time
class MonoTimer:
    """time for one statement"""
    def __init__(self, label):
        self.label = label
        self.t0 = time.time()

    def end_and_print(self):
        t1 = time.time()
        dt = t1 - self.t0
        print "time({0}) : {1}".format(self.label, dt)
