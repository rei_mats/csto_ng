X0 = 2
D  = -4
A0 = 1.0
Z = 0.5

def f_x (x)
  x*x
end

def df_x(x)
  2 * x
end

def ddf_x(x)
  2
end

def ak(k)
  A0 * (0.5 **k)
end

def lhe(k)
  f_x(X0 + ak(k) * D)
end
def rhe(k)
  f_x(X0) + Z * ak(k) * df_x(X0) * D
end


10.times do |k|
#  printf "%d, %8.8f, %8.8f, %8.8f\n", k, ak(k), lhe(k), rhe(k)
  puts "#{k}, #{ak(k)}, #{lhe(k)}, #{rhe(k)}"
end
