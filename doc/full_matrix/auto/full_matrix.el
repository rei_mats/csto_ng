(TeX-add-style-hook
 "full_matrix"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("jsarticle" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("graphicx" "dvipdfmx")))
   (TeX-run-style-hooks
    "latex2e"
    "jsarticle"
    "jsarticle10"
    "graphicx"
    "braket")
   (TeX-add-symbols
    "vector"
    "braces"
    "bracem"
    "braceb"
    "lefts"
    "rights"
    "leftb"
    "rightb"
    "leftm"
    "rightm")
   (LaTeX-add-bibliographies
    "library"
    "books"
    "paper")))

