// *  Begin     

#ifndef AO_HPP
#define AO_HPP

#include <vector>

#define PETSC_DESIRE_COMPLEX
#include <petsc.h>
#include <petscsys.h>
#include <petscvec.h>

#include "mathfunc.hpp"

namespace csto_ng
{
namespace ao
{

  using std::vector;
  using std::pair;
  

// *  integrate 
// ** STO
  
  PetscComplex int_sto(PetscInt n, PetscComplex z);  


// ** GTO

  PetscComplex int_gto(PetscInt n, PetscComplex z);
    

// ** STO-GTO

  PetscComplex int_sto_gto_0(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_1(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_2(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_3(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_4(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_5(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_6(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_7(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_8(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto_9(PetscComplex, PetscComplex);
  PetscComplex int_sto_gto(int, PetscComplex, PetscComplex);

  
// *  STO       
// ** begin
  
class STO
{

// ** field

private:
  
  PetscComplex z_;
  PetscInt     n_;
  PetscComplex nterm_;


// ** Accessor

public:
  
  PetscComplex& z() { return z_; }
  const PetscComplex& z() const { return z_; }

  PetscInt& n() { return n_; }
  const PetscInt& n() const { return n_; }

  const PetscComplex nterm() { return nterm_; }

  
// ** member function

  void Normalize();
  PetscComplex AtR(PetscReal r) const;  
  STO Conj() const;
  

// ** constructor etc

public:

  STO(PetscComplex _nterm, PetscInt _n, PetscComplex _z):
    z_(_z), n_(_n), nterm_(_nterm)  {}

  STO(PetscInt _n, PetscComplex _z) :
    z_(_z), n_(_n) {
    Normalize();
  }
  
  
// ** end

};


// *  GTO       
// ** begin

  class GTO
  {

    
// ** field

  private:

    PetscComplex z_;
    PetscInt     n_;
    PetscComplex nterm_;


// ** accessor

  public:

    PetscComplex& z() { return z_; }
    const PetscComplex& z() const { return z_; }

    PetscInt& n() { return n_; }
    const PetscInt& n() const { return n_; }

    const PetscComplex& nterm() { return nterm_; }
    
    
// ** member function

    void Normalize();
    PetscComplex AtR(PetscReal r) const;
    GTO Conj() const;
    

// ** constructor

    GTO(PetscComplex _nterm, PetscInt _n, PetscComplex _z):
      z_(_z), n_(_n), nterm_(_nterm) {}
    
    GTO(PetscInt _n, PetscComplex _z):
      z_(_z), n_(_n) {
      Normalize();
    }
      


// ** end

  };



// *  GTOs      
// ** begin

  class LCombGTO
  {

// ** member

  private:

    vector<pair<PetscComplex, GTO> > c_gto_list_;


// ** accessor

  public:

    PetscComplex& c_i(int i) { return c_gto_list_[i].first; }
    const PetscComplex& c_i(int i) const{ return c_gto_list_[i].first;}
    
    void set_z_i(int i, PetscComplex z);
    const PetscComplex& z_i(int i) { return c_gto_list_[i].second.z();}

    const GTO& gto_i(int i) { return c_gto_list_[i].second; }

    void Add(PetscComplex,const GTO& );
      

// ** member function

    PetscComplex AtR(PetscReal r) const;    
    LCombGTO Conj() const;
    

// ** end
    
  };
  


// *  inner prod
  
  PetscComplex CIP(const GTO& a, const GTO& b);
  PetscComplex CIP(const GTO& a, const STO& b);
  PetscComplex CIP(const STO& a, const STO& b);
  PetscComplex CIP(const STO& a, const GTO& b);


// *  End       
}
}
#endif
