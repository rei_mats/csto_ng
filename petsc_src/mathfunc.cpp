// *  Begin      

#include "mathfunc.hpp"

namespace csto_ng
{
namespace mathfunc
{

  using std::cout;
  using std::cerr;
  using std::endl;
  using std::abs;
  
// *  Constant   

  PetscReal MachineEps()
  {
    return 2.22 * pow(10.0, -16.0);
  }

  
// *  Factrial   
// ** factorial
  
  int Fact(int n)
  {
    if (n < 0)
      {
        cerr << "Error:fact.cpp:fact. ";
        cerr << "argument n must be 0 or positive.";
        cerr << "inputted n is : " << n << endl;
	throw(std::exception());
      }

    if (n > 20)
      {
	cerr << "Error:fact.cpp::fact.";
	cerr << "argument n is too large.";
	cerr << "inputted n is : " << n << endl;
	throw(std::exception());
      }

    if (n == 0 || n ==1)
      {
	return 1;
      }
    else
      {
	int acc = 1;
	for(int m = 1; m <= n; m++)
	  acc *= m;
	return acc;
      }
  }


// ** double factrial

  int DoubleFact(int n)
  {
    if(n < 0)
      {
	cerr << "Error:fact.cpp:d_fact. ";
	cerr << "argument n must be 0 or positive.";
	cerr << "inputted n is : " << n << endl;
	throw(std::exception());
      }
    else if(n > 20)
      {
	cerr << "Error:fact.cpp:d_fact.";
	cerr << "argument n is too large.";
	cerr << "inputted n is : " << n << endl;
	throw(std::exception());	
      }
    
    if (n == 1 || n == 2)
      return n;
    else
      {
	int acc = 1;
	for(int m = n; 0 < m; m -= 2)
	  {
	    acc *= m;
	  }
	return acc;
      }
  }


// *  c jacob    
// ** gradient
  
  void GradRealize(int num, PetscComplex* dfdz, 
		    PetscReal* dfdx_dfdy)
  {

    for(int i = 0; i < num; i++)
      {
	dfdx_dfdy[i    ] = +2 * dfdz[i].real();
	dfdx_dfdy[i+num] = -2 * dfdz[i].imag();
      }    
  }

// ** hessian

  void HessRealize(int num,
		    PetscComplex* d2f_dzi_dzj,
		    PetscComplex* d2f_dzi_dczj,
		    PetscComplex* d2f_dadb)

  {
    int n = num;
    int nn= 2 * n;
    for(int i = 0; i < n; i++)
      for(int j = 0; j < n; j++)
	{
	  PetscComplex f  = d2f_dzi_dzj[i+n*j];
	  PetscComplex fc = d2f_dzi_dczj[i+n*j];
	  PetscReal r_f = f.real();
	  PetscReal i_f = f.imag();
	  PetscReal r_c = fc.real();
	  PetscReal i_c = fc.imag();
	  
	  d2f_dadb[i   + nn* j   ] = +2*r_f +2*r_c;
	  d2f_dadb[i   + nn*(j+n)] = -2*i_f + 2*i_c;
	  d2f_dadb[i+n + nn* j   ] = -2*i_f - 2*i_c;      
	  d2f_dadb[i+n + nn*(j+n)] = -2*r_f + 2*r_c;
    }
  }


// *  Log Trans  

  void GradLogTrans(Vec xs_vec, Vec df_vec, PetscBool* is_log, 
		      Vec logdf_vec)
  {
    int nx, nd, nl;
    
    VecGetSize(xs_vec, &nx);
    VecGetSize(df_vec, &nd);
    VecGetSize(logdf_vec, &nl);
    
    if(nx != nd && nx == nl)
      {
	cout << "Error:stong_utils:grad_log_trans.";
	cout << "the size of xs and df must be equal.";
	cout << "size(xs) = " << nx << ", ";
	cout << "size(df) = " << nd << ".";
	cout << "size(logdf) = " << nl << ". " << endl;
	throw (std::exception());
      }

    PetscScalar *xs, *df;
    VecGetArray(xs_vec, &xs);
    VecGetArray(df_vec, &df);
    

    for(int i = 0; i < nx; i++)
      {
	PetscScalar x_i  = xs[i];
	PetscScalar df_i = df[i];
	if(is_log[i])
	  {
	    PetscScalar val = df_i / x_i;
	    VecSetValues(logdf_vec, 1, &i, &val, INSERT_VALUES);
	  }
	else
	  {
	    VecSetValues(logdf_vec, 1, &i, &df_i, INSERT_VALUES);
	  }
      }

    VecAssemblyBegin(logdf_vec);
    VecAssemblyEnd(logdf_vec);

    VecRestoreArray(xs_vec, &xs);
    VecRestoreArray(df_vec, &df);
  }


  /*
  void hess_log_trans(int num, PetscReal* xs, PetscBool* is_)
  {
  }
  */

  
// *  Erfc       
// ** at n
  
  void Exp2ErfcAtN(PetscComplex x, int n, 
		      PetscComplex& y, PetscComplex& yn)
  {

    PetscComplex y0, yn0;
    PetscReal pi = M_PI;
    PetscComplex xx = x * x;
    PetscReal h = sqrt(pi / n);

    y0 = 0;
    for(int i = 0; i < n; i++) {
      PetscReal nnhh = (2 * i + 1) * h / 2;
      nnhh = nnhh * nnhh;
      PetscComplex t2 = exp(-nnhh) / (nnhh + xx);
      y0 += t2;
      yn0 = t2;
    }

    PetscComplex t3 = 2 * h / pi * x;
    y0  *= t3;
    yn0 *= t3;

    if(x.real() + abs(x.imag()) < pi / h)
      y0 += exp(xx) * 2.0 / (1.0 + exp(2*pi*x/h));

    y=y0; yn=yn0;
  }

  
// ** erfc

  PetscComplex Exp2Erfc(PetscComplex x)
  {

    PetscComplex y0, yn0, ratio;
    double eps = 0.00000000001;

    int n0, n1;
    n0 = 10;
    n1 = n0 + 10;

    bool convQ = false;

    for(int n = n0; n < n1; n++) {
      Exp2ErfcAtN(x, n, y0, yn0);
      ratio = yn0 / y0;

      if(convQ)
	break;

      if(abs(ratio) < eps && abs(yn0) < eps)
	  convQ = true;
    }

    return y0;
  }  

  
// *  End        

}
}
