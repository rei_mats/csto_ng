// *  Begin      

#include <iostream>
#include "ao.hpp"



namespace csto_ng
{

  //  int mathfunc::Fact(int);
  //  int mathfunc::DoubleFact(int);
  //  PetscComplex mathfunc::Exp2Erfc(PetscComplex);
  
  using mathfunc::Fact;
  using mathfunc::DoubleFact;
  using mathfunc::Exp2Erfc;
  
namespace ao
{
  using std::pow;
  using std::cerr;
  using std::cout;
  using std::endl;

  
  

// *  integrate  
// ** STO
  
  PetscComplex int_sto(PetscInt n, PetscComplex z)
  {
    return pow(z, -1 - n) * (1.0 * Fact(n));
  }

// ** GTO

  PetscComplex int_gto(PetscInt n, PetscComplex z)
  {

    PetscComplex res;
    PetscReal    pi = M_PI;

    if(n % 2 == 0) {
      int nn = n/2;

      res = DoubleFact(2*nn-1) * sqrt(pi) /
	(pow(2, nn+1) * 
	 pow(sqrt(z), 2*nn+1));
      
    } else {
      int nn = (n-1)/2;
      res = PetscReal(Fact(nn)) / (2.0 * pow(z, nn+1));
    }

    return res;
  }

// ** STO-GTO
// *** 0

  PetscComplex int_sto_gto_0(PetscComplex as, PetscComplex ag) {
    PetscComplex exp2erfc_val, sqrtPi,res;
    exp2erfc_val = Exp2Erfc(as / (2.0 * sqrt(ag)));
    sqrtPi=sqrt(M_PI);
    
    res = (exp2erfc_val * sqrtPi) / (2.0 * sqrt(ag));
    return (res);
  }


// *** 1

  PetscComplex int_sto_gto_1(PetscComplex as, PetscComplex ag) {

    PetscComplex sqrt_ag = sqrt(ag);
    PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);

    PetscComplex res;
    res = 1.0 / (2.0 * ag) - (as * ee) / (4.0 * ag * sqrt_ag);  
    return(res);
  }

  
// *** 2

    PetscComplex int_sto_gto_2(PetscComplex as, PetscComplex ag) {
      PetscComplex sqrt_ag = sqrt(ag);
      PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);

      PetscComplex res = 
	(-2.0 * sqrt_ag*as + (2.0 * ag + pow(as,2)) * 
	 ee) / (8.0 * pow(sqrt_ag, 5));

      return (res);
    }

// *** 3

  PetscComplex int_sto_gto_3(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res;
      
	res = (4.0 * ag + pow(as,2)) / (8.0 * pow(ag,3))
	  -(as * (6.0 * ag + pow(as,2)) * ee) / 
	  (16.0 * sqrt_ag * ag * ag * ag);
	return (res);
    }
    
// *** 4

    PetscComplex int_sto_gto_4(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res = 
	  (-2.0 * sqrt_ag * as * (10.0 * ag + as * as)
	   + (12.0 * ag * ag + 12.0 * ag * as * as + as*as*as*as) * 
	   ee) / (32.0*ag*ag*ag*ag*sqrt_ag);
	return (res);
    }

// *** 5
  
    PetscComplex int_sto_gto_5(PetscComplex as, PetscComplex ag) {
	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res
	  = (2.0*sqrt_ag*(2.0*ag + as*as)*(16.0*ag + as*as) -
	     as*(60.0*ag*ag + 20.0*ag*as*as + as*as*as*as)*
	     ee)/ (64.0*sqrt_ag*ag*ag*ag*ag*ag);
      return (res);
    }

// *** 6

    PetscComplex int_sto_gto_6(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res;      
	res = (-2.0*sqrt_ag*as*(6.0*ag + as*as)*(22.0*ag + as*as) +
	       (120.0*ag*ag*ag +180.0*ag*ag*as*as + 30.0*ag*pow(as,4) + 
		pow(as,6))*ee)/
	  (128.0*sqrt_ag*ag*ag*ag*ag*ag*ag);

      return (res);
    }

// *** 7
    
    PetscComplex int_sto_gto_7(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res;            
      
	res = (2.0 * sqrt_ag * 
	       (384.0 * pow(ag,3) + 348.0 * pow(ag,2) * pow(as,2) + 
		40.0  * ag * pow(as,4) + pow(as,6)) - 
	       as * (840.0 * pow(ag,3) + 420.0 * pow(ag,2) * pow(as,2) 
		     + 42.0 * ag * pow(as,4) + pow(as,6)) * ee)/
	  (256.0 * sqrt_ag * ag* ag* ag* ag* ag* ag* ag);

      return (res);
    }

// *** 8

    PetscComplex int_sto_gto_8(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res;            
      
	res = (-2.0*sqrt_ag*as*(2232.0*pow(ag,3) + 740.0*pow(ag,2)*pow(as,2) + 54.0*ag*pow(as,4) + pow(as,6)) + (1680.0*pow(ag,4) + 3360.0*pow(ag,3)*pow(as,2) + 840.0*pow(ag,2)*pow(as,4) + 56.0*ag*pow(as,6) + pow(as,8))*ee)/(512.0*sqrt_ag*pow(ag,8));

      return (res);
    }

// *** 9
    
    PetscComplex int_sto_gto_9(PetscComplex as, PetscComplex ag) {

	PetscComplex sqrt_ag = sqrt(ag);
	PetscComplex ee = Exp2Erfc(as / (2.0 * sqrt_ag)) * sqrt(M_PI);
	PetscComplex res;            
      
	res = (2.0*sqrt_ag*(6144.0*pow(ag,4) + 7800.0*pow(ag,3)*pow(as,2) + 1380.0*pow(ag,2)*pow(as,4) + 70.0*ag*pow(as,6) + pow(as,8)) - as*(15120.0*pow(ag,4) + 10080.0*pow(ag,3)*pow(as,2) + 1512.0*pow(ag,2)*pow(as,4) + 72.0*ag*pow(as,6) + pow(as,8))*ee)/(1024.0*sqrt_ag*pow(ag,9));

      return (res);
    }  


// *** swith

  PetscComplex int_sto_gto(int n, PetscComplex as, PetscComplex ag)
  {
    PetscComplex res;
    switch(n) {
    case 0:
      res = int_sto_gto_0(as, ag);
      break;
    case 1:
      res = int_sto_gto_1(as, ag);
      break;
    case 2:
      res = int_sto_gto_2(as, ag);
      break;
    case 3:
      res = int_sto_gto_3(as, ag);
      break;
    case 4:
      res = int_sto_gto_4(as, ag);
      break;
    case 5:
      res = int_sto_gto_5(as, ag);
      break;
    case 6:
      res = int_sto_gto_6(as, ag);
      break;
    case 7:
      res = int_sto_gto_7(as, ag);
      break;
    case 8:
      res = int_sto_gto_8(as, ag);
      break;
    case 9:
      res = int_sto_gto_9(as, ag);
      break;      
    default:
      cerr << "Error:ao.cpp:int_sto_gto. ";
      cerr << "n = " << n << " is not supported." << endl;
      throw(std::exception());
      break;
    }

    return res;    
  }

  
// *  STO        

  void STO::Normalize()
  {
    nterm_ = 1.0 / sqrt(int_sto(2 * n_, 2.0 * z_));    
  }

  PetscComplex STO::AtR(PetscReal r) const
  {
    return nterm_ * pow(r, n_) * exp(-z_ * r);
  }

  STO STO::Conj() const {
    STO sto(conj(nterm_), n_, conj(z_));
    return sto;
  }  
  

// *  GTO        


  void GTO::Normalize()
  {
    nterm_ = 1.0 / sqrt(int_gto(2 * n_, 2.0 * z_));
  }

    
  PetscComplex GTO::AtR(PetscReal r) const
  {
    return nterm_ * pow(r, n_) * exp(-z_ * r * r);
  }


  GTO GTO::Conj() const 
  {
    GTO gto(conj(nterm_), n_, conj(z_));
    return gto;
  }
    


// *  GTOs       

  void LCombGTO::set_z_i(int i, PetscComplex z)
    {
      c_gto_list_[i].second.z() = z;
      c_gto_list_[i].second.Normalize();
    }

  void LCombGTO::Add(PetscComplex c, 
		     const GTO& g) {
    c_gto_list_.push_back(make_pair(c, g));
  }

  PetscComplex LCombGTO::AtR(PetscReal r) const {
      
      PetscComplex acc(0.0, 0.0);
      vector<pair<PetscComplex, GTO> >::const_iterator it;
      vector<pair<PetscComplex, GTO> >::const_iterator endit;
      for(it = c_gto_list_.begin();
	  it != endit; ++it)
	{
	  PetscComplex c = it->first;
	  GTO          g = it->second;
	  acc += c * g.AtR(r);
	}
      return acc;
    }

  
  LCombGTO LCombGTO::Conj() const      
  {
    LCombGTO lc;

    vector<pair<PetscComplex, GTO> >::const_iterator it;
    vector<pair<PetscComplex, GTO> >::const_iterator endit;

    for(it = c_gto_list_.begin(); it != endit; ++it)
      {
	PetscComplex c = it->first;
	GTO         cg = it->second.Conj();
	lc.Add(conj(c), cg);
      }
    return lc;
  }
  
  
// *  Inner prod 
// ** complex inner product

  



// *  End        

}
}
