// *  Begin    

#ifndef STONG_MATHFUNC_HPP
#define STONG_MATHFUNC_HPP


#define PETSC_DESIRE_COMPLEX
#include <petscsys.h>
#include <petscvec.h>
#include <iostream>

namespace csto_ng
{
namespace mathfunc
{

// *  Constant 

  PetscReal MachineEps();
  

// *  Factrial 
  
  int Fact(int n);
  int DoubleFact(int n);


// *  c jacob  

/** From gradient of complex function f(z^*,z), 
 * compute derivative by real part and imaginary
 * part of variable z. Output array must allocated 
 * before call grad_realize
 */
  void GradRealize(int num, PetscComplex*, PetscReal*);
		    
/**
 * From hessian of complex function f(z^*, z),
 * compute second derivative by real and imaginary
 * part of z. Output array must allocated 
 * before call hess_realize
 */
void HessRealize(int num, PetscComplex* d2f_dzi_dzj, 
		  PetscComplex* d2f_dzi_dczj,
		  PetscReal* d2f_dadb);

// *  Log Trans
// ** log transform

  void GradLogTrans(Vec, Vec, PetscBool*, Vec);
  //  void hess_log_trans(Vec, Mat, PetscBool*, Mat);
  

// *  Erfc     

  PetscComplex Exp2Erfc(PetscComplex);

  

// *  End      

}
}  

#endif
