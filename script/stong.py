import os
import os.path
import shutil
from math import pi,exp,cos,sin
from decimal import Decimal
from key_val_reader import KeyValReader

def run_stong(base_file_name):
    return run_stong(base_file_name + ".in", 
                     base_file_name + ".out")

def run_stong(in_file, out_file):
    cmd = "stong " + in_file + " > " + out_file
    return os.system(cmd)    

def theta_to_dot_out(t):
    return str(t) + "deg" + ".out"

def theta_to_dot_in(t):
    return  str(t) + "deg" + ".in"

def run_stong_range(**args):

    t0 = Decimal(args['theta0'])
    t1 = Decimal(args['theta1'])
    dt = Decimal(args['d_theta'])
    t0_out  = args['guess_in']
    base_in = args['base_in']

    if 'out_dir' in args:
        out_dir = args['out_dir']
    else:
        out_dir = 'result'

    if 'key_val' in args:
        key_val = args['key_val']
    else:
        key_val = 0

    # make calculation directory if necessary
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    # read guess data for calculation and overwrite it
    guess_data = KeyValReader(t0_out)
    if key_val!=0:
        guess_data.overwrite_from_key_val_list(key_val)

    # change to calculation directory
    os.chdir(out_dir)

    # create range object which represent argument of cSTOs.
    num_t = int((t1-t0)/dt)
    ts = [t0 + dt * i for i in range(num_t+1)]

    
    # start loop
    for t in ts:
        print t

        # read base file
        new_data   = KeyValReader(base_in)
        new_data.overwrite(guess_data)

        # compute complex orbital exponent for STO and set it.
        rad = -float(t) * pi / 180.0
        z   = complex(cos(rad), sin(rad))
        new_data.new_value("zSto", str(z.real)+","+str(z.imag))

        # write input file and run calculation
        new_data.write_file(theta_to_dot_in(t))
        
        if(run_stong(theta_to_dot_in(t),
                     theta_to_dot_out(t))):
            break

        # read current output data for next step initial guess
        guess_data = KeyValReader(theta_to_dot_out(t))

        # if the result is not convergence, quite
        import sys
        if(guess_data.get("convergence") == "FAILED"):
            sys.stderr.write("FAILED: not convergence\n")
            break

    # come back to original directory
    os.chdir("..")
        
    
    
