import os

class KeyValReader:    
    def __init__(self, file):
        self.dict = {}
        self.keys = []
        f = open(file, 'r')
        for line in f.readlines():
            if(line != "\n"):
                strs = line.split(':')
                key = strs[0].strip()
                val = strs[1].strip()
                self.dict[key] = val
                self.keys.append(key)
        f.close()

    def write_file(self, file):
        f = open(file, 'w')
        for k in self.keys:
            f.write(k)
            f.write(": ")
            f.write(self.dict[k])
            f.write("\n")

    def remove(self, key):
        if key in self.keys:
            self.keys.remove(key)
            del self.dict[key]

    def new_value(self, key, val):
        if key in self.keys:
            self.dict[key] = val
        
    def get(self, key):
        return self.dict[key]

    def overwrite(self, other):
        for kv in other.dict.items():
            k = kv[0]
            v = kv[1]
            if k in self.keys:
                self.dict[k] = v


    def overwrite_from_key_val_list(self, kv_list):
        for kv in kv_list:
            k,v = kv
            if k in self.keys:
                self.dict[k] = v
            else:
                self.keys.append(k)
                self.dict[k] = v

    def display(self):
        for k in self.keys:
            print k + ": " +  self.dict[k]

def complex_to_string(z):
    return str(z.real) + "," + str(z.imag)


