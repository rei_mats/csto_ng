(* ::Package:: *)


calc[x_]:=Block[{
z,
v
},
z=N[x,5MachinePrecision];
v=Exp[z*z]*Erfc[z];
{Re@z,Im@z,Re@v,Im@v}
]

zs={3/10+1/5I,1/100+1/100I,101+201I,1/10+40I};
data = ToString/@Flatten@Table[
calc[z], 
{z, zs}
]
header=StringJoin[
"string* c_exp2_erfc_array[] = {\n\"",
Riffle[data,"\", \n \""], 
"\"};\n",
"unsigned int num_c_exp2_erfc=",ToString[Length[zs]],";\n\n"]

Export["./result/ref_c4_exp2_erfc.h",header, "Text"]

Exit[]
