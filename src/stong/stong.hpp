// *  Begin      
// ** ifndef

#ifndef STONG_HPP
#define STONG_HPP



// *  Field dep. 
// ** number of real variables

template<class F>
unsigned int num_real_vars(unsigned int n) {
  std::cout << "unsupported type in num_real_vars" 
	    << std::endl;
  abort();
  return n;
}

template<>
unsigned int num_real_vars<double>(unsigned int n) 
{ 
  return 2 * n;
}

template<>
unsigned int num_real_vars<dd_real>(unsigned int n) 
{ 
  return 2 * n;
}

template<>
unsigned int num_real_vars<dd_complex>(unsigned int n) 
{ 
  return 4 * n;
}


// ** solve linear equation

/** wrapper of solving linear equation */
void SolveLinearEq(unsigned int n, 
		   dd_real* A, dd_real* bs, dd_real* xs)
{
  mpackint *ipiv = new mpackint[n];
  mpackint info;

  for(unsigned int i = 0; i < n; i++)
    xs[i] = bs[i];
  
  Rgesv(n, 1, A, n, ipiv, xs, n, &info);

  delete[] ipiv;
  
}

void SolveLinearEq(unsigned int n, 
		   dd_complex* A, 
		   dd_complex* bs, 
		   dd_complex* xs)
{
  mpackint *ipiv = new mpackint[n];
  mpackint info;

  for(unsigned int i = 0; i < n; i++)
    xs[i] = bs[i];
  
  Cgesv(n, 1, A, n, ipiv, xs, n, &info);

  delete[] ipiv;
  
}

// ** abs2

dd_real abs2(dd_complex z)
{
  return (z * conj(z)).real();
}
dd_real abs2(dd_real z)
{
  return z * z;
}

// ** conjugate

dd_real conj(dd_real z) { return z; }
  

// ** is real

template<class F>
bool is_real() {exit(1); return false; }

template<> bool is_real<double>()     { return true;  }
template<> bool is_real<dd_real>()    { return true;  }
template<> bool is_real<dd_complex>() { return false; }
template<> bool is_real<std::complex<double> >() { return false;}



// ** Ps -> (cs, zs) 
 
void real_valued_to_zs_cs(unsigned int nGto,
			  dd_real* Ps, 
			  dd_real* cs, 
			  dd_real* zs)
{
  for(unsigned int i = 0; i < nGto; i++)
    {
      cs[i] = Ps[i];
      zs[i] = Ps[i + nGto];
    }
}

void real_valued_to_zs_cs(unsigned int nGto,
			  dd_real* Ps,
			  dd_complex* cs,
			  dd_complex* zs)
{
  for(unsigned int i = 0; i < nGto; i++)
    {
      cs[i].real() = Ps[i];
      zs[i].real() = Ps[i + nGto];
      cs[i].imag() = Ps[i + nGto * 2];
      zs[i].imag() = Ps[i + nGto * 3];
    }  
}


// *  Class      
// ** begin

/**
 * represent calculation system for STO-NG expansion.
 * FS means field for STO and FG means field for GTO.
 * RF is real field(double or dd_real).
 */
template<class FS, class FG, class RF>
class STONG
{
  
// ** field member

private:
  /*  ---- calculation input data  ----*/
  LCAO<STO<FS>, FS>* sto;     /** target STO */
  LCAO<GTO<FG>, FG>* chi;   /** STO-NG */

  /*  ----- calculation output data ---*/
  //  RF  sqerr;      /** square error of STO-NG */
  //  RF* grad_sqerr; /** gradient of square error */
  //  RF* hess_sqerr; /** hessian of square error */

  //  RF cnorm_dif;        /** Abs[1-(chi,chi)]^2 */
  //  RF* grad_cnorm_dif;  /** grad of cnorm_dif */
  //  RF* hess_cnorm_dif;  /** hess of cnorm_df  */

  /* ----- working memory ------ */
  LCAO<GTO<FG>, FG>** dmu_chi; /** grad of chi */
  LCAO<GTO<FG>, FG>** dmu_dnu_chi; /** hess of chi */
  
  FG* h_ui_uj;                /** {<ui, uj>} */
  FG* h_ui_phi;               /** {<ui, phi>}*/
  FG* cs;                     /** {c_i} */

  FG* dmu;
  FG* dmu_dnu;
  FG* dmu_dnu_star;


// ** constructors
// *** constructor

public:
  STONG(unsigned int _numGto, int _pnGto, 
	int _pnSto, FS _zSto)
  {
    sto = New_LC_MonoSTO<FS>(_pnSto, _zSto);

    chi = new LCAO<GTO<FG>, FG>(_numGto);

    FG one = value_one<FG>();
    for(unsigned int i = 0; i < _numGto; i++)
      {
	GTO<FG>* g = GTO<FG>::New_Normalized
	  (_pnGto, one);
	chi->Set_aos_i(i, g);
	chi->Set_cs_i(i, one);
      }

    /*  ------ output ----- */
    //    unsigned int n = num_real_vars<FG>(_numGto);
    //    grad_sqerr = new RF[n];
    //    hess_sqerr = new RF[n*n];
    //    grad_cnorm_dif = new RF[n];
    //    hess_cnorm_dif = new RF[n*n];

    /* ----- working spaces ----- */
    dmu_chi = MAlloc_grad_mu(*chi);
    dmu_dnu_chi = MAlloc_hess_mu_nu(*chi);

    h_ui_uj  = new FG[_numGto * _numGto];
    h_ui_phi = new FG[_numGto];
    cs       = new FG[_numGto];
    
    dmu = new FG[2 * _numGto];
    dmu_dnu = new FG[4*_numGto * _numGto];
    dmu_dnu_star = new FG[4*_numGto * _numGto];
  }
  
// *** delegate

public:
  ~STONG()
  {

    Delete_grad_mu(*chi, dmu_chi);
    Delete_hess_mu_nu(*chi, dmu_dnu_chi);

    delete sto;
    delete chi;

    /*
    delete[] grad_sqerr;
    delete[] hess_sqerr;
    delete[] grad_cnorm_dif;
    delete[] hess_cnorm_dif;
    */

    delete[] h_ui_uj;
    delete[] h_ui_phi;
    delete[] cs;

    delete[] dmu;
    delete[] dmu_dnu;
    delete[] dmu_dnu_star;
  }
  
  
// ** accessor
// *** setter
  
public:
  void Set_gto_zeta(FG* _zs)
  {
    for(unsigned int i = 0; 
	i < chi->Get_num(); i++)
	chi->Ref_aos_i(i)->Set_z(_zs[i]);
  }
  void Set_gto_cs(FG* _cs)
  {
    for(unsigned int i = 0; 
	i < chi->Get_num(); i++)
      chi->Set_cs_i(i, _cs[i]);
  }

  void Set_gto_zs_i(unsigned int i, FG _z)
  {
    chi->Ref_aos_i(i)->Set_z(_z);
  }
  void Set_gto_cs_i(unsigned int i, FG _c)
  {
    chi->Set_cs_i(i, _c);
  }

  void Set_sto_zeta(FS* _z)
  {
    sto->Set_z(_z);
  }

// *** getter

public:

  unsigned int Get_numGto ()
  {
    return chi->Get_num();
  }

  FG Get_gto_zeta_i(unsigned int i) const {
    return chi->Get_aos_i(i).Get_z();
  }

  FG Get_gto_cs_i(unsigned int i) const {
    return chi->Get_cs_i(i);
  }

  void Get_gto_zetas(FG* z) const {
    for(unsigned int i = 0; i < Get_numGto(); i++)
      z[i] = Get_gto_zeta_i(i);
  }


  /** --- for debugging --  */
  LCAO<STO<FS>, FS>* Get_Sto()  { return sto; }
  LCAO<GTO<FG>, FG>* Get_Stong() { return chi;}
  

// ** Update
// *** determine non restricted coefs.

public:
  void Calc_linear_coef()
  {

    // compute S matrix and m vector.
    unsigned int n = chi->Get_num();
    
    for(unsigned int i = 0; i < n; i++)
      {
	GTO<FG> *gi = chi->Ref_aos_i(i);
	h_ui_phi[i] = HIP(*gi, *sto);
	for(unsigned int j = 0; j < n; j++)
	  {
	    GTO<FG> *gj = chi->Ref_aos_i(j);
	    h_ui_uj[i + n * j] = HIP(*gi, *gj);
	  }
      }

    SolveLinearEq(n, h_ui_uj, h_ui_phi, cs);
    chi->Set_cs(cs);
    
  }
  
// *** grad chi

  void Calc_grad_chi()
  {
    Set_grad_mu(*chi, dmu_chi);
  }


// *** hess chi

  void Calc_hess_chi()
  {
    Set_hess_mu_nu(*chi, dmu_dnu_chi);
  }
  
// *** squared error

  /** objective function in minimization problem.
   * ||sto - sotng||^2
   */
  /*
  void Calc_sqerr()
  {
    sqerr = HDistance2(*sto, *chi);
  }
  */
  
// *** gradient of squared error

  /*
  void Calc_grad_sqerr()
  {
    // compute dmu_eps
    unsigned int n = chi->Get_num();
    for(unsigned int i = 0; i < 2 * n; i++ )
      {
	dmu[i]  = HIP(*chi, *dmu_chi[i]);
	dmu[i] -= HIP(*sto, *dmu_chi[i]);
      }

    // compute grad_eps
    grad_realize(2 * n, dmu, grad_sqerr);
  }
  */


// *** hessian of squared error

  /*
  void Calc_hess_sqerr()
  {
    // compute dmu_dnu_eps and dmu_dnu_start_eps
    unsigned int n = chi->Get_num();
    unsigned int N = 2 * n;
    for(unsigned int i = 0; i < N; i++)
      for(unsigned int j = 0; j < N; j++)
      {
	LCAO<GTO<FG>, FG>* gij = dmu_dnu_chi[i+N*j];
	LCAO<GTO<FG>, FG>* gi  = dmu_chi[i];
	LCAO<GTO<FG>, FG>* gj  = dmu_chi[j];
	
	dmu_dnu[i+N*j] =  HIP(*chi, *gij);
	dmu_dnu[i+N*j] -= HIP(*sto, *gij);
	dmu_dnu_star[i+N*j] = HIP(*gi, *gj);
      }

    // compute hess_eps
    hess_realize(N, dmu_dnu, dmu_dnu_star, 
		 hess_sqerr);
    
  }
  */
  
  
// *** cnorm difference

  /**
   * constraint condition for STO-NG expansion.
   * ( 1 - (chi, chi)) (1 - (chi^*, chi^*))
   */
  /*
  void Calc_cnorm_dif()
  {
    FG cnorm2 = CIP(*chi, *chi);
    FG one = value_one<FG>();
    cnorm_dif = abs2(one-cnorm2);
  }
  */
  
// *** grad of cnorm
  
/*
  void Calc_grad_cnorm_dif()
  {
    FG one = value_one<FG>();
    FG dif = one - conj(CIP(*chi, *chi));
    unsigned int n = chi->Get_num();
    
    for(unsigned int i = 0; i < 2 * n; i++)
      dmu[i] = -2 * CIP(*chi, *dmu_chi[i]) * dif;

    grad_realize(2 * n, dmu, grad_cnorm_dif);

  }
*/
  
// *** hess of cnorm

  /*
  void Calc_hess_cnorm_dif()
  {
    FG one = value_one<FG>();
    FG one_chichi= one - conj(CIP(*chi, *chi));

    unsigned int n = chi->Get_num();
    unsigned int N = 2 * n;
    for(unsigned int i = 0; i < N; i++)
      for(unsigned int j = 0; j < N; j++)
      {
	LCAO<GTO<FG>, FG>* gij = dmu_dnu_chi[i+N*j];
	LCAO<GTO<FG>, FG>* gi  = dmu_chi[i];
	LCAO<GTO<FG>, FG>* gj  = dmu_chi[j];

	dmu_dnu[i+N*j]  = -2 * one;
	dmu_dnu[i+N*j] *= CIP(*gi, *gj) + CIP(*chi, *gij);
	dmu_dnu[i+N*j] *= one_chichi;
	
	dmu_dnu_star[i+N*j] = 4 * CIP(*chi, *gi);
	dmu_dnu_star[i+N*j]*= conj(CIP(*chi, *gj));
	
      }

    hess_realize(N, dmu_dnu, dmu_dnu_star, hess_cnorm_dif);
    
  }
  */
  
// ** build
// *** squared error

  /** objective function in minimization problem.
   * ||sto - sotng||^2
   */
  void Build_sqerr(RF* sqerr)
  {
    *sqerr = HDistance2(*sto, *chi);
  }


// *** grad of squared error

  void Build_grad_sqerr(RF* grad)
  {
    unsigned int n = chi->Get_num();

    for(unsigned int i = 0; i < 2 * n; i++)
      {
	dmu[i]  =  HIP(*chi, *dmu_chi[i]);
	dmu[i] += -HIP(*sto, *dmu_chi[i]);
      }

    grad_realize(2 * n, dmu, grad);
  }


// *** hessian of squared error

  void Build_dmu_dnu_sqerr(FG* _dmu_dnu, FG* _dmu_dnu_star)
  {
    // compute dmu_dnu_eps and dmu_dnu_start_eps
    unsigned int n = chi->Get_num();
    unsigned int N = 2 * n;
    for(unsigned int i = 0; i < N; i++)
      for(unsigned int j = 0; j < N; j++)
      {
	LCAO<GTO<FG>, FG>* gij = dmu_dnu_chi[i+N*j];
	LCAO<GTO<FG>, FG>* gi  = dmu_chi[i];
	LCAO<GTO<FG>, FG>* gj  = dmu_chi[j];
	
	_dmu_dnu[i+N*j] =  HIP(*chi, *gij);
	_dmu_dnu[i+N*j] -= HIP(*sto, *gij);
	_dmu_dnu_star[i+N*j] = HIP(*gj, *gi);
      }
  }

  void Build_hess_sqerr(RF* hess)
  {
    Build_dmu_dnu_sqerr(dmu_dnu, dmu_dnu_star);
    hess_realize(2 * chi->Get_num(), 
		 dmu_dnu, 
		 dmu_dnu_star, 
		 hess);

  }

// *** cnorm difference
  
  void Build_cnorm_dif(RF* _dif)
  {
    FG cnorm2 = CIP(*chi, *chi);
    FG one = value_one<FG>();
    RF dif = abs2(one-cnorm2);
    *_dif = dif;
  }

// *** grad of cnorm

  void Build_grad_cnorm_dif(RF* grad)
  {
    FG one = value_one<FG>();
    FG dif = one - conj(CIP(*chi, *chi));
    unsigned int n = chi->Get_num();
    
    for(unsigned int i = 0; i < 2 * n; i++)
      dmu[i] = -2 * CIP(*chi, *dmu_chi[i]) * dif;

    grad_realize(2 * n, dmu, grad);    
  }
  
// *** hess of cnorm

  void Build_hess_cnorm_dif(RF* hess)
  {
    FG one = value_one<FG>();
    FG one_chichi= one - conj(CIP(*chi, *chi));

    unsigned int n = chi->Get_num();
    unsigned int N = 2 * n;
    for(unsigned int i = 0; i < N; i++)
      for(unsigned int j = 0; j < N; j++)
      {
	LCAO<GTO<FG>, FG>* gij = dmu_dnu_chi[i+N*j];
	LCAO<GTO<FG>, FG>* gi  = dmu_chi[i];
	LCAO<GTO<FG>, FG>* gj  = dmu_chi[j];

	dmu_dnu[i+N*j]  = -2 * one;
	dmu_dnu[i+N*j] *= CIP(*gi, *gj) + CIP(*chi, *gij);
	dmu_dnu[i+N*j] *= one_chichi;
	
	dmu_dnu_star[i+N*j] = 4 * CIP(*chi, *gi);
	dmu_dnu_star[i+N*j]*= conj(CIP(*chi, *gj));
	
      }

    hess_realize(N, dmu_dnu, dmu_dnu_star, hess);
  }
  
  
// ** end
};

// *  End        
// ** endif

#endif



