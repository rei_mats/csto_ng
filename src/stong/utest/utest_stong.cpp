#include <iostream>
#include <stdio.h>
#include <complex>

#include <gtest/gtest.h>
#include <mpack/mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_qd.hpp>

#include <src/math/complex_jacob.hpp>
#include <src/math/erfc.hpp>
#include <src/math/sto_gto_int_minor.hpp>
#include <src/math/sto_gto_int.hpp>

#include <src/ao/lcao.hpp>
#include <src/ao/inner_prod.hpp>
#include <src/ao/d_ao.hpp>
#include <src/stong/stong.hpp>

class Stong_dd_real : public ::testing::Test {

public:
  STONG<dd_real, dd_real, dd_real>* stong;
  dd_real zs[3];
  dd_real cs[3];

public:
  virtual void SetUp() {
  stong = new STONG<dd_real, dd_real, dd_real>
      (/*numGto=*/ 3,
       /*pnGto=*/  2,
       /*pnSto=*/  2,
       /* zSto=*/  0.5);
  zs[0] = 1.1; zs[1] = 1.2; zs[2] = 1.3;
  cs[0] = 1.0; cs[1] = 2.0; cs[2] = 3.0;

  stong->Set_gto_zeta(zs);
  stong->Set_gto_cs(cs);

  }
  ~Stong_dd_real() {
    delete stong;
  }
};
TEST_F(Stong_dd_real, grad){

  dd_real grad[3*2];
  stong->Calc_grad_chi();
  stong->Build_grad_sqerr(grad);
  //  stong->Calc_grad_sqerr();
  
  double expe[] = {11.5864, 11.6584, 11.6732, 
		   1.05397, 0.822004, -0.281963};

  for(unsigned int i = 0; i < 6; i++)
    EXPECT_NEAR(expe[i],
		grad[i].x[0],
		0.001);
}
TEST_F(Stong_dd_real, hess) {

  dd_real hess[36];
  stong->Calc_grad_chi();
  stong->Calc_hess_chi();
  stong->Build_hess_sqerr(hess);
  //  stong->Calc_hess_sqerr();
  
  dd_real expe[] = {
    "2.0", "1.99528", "1.98265", 
      "1.05397", "-0.180731", "-0.4766",  
      "1.99528", "2.", "1.996", 
      "0.0985808", "0.411002", "-0.230308",
      "1.98265", "1.996", "2.", 
      "0.187751", "0.166333", "-0.0939878",     
      "1.05397", "0.0985808", "0.187751", 
      "-6.20329", "1.87696", "2.53645",
      "-0.180731", "0.411002", "0.166333", 
      "1.87696", "-7.89789", "4.77121", 
      "-0.4766", "-0.230308", "-0.0939878", 
    "2.53645", "4.77121", "-6.73156"};

  for(unsigned int i = 0; i < 36; i++)
    EXPECT_NEAR(
		expe[i].x[0],
		hess[i].x[0],
	 0.00001);
  
}
TEST_F(Stong_dd_real, linear_coef) {

  stong->Calc_linear_coef();
  dd_real sqerr;
  stong->Build_sqerr(&sqerr);

  EXPECT_NEAR(0.892781,
	      sqerr.x[0],
	      0.001);
  
}
TEST_F(Stong_dd_real, IOptTarget) {
  
}
class Stong_dd_complex : public ::testing::Test {

public:
  STONG<dd_complex, dd_complex, dd_real>* stong;
  dd_complex zs[3];
  dd_complex cs[3];

public:
  virtual void SetUp() {
    stong = new STONG<dd_complex, dd_complex, dd_real>
      (/*numGto=*/ 3,
       /*pnGto=*/  2,
       /*pnSto=*/  2,
       /* zSto=*/  dd_complex(1, 2));
    zs[0] =  dd_complex(1,0);
    zs[1] = dd_complex("1.2", "0.2");
    zs[2] = dd_complex("1.3", 0);
    cs[0] = dd_complex(0, 1);
    cs[1] = dd_complex(2, 0);
    cs[2] = dd_complex(1, 0);
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);
  }
  ~Stong_dd_complex() {
    delete stong;
  }
};
TEST_F(Stong_dd_complex, grad){

  stong->Calc_grad_chi();
  dd_real grad[3*4];
  stong->Build_grad_sqerr(grad);
  double expe[] = {
    11.4004, 11.5965, 10.5577, 
    5.60916, -10.3742, -3.30187,
    3.26823, 4.97534, 4.45947,
    1.95181, 7.17376, 2.62035 
  };

  for(unsigned int i = 0; i < 12; i++)
    EXPECT_NEAR(expe[i],
		grad[i].x[0],
		0.001);
}
TEST_F(Stong_dd_complex, hess) {

  stong->Calc_grad_chi();
  stong->Calc_hess_chi();
  dd_real hess[(3*4)*(3*4)];
  stong->Build_hess_sqerr(hess);
  //  stong->Calc_hess_sqerr();

  double expe[] = 
    {2.0, 1.99253, 1.95756, -1.95181, -0.457801, -0.245513, 
     0.0, 0.0402822, 0.0, 5.60916, 0.2557, 0.0, 1.99253, 
     2.06968, 
      2.0143, 0.1992, -5.30365, -0.0657621, -0.0402822, 0.0, 
      0.0138201, -0.249111, 4.2861, 0.159676, 1.95756, 2.0143, 
      2., 0.0, 0.0825314, -3.30187, 0., -0.0138201, 0.0, 
      -0.319167, 0.359719, 2.62035, -1.95181, 0.1992, 0.0, 
      -11.7665,	
      -0.487085, 0.0, 5.60916, 0.249111, 0.319167, 
      8.96095, 1.97053,	      
      0.885093, -0.457801, -5.30365, 0.0825314, -0.487085, 
      7.76797,			  
      1.60852, -0.2557, 2.88766, -0.359719, -1.97053, -23.4979, -0.225607, 
      -0.245513, -0.0657621, -3.30187, 0.0, 1.60852, -0.766398, 0.0,
      -0.159676, 2.62035, -0.885093, 0.225607, -7.28779, 0.0, -0.0402822, 
      0.0, 5.60916, -0.2557, 0.0, 2., 1.99253, 1.95756, 1.95181, -0.457801, 
      -0.245513, 0.0402822, 0.0, -0.0138201, 0.249111, 2.88766, -0.159676, 
      1.99253, 2.06968, 2.0143, 0.1992, 5.07058, -0.0657621, 0.0, 0.0138201, 
      0.0, 0.319167, -0.359719, 2.62035, 1.95756, 2.0143, 2., 0.0, 0.0825314, 
3.30187, 5.60916, -0.249111, -0.319167, 8.96095, -1.97053, -0.885093, 
1.95181, 0.1992, 0.0, 14.2665, -0.487085, 0.0, 0.2557, 4.2861, 
0.359719, 1.97053, -23.4979, 0.225607, -0.457801, 5.07058, 0.0825314, 
-0.487085, -0.096001, 1.60852, 0.0, 0.159676, 2.62035, 0.885093, 
-0.225607, -7.28779, -0.245513, -0.0657621, 3.30187, 0.0, 1.60852, 
      2.24569};
  
  for(unsigned int i = 0; i < 12 * 12; i++) {
    if ( abs(expe[i]) < 0.0000001)
      EXPECT_NEAR(expe[i], hess[i].x[0], 0.0000001);
    else
      EXPECT_NEAR(
		  (expe[i] - hess[i].x[0]) / expe[i],
		  0.0,
		  0.00001);
  }
}
TEST_F(Stong_dd_complex, linear_coef) {

  stong->Calc_linear_coef();
  dd_real sqerr;
  stong->Build_sqerr(&sqerr);

  EXPECT_NEAR(26.211,
	      sqerr.x[0],
	      0.001);
  
}
TEST_F(Stong_dd_complex, val_constrain) {

  stong->Calc_linear_coef();
  dd_real val;
  stong->Build_cnorm_dif(&val);

  EXPECT_NEAR(
	      val.x[0],
	      53.6693,
	      0.0001);
}
TEST_F(Stong_dd_complex, grad_constrain) {

  stong->Calc_linear_coef();
  stong->Calc_grad_chi();
  dd_real grad[12];
  stong->Build_grad_cnorm_dif(grad);

  double expe[] = {
    80.9225, 66.5476, 75.4329, -6090.5, 6161.99, -669.18, 15.387, 2.97122, -5.27535, -4802.38, -2169.78, 6645.01};

  for(unsigned int i = 0; i < 12; i++)
    {
      EXPECT_NEAR(0.0,
		  (grad[i].x[0]-expe[i])/expe[i],
		  0.00001);
    }
}
TEST_F(Stong_dd_complex, hess_constrain) {

  //  stong->Calc_linear_coef();
  stong->Calc_grad_chi();
  stong->Calc_hess_chi();

  dd_real hess[12*12];
  stong->Build_hess_cnorm_dif(hess);

  double expe[] = {
    36.5793, 24.3078, 30.0439, 
    -5350.22, 4500.07, 227.625, 
    -12.2206, -5.41184, 2.82911,
    2799.18, 2991.19, -5449.0, 
    24.3078, 14.7064, 19.7119, 
    -4107.66, 3675.56, -66.6433, 
    -20.011, -12.2206, -6.76531, 
    3173.94, 1525.28, -4373.64, 
    30.0439, 19.7119, 26.6363, 
    -4427.63,  4788.62, -854.026, 
    -26.7517, -17.4826, -12.2206, 
    3835.41, 1203.15, -4622.89, 
    -5350.22, -4107.66, -4427.63, 
    502415.0, -321808.0, -130395.0, 
    -2695.7, -2443.62, -3513.07, 
    124586.0, -556548.0, 429396.0,	
    4500.07, 3675.56, 4788.62, 
    -321808.0, 618499.0, -308372.0, 
    -2047.05, -1506.29, -1240.82, 
    241064.0, 59906., -256116., 
    227.625, -66.6433, -854.026, 
    -130395.0, -308372.0, 405164.0, 
    4762.2, 3902.93, 4650.98,	
    -384572.0, 479770.0, -133791.0,
    -12.2206, -20.011, -26.7517,
    -2695.7, -2047.05, 4762.2, 
    89.8471, 76.8844, 82.1813, 
    -5209.87, 4168.9, 668.51,
    -5.41184, -12.2206, -17.4826, 
    -2443.62, -1506.29,	3902.93, 
    76.8844, 67.9742, 73.5295, 
    -3710.16, 3844.91, -395.232, 
    2.82911, -6.76531, -12.2206, 
    -3513.07, -1240.82, 4650.98, 
    82.1813, 73.5295, 79.9042, 
    -3660.6, 4085.42, -739.679, 
    2799.18, 3173.94, 3835.41, 
    124586.0, 241064.0, -384572.0, 
    -5209.87, -3710.16, -3660.6,
    618469.0, -183313.0, -388268.0, 
    2991.19, 1525.28, 1203.15, 
    -556548.0, 59906.0, 479770.0, 
    4168.9, 3844.91, 4085.42, 
    -183313.0, 176705.0, -37108.1, 
    -5449.0, -4373.64, -4622.89, 
    429396.0, -256116.0, -133791.0, 
    668.51, -395.232, -739.679, 
    -388268., -37108.1, 425925.0};

  for(unsigned int i = 0; i < 12*12; i++)
    {
      std::cout << i << std::endl;
      EXPECT_NEAR(0.0, 
		  (hess[i].x[0]-expe[i])/expe[i], 
		  0.00001);
    }
}

