#include <iostream>
#include <fstream>

#include <gtest/gtest.h>
#include <qd/dd_real.h>
#include <mpack/dd_complex.h>
//#include <mpack/mblas_dd.h>
#include <mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_qd.hpp>
#include <src/utils/array.hpp>
#include <src/math/complex_jacob.hpp>
#include <src/math/erfc.hpp>
#include <src/math/sto_gto_int_minor.hpp>
#include <src/math/sto_gto_int.hpp>
#include <src/ao/lcao.hpp>
#include <src/ao/d_ao.hpp>
#include <src/ao/inner_prod.hpp>
#include <src/opt/i_opt_target.hpp>
#include <src/opt/i_opt_for_test.hpp>
#include <src/opt/linear_search.hpp>
#include <src/opt/hess.hpp>
#include <src/opt/sqp.hpp>
#include <src/opt/newton.hpp>

#include <src/stong/stong.hpp>
#include <src/stong/opt_full_matrix.hpp>

typedef STONG<dd_real, dd_real, dd_real> SG;
typedef IOptTargetConstraint<dd_real> OptObj;
typedef ILinearSearch<dd_real> LSearch;
typedef ICalcHess<dd_real> CalcHess;
typedef SequentialQudraticProgramming<dd_real> SQP;


TEST(Cartesian, 1s_1g) {

  std::ofstream ofs("log/rsto1g.log");

  unsigned int num = 1;

  typedef StongFullMatrixCNorm1<dd_real, dd_real, dd_real> OptStong;
  
  SG* stong = new SG(num, 1, 1, dd_real("1"));
  dd_real zs[] = {"0.1"};
  stong->Set_gto_zeta(zs);
  stong->Calc_linear_coef();
  
  OptObj* optObj = new OptStong(stong);

  LSearch* l_search = new LinearSearch_Constant<dd_real>(1, ofs);

  CalcHess *calc_hess = new CalcTrueHess<dd_real>(optObj, ofs);
    
  SQP* solver = new SQP(20, 0.000001, optObj, l_search, calc_hess,ofs);
  
  solver->Compute();
  EXPECT_TRUE(solver->IsConvergence());

  delete l_search;
  delete calc_hess;
  delete solver;
  delete optObj;
  delete stong;
}
TEST(Cartesian, 1s_2g) {

  std::ofstream ofs("log/rsto2g.log");

  unsigned int num = 2;
  SG* stong  = new SG(num, 1, 1, 1);
  dd_real zs[] = {"0.15", "0.86"};
  stong->Set_gto_zeta(zs);
  stong->Calc_linear_coef();

  typedef StongFullMatrixCNorm1<dd_real, dd_real, dd_real> OptStong;
  OptObj* optObj = new OptStong(stong);
  
  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Constant<dd_real>(1, ofs);

  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(optObj, ofs);
  
  SQP* solver = new SQP(20, 0.000001, optObj, l_search, calc_hess, ofs);
    
  solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());

  delete l_search;
  delete calc_hess;
  delete solver;
  delete optObj;
  delete stong;
}
TEST(LogScale, ConstStep_TrueHess_1s2g) {
  
  std::ofstream ofs("log/log_rsto2g.log");
  unsigned int num = 2;
  SG* stong  = new SG(num, 1, 1, 1);
  dd_real zs[] = {"0.15", "0.86"};
  stong->Set_gto_zeta(zs);
  stong->Calc_linear_coef();

  OptObj* optObj = new StongFullMatrixCNorm1LogScale
    <dd_real, dd_real, dd_real>(stong);

  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Constant<dd_real>(1, ofs);

  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(optObj, ofs);

  SequentialQudraticProgramming<dd_real>* solver =
    new SequentialQudraticProgramming<dd_real>
    (20, 0.000001, optObj, l_search, calc_hess, ofs);
  
  solver->Compute();
  EXPECT_TRUE(solver->IsConvergence());

  delete l_search;
  delete calc_hess;
  delete solver;
  delete stong;
  delete optObj;

}
TEST(LogScale, LinearSeach_BFGS) {

  std::ofstream ofs("log/log_rsto2g.log");
  unsigned int num = 2;
  SG* stong  = new SG(num, 1, 1, 1);
  dd_real zs[] = {"0.15", "0.86"};
  stong->Set_gto_zeta(zs);
  stong->Calc_linear_coef();

  OptObj* optObj = new StongFullMatrixCNorm1LogScale
    <dd_real, dd_real, dd_real>(stong);

  ILinearSearch<dd_real>* l_search =
    new LinearSearchForMerit<dd_real>
    (/* max iter = */ 20, 
     /* zeta     = */ "0.5",
     /* rho      = */ 1,
     /* alpha0   = */ 1,
     /* opts     = */ TwoPiece, Armijo, SquaredPenalty,
     /* target   = */ optObj,
     /* ofs      = */ ofs);
     
  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(optObj, ofs);

  SequentialQudraticProgramming<dd_real>* solver =
    new SequentialQudraticProgramming<dd_real>
    (20, 0.000001, optObj, l_search, calc_hess, ofs);
  
  solver->Compute();
  EXPECT_TRUE(solver->IsConvergence());

  delete l_search;
  delete calc_hess;
  delete solver;
  delete optObj;
  delete stong;  

}
