#include <iostream>
#include <fstream>

#include <gtest/gtest.h>
#include <qd/dd_real.h>
#include <mpack/dd_complex.h>
//#include <mpack/mblas_dd.h>
#include <mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_string.hpp>
#include <src/utils/support_qd.hpp>
#include <src/utils/array.hpp>
#include <src/math/factorial.hpp>
#include <src/math/log_scale.hpp>
#include <src/math/complex_jacob.hpp>
#include <src/math/erfc.hpp>
#include <src/math/sto_gto_int_minor.hpp>
#include <src/math/sto_gto_int.hpp>
#include <src/ao/lcao.hpp>
#include <src/ao/d_ao.hpp>
#include <src/ao/inner_prod.hpp>
#include <src/opt/i_opt_target.hpp>
#include <src/opt/i_opt_for_test.hpp>
#include <src/opt/linear_search.hpp>
#include <src/opt/hess.hpp>
#include <src/opt/sqp.hpp>
#include <src/opt/newton.hpp>

#include <src/stong/stong.hpp>
#include <src/stong/opt_full_matrix.hpp>

using std::cout;
using std::endl;
using std::size_t;

class ComplexLog : public ::testing::Test {
public:
  size_t num;
  STONG<dd_complex, dd_complex, dd_real>* stong;
  StongFullMatrixCNorm1LogScale<dd_complex,dd_complex,dd_real>* log_scale;
  StongFullMatrixCNorm1<dd_complex, dd_complex, dd_real>    * cart_scale;
  double eps_d; 
  double eps_q; 
  dd_complex zs[2];
  dd_complex cs[2];
  virtual void SetUp()
  {
    eps_d = 10 * value_machine_eps<double>();
    eps_q = 10 * value_machine_eps<dd_real>();
    num   = 2;
    stong = new STONG<dd_complex, dd_complex, dd_real>(num,1,1,
						       dd_complex(1,0));
    log_scale = new StongFullMatrixCNorm1LogScale
      <dd_complex,dd_complex,dd_real>(stong);
    cart_scale = new StongFullMatrixCNorm1
      <dd_complex, dd_complex, dd_real>(stong, 0);

    zs[0] = dd_complex("0.1", "-0.3");
    zs[1] = dd_complex("0.6", "-0.5");
    cs[0] = dd_complex("1.1", "-0.1");
    cs[1] = dd_complex("0.3", "0.2");

    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);
    
  }
  ~ComplexLog()
  {
    delete stong; delete log_scale; delete cart_scale;
  }

};
TEST_F(ComplexLog, GetXs_i) {

  for(size_t i = 0; i < num; i++)
    {
      dd_complex c_calc(log_scale->GetXs_i(i),
			log_scale->GetXs_i(i + 2 * num));
      dd_complex z_calc( exp(log_scale->GetXs_i(i + 1 * num)),
			-exp(log_scale->GetXs_i(i + 3 * num)));
      
      EXPECT_DOUBLE_EQ(c_calc.real().x[0],
		       cs[i].real().x[0]);
      EXPECT_DOUBLE_EQ(c_calc.imag().x[0],
		       cs[i].imag().x[0]);
      EXPECT_DOUBLE_EQ(z_calc.real().x[0],
		       zs[i].real().x[0]);
      EXPECT_DOUBLE_EQ(z_calc.imag().x[0],
		       zs[i].imag().x[0]);
      
    }
  
}
TEST_F(ComplexLog, Update) {
  dd_real d[] = {1,    2,    3,     1,
		 "0.1","1.1","-0.2", "-0.2"};
  Test_OptTarget::SetUpdate(log_scale, d, eps_d);  
}
TEST_F(ComplexLog, Grad) {

  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::Grad_finite_dif(log_scale, h, eps);
  Test_OptTarget::GradConstraint_finite_dif(log_scale, h, eps);
}
class TestFullMatrix : public ::testing::Test {
public:
  dd_real zs[3]; 
  dd_real cs[3];
  unsigned int num;
  STONG<dd_real, dd_real, dd_real>* stong;
  IOptTargetConstraint<dd_real>* opt;

  virtual void SetUp() {
    num = 3;
    cs[0]="1.1"; cs[1]="0.1"; cs[2]="3.2";
    zs[0]="0.1"; zs[1]="0.4"; zs[2]="0.9";
    stong = new STONG<dd_real, dd_real, dd_real>(num, 2, 2, "1.5");
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);
    opt = new StongFullMatrixCNorm1<dd_real, dd_real, dd_real>(stong, dd_real(0));

    opt->CalcHess();
  }
  ~TestFullMatrix() {
    delete opt;
    delete stong;
  }
};
TEST_F(TestFullMatrix, GetXs_i) {
  
  double eps = 10 * value_machine_eps<double>();
  for(unsigned int i = 0; i < num; i++)
    {
      EXPECT_NEAR( opt->GetXs_i(i).x[0],     cs[i].x[0], eps);
      EXPECT_NEAR( opt->GetXs_i(i+num).x[0], zs[i].x[0], eps);
    }
}
TEST_F(TestFullMatrix, Update) {
  dd_real d[] = {1, 2, 3, 1, 4, 5};
  double eps = 10 * value_machine_eps<double>();
  Test_OptTarget::SetUpdate(opt, d, eps);
}
TEST_F(TestFullMatrix, Grad) {
  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::Grad_finite_dif(opt, h, eps);
}
TEST_F(TestFullMatrix, Hess) {
  dd_real h   = "0.00001";
  dd_real eps = "0.0005";
  Test_OptTarget::Hess_finite_dif(opt, h, eps);
  Test_OptTarget::HessSymmetric(opt);
}
TEST_F(TestFullMatrix, GradConstraint) {
  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::GradConstraint_finite_dif(opt, h, eps);
}
TEST_F(TestFullMatrix, HessConstraint) {
  
  dd_real h   = "0.00001";
  dd_real eps = "0.001";
  Test_OptTarget::HessConstraint_finite_dif(opt, h, eps);
  //  Test_OptTarget::HessConstraintSymmetric(opt);
  
}
class TestComplexNumeric : public ::testing::Test {
public:
  static const unsigned int num = 2;
  dd_complex cs[num];
  dd_complex zs[num];
  STONG<dd_complex,dd_complex,dd_real>* stong;
  IOptTargetConstraint<dd_real>* opt;

  virtual void SetUp() 
  {
    zs[0] = dd_complex("0.1", "-0.1");
    zs[1] = dd_complex("0.9", "-0.5");
    cs[0] = dd_complex("0.5", "-0.5");
    cs[1] = dd_complex("0.25", "0.5");

    stong = new STONG<dd_complex, dd_complex, dd_real>
      (num, 1, 1, dd_complex(1, 0));
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);

    opt = new StongFullMatrixCNorm1
      <dd_complex, dd_complex, dd_real>(stong, dd_real(0));
  }

  
  ~TestComplexNumeric()
  {
    delete stong;  delete opt;
  }
  
};
TEST_F(TestComplexNumeric, InnerProd) {

  LCAO<GTO<dd_complex>, dd_complex>* chi = stong->Get_Stong();
  //  LCAO<STO<dd_complex>, dd_complex>* sto = stong->Get_Sto();
  double eps = 1000 * value_machine_eps<dd_real>();
  dd_complex cnorm2 = CIP(*chi, *chi);
  dd_complex numeri = dd_complex
    ("0.22546299006500258895309377365778943460407849160816",
     "-0.18208138421595637798796021318062769624123006136272");
  EXPECT_TRUE(abs(cnorm2 - numeri) < eps);

  dd_complex one_cnorm2 = dd_complex(1,0) - cnorm2;
  numeri = dd_complex
    ("0.77453700993499741104690622634221056539592150839184",
     "0.18208138421595637798796021318062769624123006136272");
  EXPECT_TRUE(abs(one_cnorm2 - numeri) < eps);

  dd_complex cnorm_dif = one_cnorm2 * conj(one_cnorm2);
  dd_real numeric = "0.63306121023704500664623579227296909725561276864169";  
  EXPECT_DOUBLE_EQ(numeric.x[0], cnorm_dif.real().x[0]);

  opt->CalcValue();
  dd_real calc = opt->GetValue_h();
  EXPECT_DOUBLE_EQ(numeric.x[0], calc.x[0]);
  
}
TEST_F(TestComplexNumeric, Constraint) {
  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcValue();
  dd_real calc = opt->GetValue_h();
  dd_real numeric = "0.63306121023704500664623579227296909725561276864169";
  EXPECT_DOUBLE_EQ(numeric.x[0], calc.x[0]);
  EXPECT_NEAR(numeric.x[1], calc.x[1], eps);
}
TEST_F(TestComplexNumeric, GradConstraint) {
  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcGrad();
  dd_real* calc = opt->GetGrad_h();
  dd_real numeric[] =
    {
      "-1.8945297042129867852537552290631769806090466114274",
      "-1.5979750624778794810564314713765497616182070653153", 
      "-1.8983318378800702972116757101057145366204160054803", 
      "0.32364300072446260921071512666966541465446933393508", 
      "-1.29396790061779824332977446257143627272456070522105", 
      "0.26774293725050904724683302962608384357111952086278", 
      "1.9323780866180131038430858713761326101719813300986", 
      "-0.18358458359558398363166508829097168298043466603264"};
  for(unsigned int i = 0; i < 4 * num; i++)
    {
      EXPECT_DOUBLE_EQ(numeric[i].x[0], calc[i].x[0]) <<
	"index : " << i << endl;
      EXPECT_NEAR(     numeric[i].x[1], calc[i].x[1],   eps) <<
      	"index : " << i << endl;
    }  
}
TEST_F(TestComplexNumeric, HessConstraint) {
  //  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcHess();
  dd_real* calc = opt->GetHess_h();
  dd_real numeric[] ={
      "1.05910851212145324537947039255579993362489107945326",
      "0.55893254977751815390744541471848393485380055739686", 
      "0.89968990345316682571654237296083364592315914343615", 
      "-0.15659435051700905288371545913153192668849330398799", 
      "-0.72832553686382551195184085272251078496492024545087", 
      "-2.6737235549042868102791334323092775229085228483997",  
      "-1.00083562661465601141056136162267829651401555365508", 
      "0.098235275082948272234732920479944920448146992239873", 
      "0.55893254977751815390744541471848393485380055739686", 
      "-1.02472248602005590522395863531485580318115659061773", 
      "-1.8059489308669348867127702072932084188140930790556", 
      "0.105357347126425325923726280993804110132412062426249", 
      "1.39377472631916664462830464990002594842918684767941", 
      "-0.72832553686382551195184085272251078496492024545087", 
      "-3.5288515591840009665159492791904205473404061223990", 
     "0.53422375049097880262334312016828982394360432103593", 
    "0.89968990345316682571654237296083364592315914343615", 
     "-1.8059489308669348867127702072932084188140930790556", 
     "8.0433159349562194840434902078265517136235385503859", 
     "-0.86451739113036909060075071236057839688763669923500", 
     "8.6622554756108228135200845245863725900988102248129", 
     "0.54599461655659698158752440105394620643024503301999", 
     "-13.3158453621844305959918082353393159666495811004500", 
     "-1.08096272036667494149764301382729665158395413941638", 
    "-0.15659435051700905288371545913153192668849330398799", 
    "0.105357347126425325923726280993804110132412062426249", 
     "-0.86451739113036909060075071236057839688763669923500", 
     "-0.098298854359386882508610273146954002713474552159697", 
     "-1.11269044372304145791949335040121911571795499217532", 
     "0.20769851807436717304028114390908481018200208190410", 
     "-0.64357028812443529173552298522597966854367739142319", 
     "0.42615864300084207013288621624789387875589261242034", 
    "-0.72832553686382551195184085272251078496492024545087", 
    "1.39377472631916664462830464990002594842918684767941", 
    "8.6622554756108228135200845245863725900988102248129", 
     "-1.11269044372304145791949335040121911571795499217532", 
     "7.2554045916014325337547202032934844567922631465880", 
     "3.6759825148065155756926934718200707242924382949092", 
     "0.83159740597728121245372205041999749882002849419942", 
     "-0.43671118477476630404181553588891939003656263979286", 
    "-2.6737235549042868102791334323092775229085228483997", 
     "-0.72832553686382551195184085272251078496492024545087", 
     "0.54599461655659698158752440105394620643024503301999", 
     "0.20769851807436717304028114390908481018200208190410", 
     "3.6759825148065155756926934718200707242924382949092", 
     "5.1715735934599233831512911754228287199862154765170", 
     "7.4149918869188195211237857172795591923289127860285", 
     "-0.99994212153858359643474620420876993885212980317433", 
    "-1.00083562661465601141056136162267829651401555365508", 
     "-3.5288515591840009665159492791904205473404061223990", 
     "-13.3158453621844305959918082353393159666495811004500", 
     "-0.64357028812443529173552298522597966854367739142319", 
     "0.83159740597728121245372205041999749882002849419942", 
     "7.4149918869188195211237857172795591923289127860285", 
     "3.5475835194631312346526905501083483369437952714336", 
     "-0.66635612171746968356666938774403104375333191874116", 
    "0.098235275082948272234732920479944920448146992239873", 
     "0.53422375049097880262334312016828982394360432103593", 
     "-1.08096272036667494149764301382729665158395413941638", 
     "0.42615864300084207013288621624789387875589261242034", 
     "-0.43671118477476630404181553588891939003656263979286", 
     "-0.99994212153858359643474620420876993885212980317433", 
      "-0.66635612171746968356666938774403104375333191874116", 
      "0.31699507048050670738967028744761249423361292615629" };
    
  for(unsigned int i = 0; i < 4 * num; i++)
    for(unsigned int j = 0; j < 4 * num; j++)
      {
	unsigned int idx = i + (4*num) * j;
	EXPECT_DOUBLE_EQ(numeric[idx].x[0], calc[idx].x[0]) 
	  << "index : " << i << ", " << j << endl;
	//	EXPECT_NEAR( numeric[idx].x[1], calc[idx].x[1],   eps) 
	//	  << "index : " << i << ", " << j << endl;
    }

}
TEST_F(TestComplexNumeric, Value) {

  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcValue();
  dd_real v = opt->GetValue();
  dd_real numeric = "0.65327104784349052053829504130709137253497472500742";

  EXPECT_DOUBLE_EQ(numeric.x[0], v.x[0]);
  EXPECT_NEAR(numeric.x[1], v.x[1], eps);
  
}
TEST_F(TestComplexNumeric, Grad) {

  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcGrad();  
  dd_real* calc = opt->GetGrad();
  dd_real numeric[] = 
    {"-0.51674055264004374312954715792863921269047873460154", 
     "-0.99928330640785757146063240331072063643061656769817",
     "-8.3361893874908396009054579527437625978706961920167",
     "0.19173977813215175473243409588479270385084909172824",
     "-1.30529393264294134667190249791497173442831696524662",  
     "0.62312588225412368145059618789409427952218089304853",  
     "-2.1854100914124598988617266624422044148478531874421", 
     "-0.47580150162456422426885801238300363495722659938678"};
  for(unsigned int i = 0; i < 4 * num; i++)
    {
      EXPECT_DOUBLE_EQ(numeric[i].x[0], calc[i].x[0]) <<
	"index : " << i << endl;
      EXPECT_NEAR(     numeric[i].x[1], calc[i].x[1],   eps) <<
      	"index : " << i << endl;
    }
}
TEST_F(TestComplexNumeric, Hess) {
  //  double eps = 1000 * value_machine_eps<dd_real>();
  opt->CalcHess();
  dd_real* calc = opt->GetHess();
  
  dd_real numeric[] = 
    {"3.3635856610148581721245019049328595801601370494271", 
     "0.84848793610220169046900792548056687198150067366607",
     "-10.5215994789032994997671846151859670127185493794588", 
     "0.23956167270703987597085990403616970694142724862708", 
     "0.0",
     "-0.83851115436016342182646036464524885386954465701498",
     "-6.4626669327273384434231508531966652425776709307771", 
     "0.38215733196595686687560300756924094389711884124817",	
     "0.84848793610220169046900792548056687198150067366607", 
     "2.4470657044939371555548529471953033801351248841404", 
     "3.6116721231696191256438916312514845666300642389340", 
     "0.36158350966319481338497212299968893987006541401280", 
     "0.83851115436016342182646036464524885386954465701498", 
     "0", 
     "2.0511044561183841121816699141050649263022831508699",
     "-0.049810133877694252311402814163736710537371584118641",
     "-10.5215994789032994997671846151859670127185493794588", 
     "3.6116721231696191256438916312514845666300642389340", 
     "194.11175330979062276258931487291648441183241745865",
     "-0.124529497228840142286912930802051758000418911259526", 
     "18.764225524884097847510613433799781608623356939926",
     "-2.0511044561183841121816699141050649263022831508699", 
     "73.870711610037605999227118614542415268160412729074", 
     "1.6768906465903705917516758211309502049323264257683",
     "0.23956167270703987597085990403616970694142724862708", 
     "0.36158350966319481338497212299968893987006541401280",
     "-0.124529497228840142286912930802051758000418911259526",  
     "-0.043342825987328525023609177313446495111278211896986",
     "-0.38215733196595686687560300756924094389711884124817",
     "-0.097904978698722891374980898817732453071473881369827",
     "-1.6768906465903705917516758211309502049323264257683",
     "0.79919188301647324162506121442485033769102783189869", 
     "0",     
     "0.83851115436016342182646036464524885386954465701498", 
     "18.764225524884097847510613433799781608623356939926",	
     "-0.38215733196595686687560300756924094389711884124817", 
     "3.3635856610148581721245019049328595801601370494271", 
     "0.84848793610220169046900792548056687198150067366607", 
     "10.5215994789032994997671846151859670127185493794588", 
     "0.23956167270703987597085990403616970694142724862708",
     "-0.83851115436016342182646036464524885386954465701498", 
     "0", 
     "-2.0511044561183841121816699141050649263022831508699",
     "-0.097904978698722891374980898817732453071473881369827", 
     "0.84848793610220169046900792548056687198150067366607", 
     "2.4470657044939371555548529471953033801351248841404", 
     "3.6116721231696191256438916312514845666300642389340",    
     "-1.4677649405468535118472680700415910181544182507901",
     "-6.4626669327273384434231508531966652425776709307771", 
     "2.0511044561183841121816699141050649263022831508699", 
     "73.870711610037605999227118614542415268160412729074",
     "-1.6768906465903705917516758211309502049323264257683", 
     "10.5215994789032994997671846151859670127185493794588", 
     "3.6116721231696191256438916312514845666300642389340", 
     "26.623555694309444783081122638302425536176576410003", 
     "-0.124529497228840142286912930802051758000418911259526",
     "0.38215733196595686687560300756924094389711884124817",
     "-0.049810133877694252311402814163736710537371584118641", 
     "1.6768906465903705917516758211309502049323264257683", 
     "0.79919188301647324162506121442485033769102783189869", 
     "0.23956167270703987597085990403616970694142724862708",  
     "-1.4677649405468535118472680700415910181544182507901",
     "-0.124529497228840142286912930802051758000418911259526", 
     "1.00189980262866320636020017007016719700623371961160"
    };
  for(unsigned int i = 0; i < 4 * num; i++)
    for(unsigned int j = 0; j < 4 * num; j++)
      {
	unsigned int idx = i + (4*num) * j;
	EXPECT_DOUBLE_EQ(numeric[idx].x[0], calc[idx].x[0]) 
	  << "index : " << i << ", " << j << endl;
	//	EXPECT_NEAR( numeric[idx].x[1], calc[idx].x[1],   eps) 
	//	  << "index : " << i << ", " << j << endl;
    }
}
class TestFullMatrixComplex : public ::testing::Test {

public:

  dd_complex zs[3];
  dd_complex cs[3];
  unsigned int num;
  STONG<dd_complex, dd_complex, dd_real>* stong;
  StongFullMatrixCNorm1<dd_complex,dd_complex, dd_real>* opt;

  virtual void SetUp(){

    num = 3;
    zs[0] = dd_complex("0.1","-0.1");
    zs[1] = dd_complex("1.0", "0.2");
    zs[2] = dd_complex("1.3", 0);
    cs[0] = dd_complex(0, 1);
    cs[1] = dd_complex(2, 0);
    cs[2] = dd_complex(1, 0);
    
    stong = new STONG<dd_complex, dd_complex, dd_real>
      (num, 2, 2, dd_complex(1, 2));
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);
    
    opt = new StongFullMatrixCNorm1
      <dd_complex, dd_complex,dd_real>(stong, dd_real(0));
  }
  ~TestFullMatrixComplex() {
    delete stong;
    delete opt;
  }

};
TEST_F(TestFullMatrixComplex, GetXs_i) {
  
  double eps = 10 * value_machine_eps<double>();

  for(unsigned int i = 0; i < num; i++)
    {

      EXPECT_TRUE( abs(opt->GetXs_i(i)-cs[i].real()) < eps) <<
	"cs.real : " << cs[i].real() << endl <<
	"GetXs_i : " << opt->GetXs_i(i) << endl;
      EXPECT_TRUE( abs(opt->GetXs_i(i+num)-zs[i].real()) < eps) <<
	"cs.real : " << zs[i].real() << endl <<
	"GetXs_i : " << opt->GetXs_i(i+num) << endl;
      EXPECT_TRUE( abs(opt->GetXs_i(i+2*num)-cs[i].imag()) < eps) <<
	"cs.imag : " << cs[i].imag() << endl <<
	"GetXs_i : " << opt->GetXs_i(i+2*num) << endl;      
      EXPECT_TRUE( abs(opt->GetXs_i(i+3*num)-zs[i].imag()) < eps) <<
	"cs.imag : " << zs[i].imag() << endl <<
	"GetXs_i : " << opt->GetXs_i(i+3*num) << endl;      

    }
}
TEST_F(TestFullMatrixComplex, Update) {

  stong->Set_gto_cs(cs);
  dd_real d[12] = {
    "0.1", "0.2", "0.3", "0.4", "0.5", "0.6",
    "0.7", "0.8", "0.9", "1.0", "1.1", "1.2"};

  double eps = 0.000000001;

  opt->SetUpdate(d);
  for(unsigned int i = 0; i < 3; i++)
    {
      EXPECT_TRUE(abs(cs[i].real()+d[i]   - opt->GetXs_i(i)) 
		  < eps);
      EXPECT_TRUE(abs(zs[i].real()+d[i+3] - opt->GetXs_i(i+3)) 
		  < eps);
      EXPECT_TRUE(abs(cs[i].imag()+d[i+6] - opt->GetXs_i(i+6)) 
		  < eps);
      EXPECT_TRUE(abs(zs[i].imag()+d[i+9] - opt->GetXs_i(i+9)) 
		  < eps);                  
    }
	      
}
TEST_F(TestFullMatrixComplex, Grad) {

  stong->Set_gto_cs(cs);
  dd_real h   = "0.00001";
  dd_real eps = "0.001";
  Test_OptTarget::Grad_finite_dif(opt, h, eps);
  Test_OptTarget::GradConstraint_finite_dif(opt, h, eps);
  
}
TEST_F(TestFullMatrixComplex, Hess) {

  dd_real h   = "0.00001";
  dd_real eps = "0.001";
  Test_OptTarget::Hess_finite_dif(opt, h, eps);
}
TEST_F(TestFullMatrixComplex, HessConstraint) {
  
  dd_real h   = "0.00001";
  dd_real eps = "0.001";
  stong->Calc_linear_coef();
  opt->CalcHess();  
  Test_OptTarget::HessConstraint_finite_dif(opt, h, eps);
  
}
class TestFullMatrixAccuracy : public ::testing::Test {
public:

  STONG<dd_real, dd_real, dd_real>* stong;
  StongFullMatrixCNorm1<dd_real, dd_real, dd_real>* opt;
  double eps; 
  virtual void SetUp() {
    eps = 10000 * value_machine_eps<dd_real>();
    stong = new STONG<dd_real, dd_real, dd_real>(2,1,1,1);
    opt = new StongFullMatrixCNorm1
      <dd_real, dd_real, dd_real> (stong, dd_real(0));

    dd_real zs[] = {"0.2", "0.8"};
    dd_real cs[] = {"1.1", "1.2"};
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);
    opt->CalcHess();
  }
  ~TestFullMatrixAccuracy() {
    delete stong;}
};
TEST_F(TestFullMatrixAccuracy, grad) {

  dd_real grad_expe[] ={
    "1.9827063318604979192386921002557095916552988034843", 
    "2.2668819473728482605438520696929496958764577584427", 
    "3.4470578163484327396605091700470354279373463190696", 
    "-0.43897805780830141560216417668012339580315036225208"};
  
  double eps = 1000 * value_machine_eps<dd_real>();
  for(unsigned int i = 0 ; i < 4; i++)
    {
      EXPECT_TRUE(abs(grad_expe[i] - opt->GetGrad()[i])<eps)
	<< "expe: " << grad_expe[i] << std::endl
	<< "calc: " << opt->GetGrad()[i] << std::endl
	<< "dif : " << grad_expe[i]-opt->GetGrad()[i]<<std::endl
	<< "eps:  " << eps << std::endl;
    }  
}
TEST_F(TestFullMatrixAccuracy, grad_const) {
  dd_real grad_expe[] = {
    "27.726887682699485282895877373546948011381734487479", 
    "28.129570216808258633224847860848575335936769646009", 
    "30.084007838384280841284039121430552565474240347234", 
    "-7.5210019595960702103210097803576381413685600868084"};
  
  for(unsigned int i = 0 ; i < 4; i++)
      {
	dd_real calc = opt->GetGrad_h()[i];
	dd_real expe = grad_expe[i];
      EXPECT_TRUE(abs(calc - expe)<eps)
	<< "i : " << i << std::endl
	<< "expe: " << expe << std::endl
	<< "calc: " << calc << std::endl
	<< "dif : " << calc - expe <<std::endl
	<< "eps:  " << eps << std::endl;
      }
}
TEST_F(TestFullMatrixAccuracy, hess) {
  dd_real hess_expe[] = {
"2.0000000000000000000000000000000000000000000000000", 
"1.4310835055998654057018711479880167906819957501514", 
"3.133688923953120672418644700042759479943042108245", 
"-0.96598136627990914884876302489191133371034713135218", 
"1.4310835055998654057018711479880167906819957501514", 
"2.0000000000000000000000000000000000000000000000000", 
"3.5419316763596668791121310912703415569379394816247", 
"-0.36581504817358451300180348056676949650262530187673", 
"3.1336889239531206724186447000427594799430421082451", 
"3.5419316763596668791121310912703415569379394816247", 
"-5.9532767465705250146553291293630132618477591293614", 
"0.44274145954495835988901638640879269461724243520308", 
"-0.96598136627990914884876302489191133371034713135218", 
"-0.36581504817358451300180348056676949650262530187673", 
"0.44274145954495835988901638640879269461724243520308", 
"0.88628730441285102451474010335130059624218645305794"
  };

  for(unsigned int i = 0 ; i < 4; i++)
    for(unsigned int j = 0 ; j < 4; j++)
      {
	dd_real calc = opt->GetHess()[i + 4 * j];
	dd_real expe = hess_expe[i + 4 * j];
	EXPECT_TRUE(abs(calc - expe)<eps)
	  << "i, j : " << i << ", " << j << std::endl
	  << "expe: " << expe << std::endl
	  << "calc: " << calc << std::endl
	  << "dif : " << calc - expe <<std::endl
	  << "eps:  " << eps << std::endl;	
      }
}
TEST_F(TestFullMatrixAccuracy, hess_h) {
  dd_real hess_expe[] = 
    {"44.846602728701868026317638984130185964402812682398", 
    "41.265500727838129139256008957033433390479740927104", 
    "60.648641286073841886515724090480633025608902596827", 
    "-15.162160321518460471628931022620158256402225649207", 
    "41.265500727838129139256008957033433390479740927104", 
    "45.744522728701868026317638984130185964402812682398", 
    "58.853164987818581924755907686909294505857731703519", 
    "-14.713291246954645481188976921727323626464432925880", 
    "60.648641286073841886515724090480633025608902596827", 
    "58.853164987818581924755907686909294505857731703519", 
    "-126.82463605791485455695521190774882639631880188085", 
    "-5.8988507835016374123662459248509841077630999638298", 
    "-15.162160321518460471628931022620158256402225649207", 
    "-14.713291246954645481188976921727323626464432925880", 
    "-5.8988507835016374123662459248509841077630999638298", 
    "10.875965145370497115992823706659793703651475099468"};

  for(unsigned int i = 0 ; i < 4; i++)
    for(unsigned int j = 0 ; j < 4; j++)
      {
	dd_real calc = opt->GetHess_h()[i + 4 * j];
	dd_real expe = hess_expe[i + 4 * j];
	EXPECT_TRUE(abs(calc - expe)<eps)
	  << "i, j : " << i << ", " << j << std::endl
	  << "expe: " << expe << std::endl
	  << "calc: " << calc << std::endl
	  << "dif : " << calc - expe <<std::endl
	  << "eps:  " << eps << std::endl;	
      }  
}
class FullMatrixLogScale : public ::testing::Test {
public:
  unsigned int num;

  STONG<dd_real, dd_real, dd_real>* stong;
  
  StongFullMatrixCNorm1LogScale<dd_real,dd_real,dd_real>* target;
  StongFullMatrixCNorm1<dd_real, dd_real, dd_real> * carte;
  
  virtual void SetUp() {
    
    num = 2;
    stong = new STONG<dd_real, dd_real, dd_real>(num,1,1,1);
    dd_real zs[] = {"0.2", "0.8"};
    dd_real cs[] = {"0.44", "1.2"};
    stong->Set_gto_zeta(zs);
    stong->Set_gto_cs(cs);

    
    target = new StongFullMatrixCNorm1LogScale
      <dd_real, dd_real, dd_real>(stong);
    carte = new StongFullMatrixCNorm1
      <dd_real, dd_real, dd_real>(stong, dd_real(0));

    
  }

  ~FullMatrixLogScale() {
    delete stong;
    delete target;
    delete carte;
  }
};
TEST_F(FullMatrixLogScale, update) {
  double eps = 10 * value_machine_eps<double>();
  dd_real d[] = {1, 2, 1, 5};
  Test_OptTarget::SetUpdate(target, d, eps);
}
TEST_F(FullMatrixLogScale, GetXi) {

  dd_real d[] = {1, 2, 0, 1};
  dd_real c0[4];
  dd_real c1[4];

  for(unsigned int i = 0; i < 4; i++)
    c0[i] = target->GetXs_i(i);

  target->SetUpdate(d);
  
  for(unsigned int i = 0; i < 4; i++)
    c1[i] = target->GetXs_i(i);

  for(unsigned int i = 0; i < 4; i++)
    EXPECT_NEAR((c0[i] + d[i]).x[0], 
		c1[i].x[0], 0.0000000001 );
  
}
TEST_F(FullMatrixLogScale, value) {
  target->CalcValue();
  carte->CalcValue();

  double eps = 0.0000000001;
  EXPECT_TRUE(
	      abs(target->GetValue() - carte->GetValue())<eps);
}
TEST_F(FullMatrixLogScale, grad) {
  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::Grad_finite_dif(target, h, eps);
  Test_OptTarget::GradConstraint_finite_dif(target, h, eps);
}
TEST_F(FullMatrixLogScale, hess) {
  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::Hess_finite_dif(target, h, eps);
  Test_OptTarget::HessConstraint_finite_dif(target, h, eps);
  //  Test_OptTarget::HessSymmetric(target);  
}




