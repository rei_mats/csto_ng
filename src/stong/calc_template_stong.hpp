// *  Begin     

#ifndef CALC_TEMPLATE_STONG_HPP
#define CALC_TEMPLATE_STONG_HPP

#include <boost/lexical_cast.hpp>
#include <iostream>
#include <fstream>

using boost::lexical_cast;
using std::string;
using std::cout;
using std::endl;
using std::cerr;
using std::ofstream;

// *  class     
// ** begin
// *** begin

template<class FS, class FG, class RF>
class CalcStong
{


// ** field

private:
  
  KeyValReader* reader_;
  STONG<FS,FG,RF>* stong_;
  IOptTargetConstraint<RF>* opt_target_;
  ILinearSearch<RF>* linear_search_;
  ICalcHess<RF>* calc_hess_;
  SequentialQudraticProgramming<RF>* solver_;
  ofstream ofs_;

  
// ** constructors
// *** constructor

public:
  
  CalcStong(KeyValReader* _reader) :
    reader_(_reader) {}
  
// *** destructor

public:

  ~CalcStong()
  {
    /*    
	  delete stong_;
    delete opt_target_;
    delete linear_search_;
    delete calc_hess_;
    delete solver_;
    */
  }
  

// ** Build
// *** log file

private:
  void build_LogFile()
  {
    string log_file = reader_->CheckGet<string>("log");
    ofs_.open(log_file.c_str());
  }
  
// *** stong_zs

  void build_stong_zs(unsigned int numGto)
  {
    FG* zs = new FG[numGto];

    try
      {
	for(unsigned int i = 0; i < numGto; i++)
	  {
	    string key = "z" + lexical_cast<string>(i);
	    zs[i] = reader_->CheckGet<FG>(key);
	  }
	stong_->Set_gto_zeta(zs);
      }
    catch(string& s)
      {
	delete[] zs;
	throw s;
      }

    delete[] zs;
  }
  
// *** stong_cs

  void build_stong_cs(unsigned int numGto)
  {
    FG* cs = new FG[numGto];

    try
      {
	string value = "value";
	string linear = "linear";
	
	string coef_method = reader_->CheckGet<string>("coef");
	vector<string> vec;
	vec.push_back(value); vec.push_back(linear);
	reader_->CheckOneOf("coef", vec);
	
	if(coef_method == value)
	  {
	    for(unsigned int i = 0; i < numGto; i++)
	      {
		string key = "c" + lexical_cast<string>(i);
		cs[i] = reader_->CheckGet<FG>(key);
	      }
	    stong_->Set_gto_cs(cs);
	  }
	else if (coef_method == linear)
	  {
	    stong_->Calc_linear_coef();
	  }
      }
    catch(string& s)
      {
	delete[] cs;
	throw s;
      }

    delete[] cs;
  }
  
// *** stong

  void build_stong() 
  {
    string key, val;

    unsigned int numGto = reader_->CheckGet<int>("numGto");
    int pnGto = reader_->CheckGet<int>("nGto");
    int pnSto = reader_->CheckGet<int>("nSto");
    FS  zSto  = reader_->CheckGet<FS>("zSto");

    stong_ = new STONG<FS,FG,RF>(numGto, pnGto, pnSto, zSto);

    try
      {
	build_stong_zs(numGto);
	build_stong_cs(numGto);
      }
    catch(string& s)
      {
	delete stong_;
	string msg = "Error occured when constructing STONG ";
	msg += "object. \n";
	msg = msg + s;
	throw msg;
      }
  }
// *** opt target

  void build_optTarget()
  {
    RF lambda;
    if(reader_->Exist("lambda"))
      lambda = reader_->CheckGet<RF>("lambda");
    else
      lambda = RF(0);
    
    vector<string> cand;
    cand.push_back("log");
    cand.push_back("cartesian");
    reader_->CheckOneOf("scale", cand);
    string scale = reader_->CheckGet<string>("scale");

    if(scale == "cartesian")
      {
	opt_target_ = new
	  StongFullMatrixCNorm1<FS,FG,RF>(stong_, lambda);
      }
    else if(scale == "log")
      {
	opt_target_ = new
	  StongFullMatrixCNorm1LogScale<FS,FG,RF>(stong_);
      }
  }

  
// *** linear search

  void build_linearSearch()
  {
    string one = "one"; string constant = "constant";
    string merit = "merit";
    vector<string> cand; cand.push_back(one);
    cand.push_back(constant); cand.push_back(merit);
    reader_->CheckOneOf("linear_search", cand);
    string l_search = reader_->CheckGet<string>("linear_search");

    if(l_search == one)
      {
	linear_search_ = 
	  new LinearSearch_Constant<RF>(1,ofs_);
      }
    else if(l_search == constant)
      {
	RF alpha = reader_->CheckGet<RF>("step_size");
	linear_search_ =
	  new LinearSearch_Constant<RF>(alpha, ofs_);
      }
    else if(l_search == merit)
      {
	RF rho   = reader_->CheckGet<RF>("rho");
	RF alpha = reader_->CheckGet<RF>("step_size");
	RF zeta  = reader_->CheckGet<RF>("zeta");
	unsigned int max_iter = 
	  reader_->CheckGet<int>("ls_max_iter");

	vector<string> cand2;
	cand2.push_back("square"); cand2.push_back("exact");
	reader_->CheckOneOf("penalty_type", cand2);
	LinearSearchPenalty penalty;
	string t = reader_->CheckGet<string>("penalty_type");
	if(t == "square")
	  {
	    penalty = SquaredPenalty;
	  }
	else if(t == "exact")
	  {
	    penalty = ExactPenalty;
	  }

	linear_search_ = new LinearSearchForMerit
	  <RF>(max_iter, zeta, rho, alpha,
	       TwoPiece, Armijo, penalty, opt_target_, ofs_);
      }
  }


  
// *** hess

  void build_calcHess()
  {
    vector<string> v;
    v.push_back("true"); v.push_back("BFGS"); 
    v.push_back("ModBFGS");
    reader_->CheckOneOf("hess_calc", v);
    string type = reader_->CheckGet<string>("hess_calc");

    if(type == "true")
      {
	calc_hess_ = new CalcTrueHess<RF>(opt_target_, ofs_);
      }
    else if(type == "BFGS")
      {
	calc_hess_ = new CalcHessBFGS<RF>(opt_target_, ofs_);
      }
    else if(type == "ModBFGS")
      {
	RF w  = reader_->CheckGet<RF>("w_Powell");
	RF b0 = reader_->CheckGet<RF>("b0");
	calc_hess_ = new CalcHessModBFGS
	  <RF>(opt_target_, w, b0,  ofs_);
      }
  }

// *** SQP

  void build_SQP()
  {
    unsigned int max_iter = 
      reader_->CheckGet<int>("max_iter");

    double eps = reader_->CheckGet<double>("eps");

    solver_ = new SequentialQudraticProgramming<RF>
      (max_iter, eps, opt_target_, linear_search_,
       calc_hess_, ofs_);

  }
  
  
// ** Print
  
public:

  void Print()
  {
    string res = solver_->IsConvergence() ? "OK" : "FAILED";
    RF sqerr;     stong_->Build_sqerr(&sqerr);
    RF cnorm_dif; stong_->Build_cnorm_dif(&cnorm_dif);

    cout << "convergence: " << res   << endl;
    cout << "SquareError: " << sqerr << endl;
    cout << "CNormDif:    " << cnorm_dif << endl;
    
    cout << "nSto: " << reader_->CheckGet<string>("nSto");
    cout << endl;
    cout << "zSto: "  << reader_->CheckGet<string>("zSto");
    cout << endl;
    cout << "numGto: "  << reader_->CheckGet<int>("numGto");
    cout << endl;
    cout << "nGto: " << reader_->CheckGet<string>("nGto");
    cout << endl;
    
    for(unsigned int i = 0; i < stong_->Get_numGto(); i++)
      {
	FG z = stong_->Get_gto_zeta_i(i);
	cout << "z" << i << ": " << to_string(z) << endl;
      }
    
    for(unsigned int i = 0; i < stong_->Get_numGto(); i++)
      {
	FG c = stong_->Get_gto_cs_i(i);
	cout << "c" << i << ": " << to_string(c) << endl;
      }
    
    cout << "lambda: " <<opt_target_->GetLambda() << endl;
  }
  

// ** Run

public:
  
  void Run()
  {
    try
      {
	build_LogFile();
	build_stong();
	build_optTarget();
	build_linearSearch();
	build_calcHess();
	build_SQP();
      }
    catch(string& s)
      {
	cerr << "error occured when building objects" <<endl;
	cerr << s;
	exit(1);
      }

    try
      {
	solver_->Compute();
      }
    catch(string& s)
      {
	cerr << "error occured in calculation." << endl;
	cerr << s;
	exit(1);
      }

    try
      {
	Print();
      }
    catch(string& s)
      {
	cerr << "error occured in printing. " << endl;
	cerr << s;
	exit(1);
      }
  }

// ** end
};


// *  End       

#endif
