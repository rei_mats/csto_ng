// *  Begin           
// ** ifndef

#ifndef OPT_FULL_MATRIX_HPP
#define OPT_FULL_MATRIX_HPP


// *  Utils           
// ** index to real valued

template<class FS, class FG, class RF>
RF index_to_real_valued_variable(STONG<FS,FG,RF>* stong,
				 unsigned int i)
{

  unsigned int n = stong->Get_numGto();
  if (is_real<FG>())
      {
	if(i < n)
	  return take_real_part(stong->Get_gto_cs_i(i));
	else
	  return take_real_part(stong->Get_gto_zeta_i(i-n));
      } 
    else 
      {
	if(i < n)
	  return take_real_part(stong->Get_gto_cs_i(i));
	else if(i < 2 * n)
	  return take_real_part(stong->Get_gto_zeta_i(i-n));
	else if(i < 3 * n)
	  return take_imag_part(stong->Get_gto_cs_i
				(i-2*n));
	else
	  return take_imag_part(stong->Get_gto_zeta_i
				(i-3*n));
      }
}

// ** shift in log scale

template<class FG, class RF>
void shift_cszs_in_logscale(unsigned int n, FG* cs, FG* zs, RF* d)
{
  std::cerr << "it is not implemented" << std::endl;
  std::cerr << "shift_in_logscale" << std::endl;
  exit(1);
}

template<>
void shift_cszs_in_logscale(unsigned int n, dd_real* cs, dd_real* zs, 
			    dd_real* d)
{
  for(unsigned int i = 0; i < n; i++)
    {
      cs[i]+= d[i];
      zs[i] = shift_in_log_scale(zs[i], d[i + n]);
    }  
}


template<>
void shift_cszs_in_logscale(unsigned int n, dd_complex* cs, 
		       dd_complex* zs, dd_real* d)
{
  for(unsigned int i = 0; i < n; i++)
    {
      dd_complex dc = dd_complex(d[i], d[i + 2 * n]);
      cs[i]        += dc;

      dd_real rz =  shift_in_log_scale(zs[i].real(), d[i+1*n]);
      dd_real iz =  shift_in_neg_log( zs[i].imag(),  d[i+3*n]);      
      zs[i] = dd_complex(rz, iz);
    }
}


// *  cartesian       
// ** begin

template<class FS, class FG, class RF>
class StongFullMatrixCNorm1 : public IOptTargetConstraint<RF>
{


// ** filed
  
private:
  STONG<FS,FG,RF>* stong;

  // ----- work space -----
  FG* work1_;
  FG* work2_;

// ** constructors
// *** constructor
public:

  StongFullMatrixCNorm1(STONG<FS,FG,RF>* _stong, RF _lambda):
    IOptTargetConstraint<RF>
    (num_real_vars<FG>(_stong->Get_numGto()),
     _lambda)
  {
    stong = _stong;
    unsigned int n = _stong->Get_numGto();
    work1_ = new FG[n];
    work2_ = new FG[n];
  }

// *** destructor

  ~StongFullMatrixCNorm1() 
  {
    delete[] work1_;
    delete[] work2_;
  }


// ** opt target
// *** get x_i
  
  RF GetXs_i(unsigned int i) const
  {

    // {Re[c_0], Re[c_1], ...,
    //  Re[z_0], Re[z_1], ...,
    //  Im[c_0], Im[c_1], ...,
    //  Im[z_0], Im[z_1], ...}

    return index_to_real_valued_variable(stong, i);
  }

// *** function value

  void  CalcValue() 
  {
    stong->Build_sqerr(&this->val_);
    stong->Build_cnorm_dif(&this->val_h_);
    //    std::cout << this->val_h_ << std::endl;    
  }

// *** grad
    
  void CalcGrad()
  {
    stong->Calc_grad_chi();
    
    stong->Build_sqerr(&this->val_);
    stong->Build_grad_sqerr(this->grad_);

    stong->Build_cnorm_dif(&this->val_h_);
    stong->Build_grad_cnorm_dif(this->grad_h_);
  }

// *** hess

  void CalcHess()
  {
    stong->Calc_grad_chi();
    stong->Calc_hess_chi();
    
    stong->Build_sqerr(&this->val_);
    stong->Build_grad_sqerr(this->grad_);
    stong->Build_hess_sqerr(this->hess_);

    stong->Build_cnorm_dif(&this->val_h_);
    stong->Build_grad_cnorm_dif(this->grad_h_);
    stong->Build_hess_cnorm_dif(this->hess_h_);
  }

// *** update
  
  void SetUpdate(RF* d)
  {
    unsigned int n = stong->Get_numGto();
    FG* dcs = work1_;
    FG* dzs = work2_;

    real_valued_to_zs_cs(n, d, dcs, dzs);

    for(unsigned int i = 0; i < n; i++)
      {
	FG z = stong->Get_gto_zeta_i(i);
	stong->Set_gto_zs_i(i, z + dzs[i]);

	FG c = stong->Get_gto_cs_i(i);
	stong->Set_gto_cs_i(i, c + dcs[i]);
	
      }
  }


// *** display

public:

  virtual std::ostream& Display(std::ostream& os)
  {
    if(is_real<FG>())
      for(unsigned int i = 0; i < stong->Get_numGto(); i++)
	{
	  os << "z" << i << ": " << take_real_part(stong->Get_gto_zeta_i(i));
	  os << std::endl;
	  os << "c" << i << ": " << take_real_part(stong->Get_gto_cs_i(i));
	  os << std::endl;
	}
    else
      for(unsigned int i = 0; i < stong->Get_numGto(); i++)
	{
	  FG z = stong->Get_gto_zeta_i(i);
	  FG c = stong->Get_gto_cs_i(i);
	  os << "z" << i << ": " << take_real_part(z) << std::endl;
	  os << "zi"<< i << ": " << take_imag_part(z) << std::endl;
	  os << "c" << i << ": " << take_real_part(c) << std::endl;
	  os << "ci"<< i << ": " << take_imag_part(c) << std::endl;
	}
    return os;
  }

  
  
// ** end

};

// *  log             
// ** begin

template<class FS, class FG, class RF>
class StongFullMatrixCNorm1LogScale :
  public IOptTargetConstraint<RF>
{

// ** field

private:
  STONG<FS,FG,RF>* stong_;
  FG* work1_;
  FG* work2_;
  

// ** constructors
// *** constructor
  
public:
  StongFullMatrixCNorm1LogScale(STONG<FS,FG,RF>* _stong):
    IOptTargetConstraint<RF>(num_real_vars<FG>(_stong->Get_numGto()),
			     value_zero<RF>())
  {
    stong_ = _stong;
    unsigned int n = _stong->Get_numGto();
    work1_ = new FG[n];
    work2_ = new FG[n];
  }

// *** destructor

  ~StongFullMatrixCNorm1LogScale()
  {
    delete[] work1_;
    delete[] work2_;
  }
				

// ** Opt target
// *** get x_i
  
public:
  RF GetXs_i(unsigned int i) const
  {
    unsigned int n = stong_->Get_numGto();
    
    if(is_real<FG>())
      {
	if(i < n)
	  return take_real_part(stong_->Get_gto_cs_i(i));
	else
	  return log(take_real_part(stong_->Get_gto_zeta_i(i-n)));
      }
    else
      {
	if(i < n)
	  return take_real_part(stong_->Get_gto_cs_i(i));
	else if(i < 2 * n)
	  return log(take_real_part(stong_->Get_gto_zeta_i(i-n)));
	else if(i < 3 * n)
	  return take_imag_part(stong_->Get_gto_cs_i(i-2*n));
	else
	  return log(-take_imag_part(stong_->Get_gto_zeta_i(i-3*n)));
      }
    
    return 1;
  }

// *** function value

public:
  void CalcValue()
  {
    stong_->Build_sqerr(&this->val_);
    stong_->Build_cnorm_dif(&this->val_h_);
  }

// *** grad

public:
  void CalcGrad()
  {
    CalcValue();
    
    stong_->Calc_grad_chi();

    stong_->Build_grad_sqerr(this->grad_);
    stong_->Build_grad_cnorm_dif(this->grad_h_);
    unsigned int n = stong_->Get_numGto();

    if(is_real<FG>())
      {
	for(unsigned int i = 0; i < n; i++)
	  {
	    FG zi = stong_->Get_gto_zeta_i(i);
	    this->grad_[i + n] *= take_real_part(zi);
	    this->grad_h_[i+n] *= take_real_part(zi);
	  }
      }
    else
      {
	for(unsigned int i = 0; i < n; i++)
	  {
	    FG zi = stong_->Get_gto_zeta_i(i);
	    this->grad_[  i + n*1] *= take_real_part(zi);
	    this->grad_h_[i + n*1] *= take_real_part(zi);
	    this->grad_[  i + n*3] *= take_imag_part(zi);
	    this->grad_h_[i + n*3] *= take_imag_part(zi);
	  }
      }
  }

// *** hess

public:
  void CalcHess()
  {

    stong_->Calc_grad_chi();
    stong_->Calc_hess_chi();
    
    stong_->Build_sqerr(         &this->val_    );
    stong_->Build_cnorm_dif(     &this->val_h_  );
    stong_->Build_grad_sqerr(     this->grad_   );
    stong_->Build_grad_cnorm_dif( this->grad_h_ );
    stong_->Build_hess_sqerr(     this->hess_   );
    stong_->Build_hess_cnorm_dif( this->hess_h_ );
    
    unsigned int n = stong_->Get_numGto();
    if(is_real<FG>())
      for(unsigned int i = 0; i < n; i++)
	for(unsigned int j = 0; j < n; j++)
	{
	  FG zi = stong_->Get_gto_zeta_i(i);
	  FG zj = stong_->Get_gto_zeta_i(j);
	  
	  this->hess_[i+n + (2*n)*   j ] *= take_real_part(zi);
	  this->hess_[i   + (2*n)*(n+j)] *= take_real_part(zj);
	  FG fij = this->hess_[i+n + (2*n)*(n+j)];
	  FG fi  = this->grad_[i + n];
	  this->hess_[i+n+(2*n)*(n+j)] = take_real_part(zi * zj * fij);
	  if(i == j)
	    this->hess_[i+n+(2*n)*(n+j)] += take_real_part(zi * fi);
	  
	  this->hess_h_[i+n + (2*n)*   j ] *=take_real_part(zi);
	  this->hess_h_[i   + (2*n)*(n+j)] *=take_real_part(zj);
	  FG hij = this->hess_h_[i+n + (2*n)*(n+j)];
	  FG hi  = this->grad_h_[i+n];
	  this->hess_h_[i+n + (2*n)*(n+j)] = take_real_part(zi*zj*hij);
	  if(i == j)
	    this->hess_h_[i+n+(2*n)*(n+j)] += take_real_part(zi * hi);
	}
    else
      {
	std::string msg = "";
	msg += "opt_full_matrix.hpp::CalcHess for complex field";
	msg += " is not implemented.";
	throw msg;

	/*
	unsigned int N = 4 * n;
	for(unsigned int i = 0; i < n; i++)
	  for(unsigned int j = 0; j < n; j++)
	    {
	      FG zi = stong_->Get_gto_zeta_i(i);
	      FG zj = stong_->Get_gto_zeta_j(j);
	      FG fij= this->hess_[i+n + (2*n)*(n+j)];
	      FG fi = this->grad_[i + n];
	      FG fj = this->grad_[j + n];

	      // this->hess_[i+n*0 + N*(j+n*0)]
	      this->hess_[i+n*0 + N*(j+n*1)] *= take_real_part(zj);
	      // this->hess_[i+n*0 + N*(j+n*2)] 
	      this->hess_[i+n*0 + N*(j+n*3)] *= take_imag_part(zj);
	      this->hess_[i+n*1 + N*(j+n*0)] *= take_real_part(zi);
	      this->hess_[i+n*1 + N*(j+n*1)] *= 11;
	      this->hess_[i+n*1 + N*(j+n*2)] *= take_real_part(zi);
	      this->hess_[i+n*1 + N*(j+n*3)] *= 11;
	      //this->hess_[i+n*2 + N*(j+n*0)]
	      this->hess_[i+n*2 + N*(j+n*1)] *= take_real_part(zj);
	      // this->hess_[i+n*2 + N*(j+n*2)]
	      this->hess_[i+n*2 + N*(j+n*3)] *= take_imag_part(zj);
	      this->hess_[i+n*3 + N*(j+n*0)] *= take_imag_part(zi);
	      this->hess_[i+n*3 + N*(j+n*1)] *= 11;
	      this->hess_[i+n*3 + N*(j+n*2)] *= take_imag_part(zi);
	      this->hess_[i+n*3 + N*(j+n*3)] *= 11;
	      
	    }
	*/
      }
  }

// *** update

  void SetUpdate(RF* d)
  {
    unsigned int n = stong_->Get_numGto();
    FG* zs = work1_;
    FG* cs = work2_;

    for(unsigned int i = 0; i < n; i++)
      {
	zs[i] = stong_->Get_gto_zeta_i(i);
	cs[i] = stong_->Get_gto_cs_i(i);
      }
    
    shift_cszs_in_logscale(n, cs, zs, d);

    for(unsigned int i = 0; i < n; i++)
      {
	stong_->Set_gto_zs_i(i, zs[i]);
	stong_->Set_gto_cs_i(i, cs[i]);
      }
  }

  
// *** display

  public:

  virtual std::ostream& Display(std::ostream& os)
  {
    /*
    if(is_real<FG>())
      for(unsigned int i = 0; i < stong_->Get_numGto(); i++)
	{
	  os << "z" << i << ": " << stong_->Get_gto_zeta_i(i);
	  os << std::endl;
	  os << "c" << i << ": " << stong_->Get_gto_cs_i(i);
	  os << std::endl;
	}
    else
      for(unsigned int i = 0; i < stong_->Get_numGto(); i++)      
	{
	  FG z = stong_->Get_gto_zeta_i(i);
	  FG c = stong_->Get_gto_cs_i(i);
	  os << "z" << i << ": " << take_real_part(z) << std::endl;
	  os << "zi"<< i << ": " << take_imag_part(z) << std::endl;
	  os << "c" << i << ": " << take_real_part(c) << std::endl;
	  os << "ci"<< i << ": " << take_imag_part(c) << std::endl;
	}
    */
    return os;
      
  }
  
  
// ** end

};


// *  End             
#endif
