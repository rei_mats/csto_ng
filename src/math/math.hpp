#ifndef MATH_HPP
#define MATH_HPP

#include <src/math/erfc.hpp>
#include <src/math/newton.hpp>
#include <src/math/sto_gto_int.hpp>
#include <src/math/support_mpack.hpp>
#include <src/math/factorial.hpp>

#endif
