// *  Begin  
// ** ifndef

#ifndef LOG_SCALE_HPP
#define LOG_SCALE_HPP


// ** include

#include <math.h>
#include <string.h>
#include <string>

// *  Main   
// ** shift in log scale

template<class RF>
RF shift_in_log_scale(RF x, RF d)
{
  if(not (x > 0))
    {
      std::string msg("Error:log_scale.hpp:shift_in_log_scale.");
      msg += "argument of this function must be positive.";
      throw msg;
    }
  
  RF logx   = log(x);
  RF logx_d = logx + d;
  return exp(logx_d);
}


// ** shift in negative log

template<class RF>
RF shift_in_neg_log(RF x, RF d)
{
  if(not (x < 0))
    {
      std::string msg("Error:log_scale.hpp>:shift_in_neg_log.");
      msg += "argument of this function must be negative";
      throw msg;
    }
  
  RF log_neg_x = log(-x);
  RF shifted   = log_neg_x + d;
  return -exp(shifted);
}


// *  End    

#endif



