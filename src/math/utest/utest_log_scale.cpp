#include <src/math/log_scale.hpp>
#include <gtest/gtest.h>

TEST(ShiftInLogScale, double)
{
  double x0 = 2.5;
  double d  = 1.1;
  double x1 = shift_in_log_scale(x0, d);
  double x2 = shift_in_log_scale(x1,-d);

  EXPECT_DOUBLE_EQ(x0, x2);
  EXPECT_TRUE(x0 < x1);

}
TEST(ShiftInLogScale, neg_double)
{
  double x0 = -2.5;
  double d  = 1.1;
  double x1 = shift_in_neg_log(x0, d);
  double x2 = shift_in_neg_log(x1,-d);

  EXPECT_DOUBLE_EQ(x0, x2);
  EXPECT_TRUE(x0 > x1);
}
TEST(ShiftInLogScale, exception_check)
{
  EXPECT_ANY_THROW(shift_in_log_scale(-1.0, 1.0));
  EXPECT_ANY_THROW(shift_in_neg_log(+1.0, 0.2));
}
  
