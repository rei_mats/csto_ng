#include <iostream>
#include <src/math/complex_jacob.hpp>
#include <gtest/gtest.h>
#include <src/utils/support_mpack.hpp>

class ComplexJacobTest : public ::testing::Test
{
public:
  dd_complex f_z[2];
  dd_real    f_a[4];
  
  dd_complex z01;
  dd_complex z02;
  dd_real    h;
  unsigned int N;
  
protected:
  virtual void SetUp()
  {
    z01.real() = "1.125";
    z01.imag() = "0.4441123";
    z02.real() = "1.105";
    z02.imag() = "2.4141123";
    h = "0.000001";
    N=2;
  }
 
public:
  dd_real func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex res = (z1+c1+z2+c2) + (z1*z2+c1*c2) + 
      exp(z1*z1 + c1*c1 + z2*z2 + c2*c2);
    return res.real();
  }
  dd_complex dz1_func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex one;
    one.real() = 1; one.imag() = "0";
    dd_complex expo = z1*z1+c1*c1+z2*z2 + c2*c2;
    return  one + z2 + 2*z1*exp( expo );
  }
  dd_complex dz2_func(dd_complex z1, dd_complex z2)
  {
    return dz1_func(z2, z1);
  }
  dd_complex dz1z1_func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex expo = z1*z1+c1*c1+z2*z2 + c2*c2;
    return 2*exp( expo ) + 4*z1*z1*exp(expo);    
  }
  dd_complex dz1z2_func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex one = dd_complex(1,0);
    dd_complex expo = z1*z1+c1*c1+z2*z2 + c2*c2;
    return one + 4*z1*z2*exp(expo);
  }
  dd_complex dz1c1_func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex expo = z1*z1+c1*c1+z2*z2 + c2*c2;
    return 4 * z1 * conj(z1) * exp(expo);
  }
  dd_complex dz1c2_func(dd_complex z1, dd_complex z2)
  {
    dd_complex c1 = conj(z1);
    dd_complex c2 = conj(z2);
    dd_complex expo = z1*z1+c1*c1+z2*z2 + c2*c2;
    return 4 * z1 * conj(z2) * exp(expo);
  }
  dd_real num_grad(dd_real h1x, dd_real h2x, 
		   dd_real h1y, dd_real h2y)
  {
    dd_complex h1 = dd_complex(h1x, h1y);
    dd_complex h2 = dd_complex(h2x, h2y);
    dd_complex fp = func(z01 + h1, z02 + h2);
    dd_complex fm = func(z01 - h1, z02 - h2);
    dd_complex res = (fp-fm)/(2*(h1x+h2x+h1y+h2y));
    EXPECT_DOUBLE_EQ(res.imag().x[0], 0.0);
    EXPECT_DOUBLE_EQ(res.imag().x[1], 0.0);
    return res.real();
  }
  dd_real num_hess(dd_complex h1_1, dd_complex h1_2,
		   dd_complex h2_1, dd_complex h2_2)
  {
    dd_complex f0 = func(z01, z02);
    dd_complex f1 = func(z01+h1_1, z02+h1_2);
    dd_complex f2 = func(z01+h2_1, z02+h2_2);
    dd_complex f12=func(z01+h1_1+h2_1, z02+h1_2+h2_2);

    dd_complex res = (f0+f12-f1-f2) / (h*h);

    EXPECT_DOUBLE_EQ(res.imag().x[0], 0.0);
    EXPECT_DOUBLE_EQ(res.imag().x[1], 0.0);
    return res.real();
  }  

  void test_grad()
  {
    f_z[0] = dz1_func(z01, z02);
    f_z[1] = dz2_func(z01, z02);
    grad_realize(2, f_z, f_a);    
    
    dd_real hx = "0.000001";
    dd_real hy = "0.00001";
    double eps = 0.00001;
    
    EXPECT_NEAR
      (num_grad(hx, 0, 0, 0).x[0],
       f_a[0].x[0],          eps);

    EXPECT_NEAR
      (num_grad(0, hx, 0, 0).x[0],
       f_a[1].x[0],          eps);

    EXPECT_NEAR
      (num_grad(0, 0, hy, 0).x[0],
       f_a[2].x[0],          eps);

    EXPECT_NEAR
      (num_grad(0, 0, 0, hy).x[0],
       f_a[3].x[0],          eps);    
  }
  void test_hess()
  {
    dd_complex f_zz[2*2];
    dd_complex f_zc[2*2];
    dd_complex zero = dd_complex(0,0);

    f_zz[0 + N * 0] = dz1z1_func(z01, z02);
    f_zz[1 + N * 0] = dz1z2_func(z02, z01);
    f_zz[0 + N * 1] = dz1z2_func(z01, z02);
    f_zz[1 + N * 1] = dz1z1_func(z02, z01);

    f_zc[0 + N * 0] = dz1c1_func(z01, z02);

    // d^2
    f_zc[1 + N * 0] = dz1c2_func(z02, z01);
    f_zc[0 + N * 1] = dz1c2_func(z01, z02);
    // d^2 f / (dz_2 dz_2^*)
    f_zc[1 + N * 1] = dz1c1_func(z02, z01);
    
    dd_real f_ab[4*4];
    hess_realize(N, f_zz, f_zc, f_ab);

    double eps = 0.00001;
    unsigned int NN=2*N;
    
    EXPECT_NEAR
      (f_ab[0 + NN * 0].x[0],
       num_hess(dd_complex(h,0), zero,
		dd_complex(h,0), zero).x[0], eps);
    EXPECT_NEAR
      (f_ab[1 + NN * 0].x[0],
       num_hess(zero, dd_complex(h,0),
		dd_complex(h,0), zero
		).x[0], eps);
    EXPECT_DOUBLE_EQ(f_ab[1 + NN * 0].x[0],
		     f_ab[0 + NN * 1].x[0]);
    
    EXPECT_NEAR
      (f_ab[2 + NN * 0].x[0],
       num_hess(dd_complex(0,h), zero, 
		dd_complex(h,0), zero
		).x[0], eps);
    EXPECT_NEAR
      (f_ab[3 + NN * 0].x[0],
       num_hess(zero, dd_complex(0,h),
		dd_complex(h,0), zero
		).x[0], eps);    
    
      

  }
};

TEST_F(ComplexJacobTest, grad)
{
  test_grad();
}
TEST_F(ComplexJacobTest, hess)
{
  test_hess();
}
