// *  Begin    

#ifndef COMPLEX_JACOB_HPP
#define COMPLEX_JACOB_HPP

#include <qd/dd_real.h>
// #include <mpack/dd_complex.h>
#include <dd_complex.h>

// *  function 
// ** grad_realize
// *** dd_complex -> dd_real

/**
 * From gradient of complex function f(z^*,z), 
 * compute derivative by real part and imaginary
 * part of variable z. Output array must allocated 
 * before call grad_realize
 */
void grad_realize(unsigned int num,
		  dd_complex* dfdz,
		  dd_real* dfdx_dfdy)
{

  for(unsigned int i = 0; i < num; i++)
    {
      dfdx_dfdy[i    ] = +2 * dfdz[i].real();
      dfdx_dfdy[i+num] = -2 * dfdz[i].imag();
    }
}


// *** dd_real -> dd_real

void grad_realize(unsigned int num,
		  dd_real* dfdz,
		  dd_real* dfdx_dfdy)
{

  for(unsigned int i = 0; i < num; i++)
    {
      dfdx_dfdy[i    ] = +2 * dfdz[i];
    }
}

// ** hess_realize
// *** dd_complex -> dd_real

/**
 * From hessian of complex function f(z^*, z),
 * compute second derivative by real and imaginary
 * part of z. Output array must allocated 
 * before call hess_realize
 */
void hess_realize(unsigned int num,
		  dd_complex* d2f_dzi_dzj,
		  dd_complex* d2f_dzi_dczj,
		  dd_real* d2f_dadb)
{
  unsigned int n = num;
  unsigned int nn= 2 * n;
  for(unsigned int i = 0; i < n; i++)
    for(unsigned int j = 0; j < n; j++)
    {
      dd_complex f  = d2f_dzi_dzj[i+n*j];
      dd_complex fc = d2f_dzi_dczj[i+n*j];
      dd_real    r_f = f.real();
      dd_real    i_f = f.imag();
      dd_real    r_c = fc.real();
      dd_real    i_c = fc.imag();

      /*
      d2f_dadb[i   + nn* j   ] = +2*r_f +2*r_c;
      d2f_dadb[i   + nn*(j+n)] = -2*i_f -2*i_c;
      d2f_dadb[i+n + nn* j   ] = -2*i_f +2*i_c;
      d2f_dadb[i+n + nn*(j+n)] = -2*r_f + 2*r_c;
      */


      d2f_dadb[i   + nn* j   ] = +2*r_f +2*r_c;
      d2f_dadb[i   + nn*(j+n)] = -2*i_f + 2*i_c;
      d2f_dadb[i+n + nn* j   ] = -2*i_f - 2*i_c;      
      d2f_dadb[i+n + nn*(j+n)] = -2*r_f + 2*r_c;
      
	
    }
}


// *** dd_real -> dd_real

void hess_realize(unsigned int num,
		  dd_real* d2f_dzi_dzj,
		  dd_real* d2f_dzi_dczj,
		  dd_real* d2f_dadb)
{
  unsigned int n = num;
  for(unsigned int i = 0; i < n; i++)
    for(unsigned int j = 0; j < n; j++)
    {
      dd_complex f  = d2f_dzi_dzj[i+n*j];
      dd_complex fc = d2f_dzi_dczj[i+n*j];
      dd_real    r_f = f.real();
      dd_real    r_c = fc.real();
	
      d2f_dadb[i + n*j] = 2 * r_f + 2 * r_c;
	
    }
}


#endif
