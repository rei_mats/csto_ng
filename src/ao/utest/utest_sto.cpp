#include <iostream>
#include <stdio.h>
#include <gtest/gtest.h>
#include <src/ao/sto.hpp>
#include <src/ao/gto.hpp>
#include <src/ao/lcao.hpp>
#include <src/utils/support_mpack.hpp>
#include <sage/result/ref_real200_exp2_erfc.h>

using namespace std;
// using namespace StongMath;

TEST(primitive_STO, Construct) {
  
  STO<double>* sto = STO<double>::New_Normalized(2, 1.1);
  EXPECT_DOUBLE_EQ(sto->Get_z(), 1.1);
  EXPECT_EQ(sto->Get_pn(), 2);
  
  delete(sto);

  sto = STO<double>::New_zero();
  EXPECT_TRUE(sto->Is_zero());

  delete sto;
}
TEST(primitive_STO, dd_complex) {
  
  STO<dd_complex>* sto = STO<dd_complex>::New_Normalized(2, dd_complex("1", "2"));
  EXPECT_DOUBLE_EQ(sto->Get_z().real().x[0], 1.0);
  EXPECT_EQ(sto->Get_pn(), 2);
  
  delete(sto);

  sto = STO<dd_complex>::New_zero();
  EXPECT_TRUE(sto->Is_zero());

  delete sto;
}
TEST(primitive_GTO, Construct) {
  GTO<double>* gto = GTO<double>::New_Normalized(3, 1.2);
  EXPECT_DOUBLE_EQ(gto->Get_z(), 1.2);
  EXPECT_EQ(gto->Get_pn(), 3);

  delete gto;

  gto = GTO<double>::New_zero();
  EXPECT_TRUE(gto->Is_zero());

  delete gto;
}
TEST(primitive_GTO, copy) {

  GTO<double>* g1 = GTO<double>::New_Normalized(2, 1.1);
  GTO<double>* g2 = GTO<double>::New_Normalized(3, 2.1);

  g2->CopyFrom(*g1);

  EXPECT_EQ(g2->Get_pn(), 2);
  EXPECT_DOUBLE_EQ(g2->Get_z(), 1.1);

  delete g1;
  delete g2;

}
TEST(LCAO, GTOs1) {

  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  int   pns[] = {1,   2,   3};
  LCAO<GTO<double>, double>* stong = New_LC_NormalizedGTOs(3,cs,zs,pns);

  EXPECT_DOUBLE_EQ(stong->Get_cs_i(1), 2.2);
  EXPECT_DOUBLE_EQ(stong->Ref_aos_i(2)->Get_z(), 1.3);

  stong->Ref_aos_i(0)->Set_z(3.1);
  EXPECT_DOUBLE_EQ(stong->Ref_aos_i(0)->Get_z(), 3.1);

  std::stringstream ss;
  ss << *stong;
  EXPECT_EQ(ss.str(), 
	    "2.1G_1(3.1) + 2.2G_2(1.2) + 2.3G_3(1.3)");

  delete stong;
  
}
TEST(LCAO, GTOs2) {

  dd_real zs[] = {1.1, 1.2, 1.3};
  dd_real cs[] = {2.1, 2.2, 2.3};
  LCAO<GTO<dd_real>, dd_real>* stong 
    = New_LC_NormalizedGTOs(1, 3,cs,zs);

  EXPECT_DOUBLE_EQ(stong->Get_cs_i(1).x[0], 2.2);
  EXPECT_DOUBLE_EQ(stong->Ref_aos_i(2)->Get_z().x[0], 1.3);
  EXPECT_EQ(stong->Ref_aos_i(1)->Get_pn(), 1);

  delete stong;
  
}
TEST(LCAO, MonoSTO) {

  STO<double>* prim_sto =STO<double>::New_Normalized(2, 1.1);
  LCAO<STO<double>, double>* sto = New_LC_Mono<STO<double>, double>(*prim_sto);
  delete prim_sto;
  
  EXPECT_DOUBLE_EQ(sto->Ref_aos_i(0)->Get_z(), 1.1);
  EXPECT_DOUBLE_EQ(sto->Ref_aos_i(0)->Get_pn(), 2);

  delete sto;

}
TEST(LCAO, Zero) {
  LCAO<GTO<double>, double>* lc_zero =
    LCAO<GTO<double>, double>::New_zero();
  EXPECT_EQ(0, lc_zero->Get_num());
  delete lc_zero;
}
TEST(LCAO, GTOs_r) {

  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  LCAO<GTO<double>, double>* stong 
    = New_LC_NormalizedGTOs(2, 3,cs,zs);
  EXPECT_NEAR(stong->At_r(1.5),
	      3.65046, 
	      0.00001);
  
  delete stong;
  
}
TEST(LCAO, ScalarProduct) {

  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  LCAO<GTO<double>, double>* stong 
    = New_LC_NormalizedGTOs(2, 3,cs,zs);
  stong->ScalarProduct(2.0);

  EXPECT_DOUBLE_EQ(stong->Get_cs_i(1), 4.4);
  
  delete stong;
  
}

/*
TEST(double_STO, Construct) {
  STO<double> sto(3);
  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  int   pns[] = {1,   2,   3};
  //  double ns[] = {3.1, 3.2, 3.3};

  double eps = 0.000000001;

  sto.Set_zs_without_normalization(zs);
  sto.Set_cs(cs);
  sto.Set_pns_without_normalization(pns);
  
  EXPECT_EQ(sto.Get_pns_i(0), 1);
  EXPECT_EQ(sto.Get_pns_i(2), 3);
  EXPECT_EQ(sto.Get_pns_i(1), 2);

  EXPECT_NEAR(sto.Get_zs_i(1), 1.2, eps);
  EXPECT_NEAR(sto.Get_cs_i(0), 2.1, eps);
  //  EXPECT_NEAR(sto.Get_ns_i(2), 3.3, eps);
}
TEST(double_STO, NormalizedSTO) {

  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  int   pns[] = {1,   2,   3};
  
  STO<double>* sto = STO<double>::NormalizedSTOs(3, zs, cs, pns);
  
  
  EXPECT_EQ(sto->Get_pns_i(0), 1);
  EXPECT_EQ(sto->Get_pns_i(2), 3);
  EXPECT_EQ(sto->Get_pns_i(1), 2);

  sto->Display();

  delete(sto);
  
}
TEST(double_STO, NormalizedSTO2) {

  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  
  STO<double>* sto = STO<double>::NormalizedSTOs(3, zs, cs, 2);

  EXPECT_EQ(sto->Get_pns_i(0), 2);
  
  delete(sto);
}

*/



