#include <gtest/gtest.h>
#include <qd/dd_real.h>
#include <dd_complex.h>

#include <src/math/factorial.hpp>
#include <src/utils/support_string.hpp>
#include <src/utils/support_qd.hpp>
#include <src/ao/lcao.hpp>
#include <src/ao/d_ao.hpp>


#define DEBUG
TEST(T_n, T3)
{
  dd_complex z("0.1", "-0.1");
  dd_complex t3 = calc_T_n_z_GTO(3, z);
  dd_complex expe
    ("9.9215674164922147143810590761472265964134719365592",
     "9.9215674164922147143810590761472265964134719365592"
     );
  EXPECT_DOUBLE_EQ(expe.real().x[0], t3.real().x[0]);
  // EXPECT_DOUBLE_EQ(expe.imag().x[0], t3.imag().x[0]);
  
} 
TEST(d_AO, d_GTO_num)
{
  GTO<double> *gto;
  LCAO<GTO<double>, double> *dz_gto;
  
  gto = GTO<double>::New_Normalized(2, 1.1);
  dz_gto = MAlloc_LC_D_zeta(*gto);
  Set_D_zeta(*gto, dz_gto);
  
  EXPECT_EQ(dz_gto->Get_num(), (unsigned int)2);

  delete gto;
  delete dz_gto;
}
TEST(d_AO, d2_GTO_num)
{
  GTO<double> *gto= GTO<double>::New_Normalized(2,1.1);
  LCAO<GTO<double>, double> *d2z_gto;
  d2z_gto = MAlloc_LC_D2_zeta(*gto);
  Set_D2_zeta(*gto, d2z_gto);

  EXPECT_EQ(d2z_gto->Get_num(), (unsigned int)3);

  delete gto;
  delete d2z_gto;
}
TEST(d_AO, d_GTO_value)  
{
  GTO<double> *gto, *gto_p, *gto_m; 
  LCAO<GTO<double>, double> *dz_gto;
  double z0 = 1.1; double dz = 0.001;
  int    n0 = 2;
  
  gto    = GTO<double>::New_Normalized(n0, z0);
  gto_p  = GTO<double>::New_Normalized(n0, z0+dz);
  gto_m  = GTO<double>::New_Normalized(n0, z0-dz);
  
  dz_gto = MAlloc_LC_D_zeta(*gto);
  Set_D_zeta(*gto, dz_gto);

  double r0 = 2.2;
  double eps = pow(10.0, -5.0);

  double dz_gto_r0 =
    (gto_p->At_r(r0) - gto_m->At_r(r0))/ (2 * dz);
  EXPECT_NEAR
    (dz_gto->At_r(r0), dz_gto_r0, eps);

  delete gto;
  delete gto_p;
  delete gto_m;
  delete dz_gto;
}
TEST(d_AO, d2_GTO_value)
{
  double z0 = 1.1; 
  int    n0 = 2;
  double dz = 0.001;
    
  GTO<double> *gto, *gto_p, *gto_m; 
  gto    = GTO<double>::New_Normalized(n0, z0);
  gto_p  = GTO<double>::New_Normalized(n0, z0+dz);
  gto_m  = GTO<double>::New_Normalized(n0, z0-dz);
  
  LCAO<GTO<double>, double> *d2z_gto;
  d2z_gto = MAlloc_LC_D2_zeta(*gto);
  Set_D2_zeta(*gto, d2z_gto);
  
  double r0 = 2.2;
  double eps = pow(10.0, -5.0);

  double dz2_gto_r0 =
    (gto_p->At_r(r0) +
     gto_m->At_r(r0) -
     2 * gto->At_r(r0)) / (dz * dz);
  EXPECT_NEAR
    (d2z_gto->At_r(r0), dz2_gto_r0, eps);

  delete gto;
  delete gto_p;
  delete gto_m;
  delete d2z_gto;
  
}
TEST(d_AO, grad)
{
  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  int   pns[] = {1,   2,   3};
  
  LCAO<GTO<double>, double>* stong = 
    New_LC_NormalizedGTOs(3,cs,zs,pns);
  
  LCAO<GTO<double>, double>** grad;
  
  grad = MAlloc_grad_mu(*stong);
  Set_grad_mu(*stong, grad);

  double ans[] = {
    0.0115366825495261,
    0.0210155119730095,
    0.0319570658247830,
    -0.123029280286791,
    -0.218148019450496,
    -0.324423215781532};

  for(unsigned int i = 0; i < 6; i++)
    EXPECT_NEAR(ans[i], grad[i]->At_r(2.4), 0.0000001);

  Delete_grad_mu(*stong, grad);
  delete stong;
  
}
TEST(d_AO, hess)
{

  // this result is obtained in daily note at 2015/1/14.
  double ans[] = {
    0.0,
    0.0,
    0.0,
    -0.0585853715651387,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    -0.0991581906593164,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0,
    -0.141053572078927,
    -0.0585853715651387,
    0.0,
    0.0,
    0.609748298045219,
    0.0,
    0.0,
    0.0,
    -0.0991581906593164,
    0.0,
    0.0,
    0.989161281547691,
    0.0,
    0.0,
    0.0,
    -0.141053572078927,
    0.0,
    0.0,
    1.35584339991480};

  
  double zs[] = {1.1, 1.2, 1.3};
  double cs[] = {2.1, 2.2, 2.3};
  int   pns[] = {1,   2,   3};
  unsigned int n = 3; unsigned int N=n*2;
  
  LCAO<GTO<double>, double>* stong = 
    New_LC_NormalizedGTOs(3,cs,zs,pns);

  LCAO<GTO<double>, double>** hess;
  
  hess = MAlloc_hess_mu_nu(*stong);
  Set_hess_mu_nu(*stong, hess);

  double r0 = 2.4;
  for(unsigned int i = 0; i < N*N; i++)
    EXPECT_NEAR(hess[i]->At_r(r0), ans[i], 0.0000001);
  Delete_hess_mu_nu(*stong, hess);
  delete stong;  
}
TEST(TestOneComplex, hess)
{
  typedef GTO<dd_complex> G;
  G* g;
  LCAO<G, dd_complex>* ddg;
  
  g = G::New_Normalized(1, dd_complex("0.1", "-0.1"));
  ddg = MAlloc_LC_D2_zeta(*g);
  Set_D2_zeta(*g, ddg);

  delete g;
  delete ddg;
}
class TestComplex : public ::testing::Test
{
public:
  static const unsigned int n = 2;
  static const unsigned int N = 4 * n;
  dd_complex zs[n];
  dd_complex cs[n];
  int    pns[n];
  static const int pn =1;
  
  LCAO<GTO<dd_complex>, dd_complex>* stong; 

  virtual void SetUp()
  {
    zs[0] = dd_complex("0.1", "-0.1");
    zs[1] = dd_complex("0.9", "-0.5");
    cs[0] = dd_complex("0.5", "-0.5");
    cs[1] = dd_complex("0.25", "0.5");
    pns[0] = 1; pns[1] = 1;
    stong = New_LC_NormalizedGTOs(n,cs,zs,pns);    
  }
};    
TEST_F(TestComplex, grad)
{
  
  LCAO<GTO<dd_complex>, dd_complex>** grad;
  grad = MAlloc_grad_mu(*stong);
  Set_grad_mu(*stong, grad);

  dd_complex numeric[] = {
    dd_complex("0.78599886477915501090345287522383507362278217633176", "-0.01025678464544864545458863866652076512498413441302"),
    dd_complex("-0.027825221920008724737479771180490444652960586881079", "0.020800105652920182618355012078369676585997739073904"),
    dd_complex("0.7133585521367569583952192808043163176117748005181", "2.2547533279224261098564521650047719463748764708961"),
    dd_complex("0.092002508135956967708740990899608336046740022347634", "0.038496157806404954870555516250303603210485301894388")
  };
  
  for(unsigned int i = 0; i < 2 * n ; i++)
    {
      dd_complex r = dd_complex("2.4", "0");
      dd_complex nume_i = numeric[i];
      dd_complex calc_i = grad[i]->At_r(r);
      EXPECT_DOUBLE_EQ(nume_i.real().x[0], calc_i.real().x[0]);
    }
  Delete_grad_mu(*stong, grad);
  
}
TEST_F(TestComplex, hess)
{
  LCAO<GTO<dd_complex>, dd_complex>** hess;
  hess = MAlloc_hess_mu_nu(*stong);
  Set_hess_mu_nu(*stong, hess);

  dd_complex zero = value_zero<dd_complex>();
  
  dd_complex numeric[] = {
    zero,
    zero,
    dd_complex("-1.5413947757856691514612328842004556287631016703780", "2.9681118800591830682516714458090882639866512714143"),
    zero,

    zero,
    zero,
    zero,
    dd_complex
    ("0.13519585899901350195988161872017243397416850090913",
     "-0.11640708677240718443754117243913045510639579424070"),
    
    dd_complex
    ("-1.5413947757856691514612328842004556287631016703780", 
     "2.9681118800591830682516714458090882639866512714143"),
    zero,
    dd_complex("-24.818969096215297955049364758628742573826072441367", "-16.402123621118232238995600984093048705503059490535"),
    zero,
    
    zero,
    dd_complex
    ("0.13519585899901350195988161872017243397416850090913",
     "-0.11640708677240718443754117243913045510639579424070"),  
    zero,
    dd_complex
    ("-0.48371315123933789806447317406056871410696559946721",
     "-0.15099220746521926947127853881844664751995435546159")
  };
  for(unsigned int i = 0; i < 2 * n ; i++)
    for(unsigned int j = 0; j < 2 * n ; j++)
      {
	unsigned int idx = i + (2*n) * j;
	dd_complex nume_i = numeric[idx];
	dd_complex r = dd_complex("2.4", "0");
	dd_complex calc_i = hess[idx]->At_r(r);
	EXPECT_DOUBLE_EQ(nume_i.real().x[0], 
			 calc_i.real().x[0]) <<
	  "i, j : " << i << ", " << j << endl;
      }
  Delete_hess_mu_nu(*stong, hess);
}



