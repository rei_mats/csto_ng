#include <iostream>
#include <stdio.h>
#include <gtest/gtest.h>
#include <src/utils/support_mpack.hpp>
#include <src/ao/inner_prod.hpp>
#include <sympy/result/ref_stong.h>

void test_dd_real(dd_real calc, dd_real expe) {
  double  eps = std::pow(10.0, -30.0);
  EXPECT_DOUBLE_EQ(expe.x[0], calc.x[0]);
  EXPECT_NEAR(expe.x[1], calc.x[1], eps);
}

TEST(IP, normalizedQ) {

  STO<double>* sto = STO<double>::New_Normalized(2, 1.1);

  double norm2 = HIP(*sto, *sto);

  EXPECT_DOUBLE_EQ(norm2, 1.0);

  delete sto;

}
TEST(IP, zero) {

  STO<double>* sto = STO<double>::New_zero();

  double norm2 = HIP(*sto, *sto);

  EXPECT_DOUBLE_EQ(norm2, 0.0);

  delete sto;  

}
TEST(IP, STO_STO) {

  STO<dd_real>* sto1 = STO<dd_real>::New_Normalized(2, "1.1");
  STO<dd_real>* sto2 = STO<dd_real>::New_Normalized(2, "1.3");

  dd_real calc = CIP(*sto1, *sto2);
  dd_real expe = sto_sto_r;

  test_dd_real(calc, expe);

  delete sto1;
  delete sto2;
}
TEST(IP, GTO_GTO) {

  GTO<dd_real>* gto1 = GTO<dd_real>::New_Normalized(2, "1.3");
  GTO<dd_real>* gto2 = GTO<dd_real>::New_Normalized(2, "2.3");
  
  dd_real calc = CIP(*gto1, *gto2);
  dd_real expe = gto_gto_r;
  test_dd_real(calc, expe);

  delete gto1;
  delete gto2;
}
TEST(IP, STO_GTO) {
  STO<dd_real>* sto1 = STO<dd_real>::New_Normalized(2, "1.1");
  GTO<dd_real>* gto1 = GTO<dd_real>::New_Normalized(2, "1.3");
  dd_real calc = CIP(*gto1, *sto1);
  dd_real expe = sto_gto_r;
  test_dd_real(calc, expe);

  delete sto1;
  delete gto1;
}
TEST(IP, STO_GTOs) {

  dd_real cs[] = {"2.1","3.1","3.1"};
  dd_real zs[] = {"1.2","1.3","1.1"};
  LCAO<GTO<dd_real>, dd_real>* stong = New_LC_NormalizedGTOs
    (/* pn = */2, /* num = */3, cs, zs);
  LCAO<STO<dd_real>, dd_real>* sto = New_LC_MonoSTO
    <dd_real>(/*pn=*/ 2, /*z=*/"1.1");

  dd_real calc = CIP(*sto, *stong);
  dd_real expe = stong_r;
  double  eps = std::pow(10.0, -30.0);

  EXPECT_DOUBLE_EQ(expe.x[0], calc.x[0]);
  EXPECT_NEAR(expe.x[1], calc.x[1], eps);

  delete stong;
  delete sto;

}
TEST(IP, distance) {


  dd_complex z1 = dd_complex("1.1", "0.3");

  STO<dd_complex>* s1 = STO<dd_complex>::New_Normalized(2, z1);

  dd_complex z2 = dd_complex("0.3", "2.3");

  STO<dd_complex>* s2 = STO<dd_complex>::New_Normalized(3, z2);

  LCAO<STO<dd_complex>, dd_complex>* s1s2 =
    new LCAO<STO<dd_complex>, dd_complex>(2);
  s1s2->Set_cs_i(0, dd_complex(1,0));
  s1s2->Set_cs_i(1, dd_complex(-1,0));
  s1s2->Set_aos_i(0, s1);
  s1s2->Set_aos_i(1, s2);


  dd_complex d, d1;
  d = CDistance2<STO<dd_complex>, dd_complex>(*s1, *s1);
  EXPECT_DOUBLE_EQ(d.real().x[0], 0.0);
  EXPECT_DOUBLE_EQ(d.real().x[1], 0.0);

  d = HDistance2<STO<dd_complex>, dd_complex>(*s2,*s2);
  EXPECT_DOUBLE_EQ(d.real().x[0], 0.0);
  EXPECT_DOUBLE_EQ(d.real().x[1], 0.0);

  d = CDistance2<STO<dd_complex>, dd_complex>(*s1,*s2);
  d1 = CNorm2<LCAO<STO<dd_complex>, dd_complex>, dd_complex>(*s1s2);
  EXPECT_DOUBLE_EQ(d.real().x[0], d1.real().x[0]);

  //  delete s1;
  //  delete s2;
  delete s1s2;

}
 
TEST(IP, HIP_LCAO) {
  dd_complex zs[] = {dd_complex(1, 0),
		     dd_complex(2, 1)};
  dd_complex cs[] = {dd_complex(1, 3),
		     dd_complex(2, 2)};
  LCAO<STO<dd_complex>, dd_complex>* sto
    = New_LC_MonoSTO(2, dd_complex(1, 0));
  LCAO<GTO<dd_complex>, dd_complex>* gto
    = New_LC_NormalizedGTOs(2, 2, zs, cs);

  std::cout << HIP(*sto, *gto).real().x[0] << std::endl;
    std::cout << HIP(*sto, *gto).imag().x[0] << std::endl;
	    

  delete sto;
  delete gto;

}
