// *  Begin           
// ** comment

/**
 * @file d_ao.hpp
 * @brief derivative basis for GTO.
 * In current version, only derivative of GTO is supported.
 * Extention to STO is easily possible.
 * @date 2014/1/7 new
 * @author Rei Matsuzaki
 */


// ** ifndef

#ifndef D_AO_HPP
#define D_AO_HPP




// *  Functions       
// ** coefficient
// *** S_n(z)

/** compute coefficient of derivative basis for GTO 
 *  see document for detail.
 */
template<class F>
F calc_S_n_z_GTO(int n, F z) {
  // return 1 / (4 * z) + n / (2 * z);
  F one = value_one<F>();
  F two = 2 * one;

  F term1 = (one / 4 + n / two);
  
  return  term1 / sqrt(z * z);
}

// *** S_n'(z)

//** compute derivative of S_n(z) */
template<class F>
F calc_S_n_prime_z_GTO(int n, F z) {
  F one = value_one<F>();
  F two = 2 * one;
  // return -1 / (4 * z * z)  - n / (2 * z * z);
  F sz = sqrt(z * z);
  return -(one/4 + n/two) * ( z / (sz * sz * sz));
}


// *** T_n(z)

/** compute second coefficient of derivative basis for GTO
 * see document for detail.
 */
template<class F>
F calc_T_n_z_GTO(int nn, F z) {

  F one = value_one<F>();
  F n = one * nn;
  F term1 = -sqrt((3 * one / 2 + n * one) * (one / 2 + n * one)) / 2;



  F term2 = 
    sqrt(pow(sqrt(z), 2 * nn + 1)) / 
    sqrt(pow(sqrt(z), 2 * nn + 5));

  return term1 * term2;
}

// *** T_n'(z)

/** compute derivative of T_n(z). */
template<class F>
F calc_T_n_prime_z_GTO(int n, F z) {
  F one = value_one<F>();
  //  F sz = sqrt(z * z);

  F term1 = -sqrt((3 * one / 2 + n * one) * (one / 2 + n * one)) / 2;

  //  F term2 = ( z / (sz * sz * sz));

  F sq = sqrt(z);
  
  F term2 = pow(sq,2*n-1) / (
    sqrt(pow(sq, 2*n+1)) *
    sqrt(pow(sq,2*n+5)));
  
  return -term1 * term2;
    

}


// ** derivative with orbital exp.
// *** allocate d/dz GTO

/** allocate memory for d/dz GTO */
template<class F>
LCAO<GTO<F>, F>* MAlloc_LC_D_zeta(const GTO<F>& gto) {

  LCAO<GTO<F>, F>* ptr = new LCAO<GTO<F>, F>(2);
  GTO<F>* g0 = new GTO<F>(false);
  GTO<F>* g1 = new GTO<F>(false);
  ptr->Set_aos_i(0, g0);
  ptr->Set_aos_i(1, g1);

  return ptr;

}

// *** set value for d/dz GTO

template<class F>
void Set_D_zeta(const GTO<F>& gto, LCAO<GTO<F>, F>* lcao) {

  int pn = gto.Get_pn();
  F   z  = gto.Get_z();
  
  lcao->Set_cs_i(0, calc_S_n_z_GTO(pn, z));
  lcao->Set_cs_i(1, calc_T_n_z_GTO(pn, z));

  lcao->Ref_aos_i(0)->Set_z_without_normalization(z);
  lcao->Ref_aos_i(0)->Set_pn(pn);
  lcao->Ref_aos_i(1)->Set_z_without_normalization(z);
  lcao->Ref_aos_i(1)->Set_pn(pn+2);

}

// *** allocate d^2/dz^2 GTO

/** allocate memory for d^2/dz^2 GTO */
template<class F>
LCAO<GTO<F>, F>* MAlloc_LC_D2_zeta(const GTO<F>& gto) 
{
  LCAO<GTO<F>, F>* ptr = new LCAO<GTO<F>, F>(3);
  GTO<F>* g0 = new GTO<F>(false);
  GTO<F>* g1 = new GTO<F>(false);
  GTO<F>* g2 = new GTO<F>(false);
  ptr->Set_aos_i(0, g0);
  ptr->Set_aos_i(1, g1);
  ptr->Set_aos_i(2, g2);

  return ptr;
}

// *** set value for d^2/dz^2 GTO

template<class F>
void Set_D2_zeta(const GTO<F>& gto, 
		 LCAO<GTO<F>, F>* lcao)
{

  int pn = gto.Get_pn();
  F   z  = gto.Get_z();

  F Sn   = calc_S_n_z_GTO(pn, z);
  F Sn_p = calc_S_n_prime_z_GTO(pn, z);
  F Sn2  = calc_S_n_z_GTO(pn+2,z);
  F Tn   = calc_T_n_z_GTO(pn, z);
  F Tn_p = calc_T_n_prime_z_GTO(pn, z);
  F Tn2  = calc_T_n_z_GTO(pn+2, z);

  for(unsigned int i = 0; i < 3; i++)
    lcao->Ref_aos_i(i)->Set_z_without_normalization(z);

  lcao->Ref_aos_i(0)->Set_pn(pn);
  lcao->Ref_aos_i(1)->Set_pn(pn+2);
  lcao->Ref_aos_i(2)->Set_pn(pn+4);

  F c0 = Sn_p + Sn * Sn;
  F c1 = Sn * Tn + Tn_p + Tn * Sn2;
  F c2 = Tn * Tn2;
  
  lcao->Set_cs_i(0, c0);
  lcao->Set_cs_i(1, c1);
  lcao->Set_cs_i(2, c2);
}

// *** d/dz GTO, (not used)

/** compute derivative of GTO with its orbital exponent */
/*
template<class F>
LCAO<GTO<F>, F>* New_LC_D_zeta(const GTO<F>& gto) {

  LCAO<GTO<F>, F>* res = new LCAO<GTO<F>, F>(2);
  int pn = gto.Get_pn();
  F   z  = gto.Get_z();
  
  res->Set_cs_i(0, calc_S_n_z_GTO(pn, z));
  res->Set_cs_i(1, calc_T_n_z_GTO(pn, z));
  
  GTO<F>* g0 = GTO<F>::New_Normalized(pn, z);
  GTO<F>* g1 = GTO<F>::New_Normalized(pn+2, z);
  
  res->Set_aos_i(0, g0);
  res->Set_aos_i(1, g1);

  return res;
}
*/

// *** d2/dz2 GTO, (not used)

/*
template<class F>
LCAO<GTO<F>, F>* New_LC_D2_zeta(const GTO<F>& gto) {

  LCAO<GTO<F>, F>* res = new LCAO<GTO<F>, F>(3);
  int pn = gto.Get_pn();
  F   z  = gto.Get_z();

  F Sn   = calc_S_n_z_GTO(n, z);
  F Sn_p = calc_S_n_prime_z_GTO(n, z);
  F Sn2  = calc_S_n_z_GTO(n+2,z);
  F Tn   = calc_T_n_z_GTO(n, z);
  F Tn_p = calc_T_n_prime_z_GTO(n, z);
  F Tn2  = calc_T_n_z_GTO(n+2, z);
  
  res->Set_cs_i(0, Sn_p + Sn * Sn);
  res->Set_cs_i(1, Sn * Tn + Tn_p + Tn * Sn2);
  res->Set_cs_i(2, Tn * Tn2);
  
  GTO<F>* g0 = GTO<F>::Normalized(pn, z);
  GTO<F>* g1 = GTO<F>::Normalized(pn+2, z);
  GTO<F>* g2 = GTO<F>::Normalized(pn+4, z);
  
  res->Set_aos_i(0, g0);
  res->Set_aos_i(1, g1);
  res->Set_aos_i(2, g2);

  return res;
}
*/


#endif
// ** d STO-NG / d mu
// *** allocate d/d(mu)

/** allocate memory for d/d(mu) GTOs  (mu=c_i,z_i) */
template<class F>
LCAO<GTO<F>, F>** MAlloc_grad_mu(const LCAO<GTO<F>, F>& stong)
{
  unsigned int num = stong.Get_num();
  LCAO<GTO<F>, F>** grad = new LCAO<GTO<F>, F>*[2*num];

  
  for(unsigned int i = 0; i < num; i++) 
    {
      grad[i]     = new LCAO<GTO<F>, F>(1);
      GTO<F>* gto = new GTO<F>(false);
      grad[i]->Set_aos_i(0, gto);
      grad[i+num] = MAlloc_LC_D_zeta(stong.Get_aos_i(i));
    }
  return grad;
}

// *** set value for d/d(mu)

/** set values for d/d(mu) GTOs (mu=c_i, z_i) */
template<class F>
void Set_grad_mu(const LCAO<GTO<F>, F>& stong,
		 LCAO<GTO<F>, F>** grad)
{
  F one = value_one<F>();
  unsigned int num = stong.Get_num();

  for(unsigned int i = 0; i < num; i++)
    {
      grad[i]->Set_cs_i(0, one);
      grad[i]->Ref_aos_i(0)->CopyFrom(stong.Get_aos_i(i));

      Set_D_zeta(stong.Get_aos_i(i), grad[i+num]);
      grad[i+num]->ScalarProduct(stong.Get_cs_i(i));
    }
}

// *** delete for d/d(mu)

/** delete for d/d(mu) GTOs (mu=c_i, z_i) */
template<class F>
void Delete_grad_mu(const LCAO<GTO<F>, F>& stong,
		    LCAO<GTO<F>, F>** grad)
{
  unsigned int num = stong.Get_num();
  for(unsigned int i = 0; i < 2 * num; i++)
    delete grad[i];
  delete[] grad;
}

// ** d2 STO-NG / d mu
// *** allocate


/** allocate memory for d^2/d(mu)d(nu) GTOs (mu,nu=c_i,z_i)*/
template<class F>
LCAO<GTO<F>, F>** MAlloc_hess_mu_nu(const LCAO<GTO<F>, F>& stong)
{
  unsigned int n = stong.Get_num();
  int N = 2 * n;
  LCAO<GTO<F>, F>** hess = new LCAO<GTO<F>, F>*[N*N];

  for(unsigned int i = 0; i < n; i++)
    for(unsigned int j = 0; j < n; j++)
      {
	hess[i+ N*j] = LCAO<GTO<F>, F>::New_zero();

	if(i == j) 
	  {
	    const GTO<F>& g_i   = stong.Get_aos_i(i);
	    hess[i+n + N*j]     = MAlloc_LC_D_zeta(g_i);
	    hess[i   + N*(j+n)] = MAlloc_LC_D_zeta(g_i);
	    hess[i+n + N*(j+n)] = MAlloc_LC_D2_zeta(g_i);
	  }
	else
	  {
	    hess[i+n + N*j]     = LCAO<GTO<F>, F>::New_zero();
	    hess[i   + N*(j+n)] = LCAO<GTO<F>, F>::New_zero();
	    hess[i+n + N*(j+n)] = LCAO<GTO<F>, F>::New_zero();
	  }
      }
  return hess;
}
// *** set

/** set value for d^2/d(mu)d(nu) GTOs (mu,nu=c_i,z_i) */
template<class F>
void Set_hess_mu_nu(const LCAO<GTO<F>, F>& stong,
		    LCAO<GTO<F>, F>** hess)
{
  unsigned int n = stong.Get_num();
  unsigned int N = 2 * n;
  
  for(unsigned int i = 0; i < n; i++)
    {
      Set_D_zeta(stong.Get_aos_i(i), hess[i   + N*(i+n)]);
      Set_D_zeta(stong.Get_aos_i(i), hess[i+n + N*(i)  ]);
      
      Set_D2_zeta(stong.Get_aos_i(i), hess[i+n + N*(i+n)]);
      hess[i+n + N*(i+n)]->ScalarProduct(stong.Get_cs_i(i));
    }
}


// *** delete

/** delete*/
template<class F>
void Delete_hess_mu_nu(const LCAO<GTO<F>, F>& stong,
		       LCAO<GTO<F>, F>** hess)
{
  unsigned int n = stong.Get_num();
  for(unsigned int i = 0; i < 4 * n * n ; i++)
    delete hess[i];
  delete[] hess;
}


// *** first derivative


/** compute derivative GTOs with coefficient and orbital exp.*/
/*
template<class F>
LCAO<GTO<F>,F>** NewSeq_gradGTOs(const LCAO<GTO<F>, F>& stong) {

  int num = stong.Get_num();
  F one = value_one<F>();
  
  LCAO<GTO<F>, F>** grad = new LCAO<GTO<F>,F>[2*num];

  for(unsigned int i = 0; i < num; i++) 
    {
      LCAO<GTO<F>, F>* dc_stong = 
	new LCAO<GTO<F>, F>(1);
      GTO<F>* gto = new GTO<F>(stong.Get_aos_i(i));
      dc_stong->Set_cs_i(0, one);
      dc_stong->Set_aos_i(0, gto);
    
      LCAO<GTO<F>, F>* dz_stong = 
	New_LC_D_zeta(stong->Get_aos_i(i));
      
      grad[i]     = dc_stong;
      grad[i+num] = dz_stong;
    
  }

  return grad;
}
*/

// *** second derivative

/** compute second derivative GTOs with coefficient and orbital exp.*/
/*
template<class F>
LCAO<GTO<F>, F>** NewSeq_hessGTOs(const LCAO<GTO<F>, F>& stong) {

  int num = stong.Get_num();
  F one = value_one<F>();

  LCAO<GTO<F>, F>** ptr = new LCAO<GTO<F>, F>[4 * num * num];
  
  
}
*/



