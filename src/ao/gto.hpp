// *  Begin
// ** comment

/**
 * @file gto.hpp
 * @brief Normalized primitive Radial Gauss type orbital basis.
 * @date 2014/1/7 new
 * @author Rei matsuzaki
 */

// ** ifndef

#ifndef GTO_HPP
#define GTO_HPP


// ** include

#include <src/math/sto_gto_int.hpp>


// ** start class

/**
 * normalized primitive radial Gauss type orbital basis.
 *
 */
template<class F>
class GTO {

// *  Member
// ** Field

private:
  F           z;     /** orbital exponent*/
  int         pn;    /** principle number  */
  F           n;     /** normalized constant */
  bool        is_zero; /** true=> this is zero */

// ** constructor/destructor
// *** constructor

public:
  /**
   * Constructor
   */
  GTO(bool _is_zero): is_zero(_is_zero) {}


// *** Destructor

  /** Destructor */
  ~GTO() {
  }



// ** accessor
// *** getter

  /** get zeta */
  const F Get_z() { return z; }
  const F Get_z() const { return z; }

  /** get principle number  */
  const int Get_pn() { return pn; }
  const int Get_pn() const { return pn; }

  /** get normalization constant */
  const F Get_n() { return n; }
  const F Get_n() const { return n; }

  /** get is zero or not */
  const bool Is_zero() { return is_zero; }
  const bool Is_zero() const { return is_zero; }

  /** get function value at r*/
  const F At_r(F r)
  {
    return n * pow(r, pn) * exp(-z*r*r);
  }
  const F At_r(F r) const
  {
    return n * pow(r, pn) * exp(-z*r*r);
  }


// *** setter

public:
  //** set z without normalization*/
  void Set_z_without_normalization(F _zs) {
    z = _zs;
    }

  //** set pns without normalization*/
  void Set_pn_without_normalization(int _pn) {
    pn = _pn;
  }

  //** set zs with normalization*/
  void Set_z(F _z) {
    z = _z;
    Normalize();
  }

  //** set pn with normalization*/
  void Set_pn(int _pn) {
    pn = _pn;
    Normalize();
  }

  //** copy */
  void CopyFrom(const GTO<F>& o)
  {
    Set_pn_without_normalization(o.Get_pn());
    Set_z_without_normalization(o.Get_z());
    Normalize();
  }

// ** action
// *** normalization

   //** compute and store normalization constant */
  void Normalize() {
    F norm2 = GTO_int(2 * pn, 2 * z);
    n = 1/sqrt(norm2);
  }

// ** IO
// *** display

  /** display internal state of this object */
  void Display() {

    std::cout << std::endl;
    std::cout << "==========================" << std::endl;
    std::cout << "===GTO.Display============" << std::endl;
    std::cout << std::endl;
    std::cout << "z, c, pn, n " << std::endl;
    std::cout << z << " ";
    std::cout << pn << " ";
    std::cout << n << std::endl;
    std::cout << std::endl;
    std::cout << "==========================" << std::endl;

  }
// *** to_s

  /*
  std::string to_s() const
  {
    return "G_" + Get_pn() + "(" << Get_z() + ")";
  }
  */

// *** cout

  template<class FF>
  friend ostream& operator<<(ostream& os, const GTO<FF>& g);

// ** Factory (static)
// *** normalized

  /** compute normalized STOs */
  static GTO* New_Normalized(int _pn, F _z) {

    GTO* f= new GTO(false);
    f->Set_z_without_normalization(_z);
    f->Set_pn_without_normalization(_pn);
    f->Normalize();
    return f;
  }

// *** zero

  /** compute zero */
  static GTO* New_zero() {
    GTO* f = new GTO(true);
    return f;
  }


// *  End

};

// *  function
// *** cout

/*
ostream& operator<<(ostream& os, const GTO<double>& g) {
  os << "G_" << g.Get_pn() << "(" << g.Get_z() << ")";
  return os;
}
*/

template<class F>
ostream& operator<<(ostream& os, const GTO<F>& g) {

  os << "G_" << g.Get_pn() << "(" << g.Get_z() << ")";
  return os;
}


#endif
