// *  Begin      
// ** comment

/**
 * @file lcao.hpp
 * @brief linear combination of AO (STO or GTO in this program)
 * @date 2014/1/7 new
 * @author Rei Matsuzaki
 */


// ** ifndef

#ifndef LCAO_HPP
#define LCAO_HPP


// ** include

#include <src/ao/sto.hpp>
#include <src/ao/gto.hpp>


// ** start class

/**
 * linear combination of STOs or GTOs.
 */

template<class AO, class F>
class LCAO {

// *  Member     
// ** field
  
private:
  unsigned int num; /** number of AO      */
  AO** aos;         /** AO array          */
  F* cs;            /** coefficient array */


// ** constructor/destructor
// *** constructor

public:
  LCAO(unsigned int _num) :
    num(_num) {
    aos = new AO*[_num];
    cs = new F[_num];
  }


// *** destructor

public:
  ~LCAO() {
    for(unsigned int i = 0; i < num; i++)
      delete(aos[i]);

    delete[] aos;
    delete[] cs;
  }

// ** Accessor
// *** reference
  
public:

  /** return the pointer of i th AO. */
  AO* Ref_aos_i(unsigned int i) {
    return aos[i];
  }
  AO* Ref_aos_i(unsigned int i) const {
    return aos[i];
  }

// *** getter

public:

  /** get i th AO  */
  const AO& Get_aos_i(unsigned int i) { return *aos[i]; }
  const AO& Get_aos_i(unsigned int i) const { return *aos[i]; }

  /** get number of terms */
  const unsigned int Get_num() { return num; }
  const unsigned int Get_num() const { return num; }

  /** get i th coefficient */
  const F Get_cs_i(unsigned int i) {return cs[i]; }
  const F Get_cs_i(unsigned int i) const {return cs[i]; }

  /** get value at r */
  const F At_r(F r) const
  {
    F acc = value_zero<F>();
    for(unsigned int i = 0; i < num; i++)
      {
	acc += cs[i] * aos[i]->At_r(r);
      }
    return acc;
  }
  

// *** setter

  /** set i th coefficient */
  void Set_cs_i(unsigned int i, F c) {
    cs[i] = c;
  }

  /** set coefficient array */
  void Set_cs(F* _cs) {
    for(unsigned int i = 0; i < num; i++)
      cs[i] = _cs[i];
  }

  /** set i th ao */
  void Set_aos_i(unsigned int i, AO* ao) {
    aos[i] = ao;
  }
  
// ** algebra

  void ScalarProduct(F a)
  {
    for(unsigned int i = 0; i < num; i++)
      {
	cs[i] *= a;
      }
  }

  
// ** Factory

public:
  
  static LCAO* New_zero()
  {
    LCAO<AO, F>* lcao = new LCAO<AO, F>(0);
    return lcao;
  }

  
// ** IO
// *** display

  /** display internal state of this object */
  void Display() {

    std::cout << std::endl;
    std::cout << "=======================" << std::endl;
    std::cout << "===LCAO.DIsplay========" << std::endl;

    for(unsigned int i = 0; i < num; i++)
      {
	std::cout << "index: " << i << std::endl;
	std::cout << "coef: " << cs[i] << std::endl;
	aos[i]->Display();
      }

    std::cout << "=======================" << std::endl;
  }
  
  
// *  End        
// ** end class
  
};

// ** linear combination of normalized GTO
// *** different principle number
template<class F>
LCAO<GTO<F>, F >* New_LC_NormalizedGTOs(unsigned int num, F* cs, F* zs, int* pns) {

  LCAO<GTO<F>, F>* f = new LCAO<GTO<F>, F>(num);

  for(unsigned int i = 0; i < num; i++) {

    GTO<F>* gto = GTO<F>::New_Normalized(pns[i], zs[i]);
    f->Set_aos_i(i, gto);
    f->Set_cs_i(i, cs[i]);
      
  }
  return(f);
}

// *** same principle number

template<class F>
LCAO<GTO<F>, F >* New_LC_NormalizedGTOs(int pn, unsigned int num, F* cs, F* zs) {

  int* pns = new int[num];
  for(unsigned int i = 0; i < num; i++)
    pns[i]= pn;
  
  LCAO<GTO<F>, F >* f = New_LC_NormalizedGTOs(num,cs,zs,pns);

  delete[] pns;
  return f;
}

// ** mono
// *** general

template<class AO, class F>
LCAO<AO, F>* New_LC_Mono(const AO& ao) {

  LCAO<AO, F>* lcao = new LCAO<AO, F>(1);
  AO* p_ao = new AO(ao);
  F one = value_one<F>();

  lcao->Set_aos_i(0, p_ao);
  lcao->Set_cs_i(0,  one);

  return lcao;
}


// *** STO

template<class F>
LCAO<STO<F>, F>* New_LC_MonoSTO(int pn, F z) {

  STO<F>* prim_sto = STO<F>::New_Normalized(pn, z);
  LCAO<STO<F>, F>* sto = New_LC_Mono<STO<F>, F>(*prim_sto);

  delete prim_sto;

  return sto;
}


// ** cout

template<class AO, class F>
ostream& operator<<(ostream& os, const LCAO<AO,F>& lcao)
{
  unsigned int num = lcao.Get_num();

  // no term
  if(num == 0)
    {
      os << "0";
      return os;
    }

  // exit terms
  for(unsigned int i = 0; i < num; i++)
    {
      os << lcao.Get_cs_i(i) << lcao.Get_aos_i(i);
      if(i != num-1)
	os << " + ";
    }
  return os;
  
}

// ** write


/*
template<class AO, class F, class RF>
WriteAO(const LCAO<AO, F>& ao, RF r0, RF r1, RF dr, const std::string name& name, std::ostream& os)
{
  std::string msg = "";
  msg += "WriteAO::unsupported type";
  throw msg;
}

template<class AO>
WriteAO<AO, dd_complex, dd_real>
(const vector<pair<LCAO<AO, dd_complex>*, string> > lcao, 
 dd_real r0, dd_real r1, dd_real dr, 
 const std::string name, std::ostream& os)
{

  // write header
  os << "# r, Re[" << name << "], Im[" << name + "]\n";

  
  for(dd_real r = r0; r < r1; r += dr)
    {
      dd_complex z = lcao.At_r(r);
      os << r << " " << z.real() << " " << z.imag();
      os << std::endl;

      }
}

*/



// ** endif

#endif



