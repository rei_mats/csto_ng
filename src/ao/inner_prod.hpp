// *  Begin                          
// ** comment

/**
 * @file inner_prod.hpp
 * @brief inner product of STOs and GTOs.
 * @date 2014/1/6 new
 * @author Rei matsuzaki
 */

// ** ifndef

#ifndef INNER_PROD_HPP
#define INNER_PROD_HPP

// ** include

#include <qd/dd_real.h>
#include <dd_complex.h>


// *  functions, pritmitive STO/GTO  
// ** STO, STO
// *** c-product

/** (STO, STO), c-product of STO and STO */
template<class F>
F CIP(const STO<F>& a, const STO<F>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    F zero = value_zero<F>();
    return zero;
  }

  F res = a.Get_n() * b.Get_n() *
    STO_int(a.Get_pn() + b.Get_pn(), a.Get_z() + b.Get_z());
  return res;
}

// *** h-product, double, dd_real

/** <STO, STO>, c-product of STO and STO */
double HIP(const STO<double>& a, const STO<double>& b) {
  return CIP(a, b); }
dd_real HIP(const STO<dd_real>& a, const STO<dd_real>& b) {
  return CIP(a, b); }

// *** h-product, dd_complex

dd_complex HIP(const STO<dd_complex>& a, const STO<dd_complex>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    dd_complex zero;
    zero.real() = "0"; zero.imag() = "0";
    return zero;
  }

  dd_complex an = a.Get_n();
  dd_complex res = conj(an) * b.Get_n() *
    STO_int(a.Get_pn() + b.Get_pn(),
	    conj(a.Get_z()) + b.Get_z());
  return res;
}


// ** GTO, GTO
// *** c-product

/** (GTO, GTO), c-product of GTO and GTO */
template<class F>
F CIP(const GTO<F>& a, const GTO<F>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    F zero = value_zero<F>();
    return zero;
  }

  F res = a.Get_n() * b.Get_n() *
    GTO_int(a.Get_pn() + b.Get_pn(), a.Get_z() + b.Get_z());
  return res;

}


// *** h-product, double, dd_real

/** <GTO, GTO>, c-product of GTO and GTO */
double HIP(const GTO<double>& a, const GTO<double>& b) {
  return CIP(a, b); }
dd_real HIP(const GTO<dd_real>& a, const GTO<dd_real>& b) {
  return CIP(a, b); }

// *** h-product, dd_complex

dd_complex HIP(const GTO<dd_complex>& a, const GTO<dd_complex>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    dd_complex zero; 
    zero.real() = "0"; zero.imag() = "0";
    return zero;
  }


  dd_complex an = a.Get_n();
  dd_complex res = conj(an) * b.Get_n() *
    GTO_int(a.Get_pn() + b.Get_pn(),
	    conj(a.Get_z()) + b.Get_z());
  return res;
  
}



// ** STO, GTO
// *** c-product

template<class F>
F CIP(const STO<F>& a, const GTO<F>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    F zero = value_zero<F>();
    return zero;
  }

  F res = a.Get_n() * b.Get_n() *
    STO_GTO_int(a.Get_pn() + b.Get_pn(),
		a.Get_z(), b.Get_z());
  return res;
}

// *** h-product for double and dd_real

double HIP(const STO<double>& a, const GTO<double>& b) {
  return CIP(a,b);}
dd_real HIP(const STO<dd_real>& a, const GTO<dd_real>& b) {
  return CIP(a, b); }


// *** h-product for dd_complex

dd_complex HIP(const STO<dd_complex>& a, const GTO<dd_complex>& b) {

  if(a.Is_zero() || b.Is_zero()) {
    dd_complex zero; 
    zero.real() = "0"; zero.imag() = "0";
    return zero;
  }

  dd_complex res = conj(a.Get_n()) * b.Get_n() *
    STO_GTO_int(a.Get_pn() + b.Get_pn(),
		conj(a.Get_z()), b.Get_z());
  return res;
}

// ** GTO, STO

template<class F>
F CIP(const GTO<F>& gto, const STO<F>& sto) {
  return CIP(sto, gto); }

double HIP(const GTO<double>& a, const STO<double>& b) {
  return HIP(b,a);}
dd_real HIP(const GTO<dd_real>& a, const STO<dd_real>& b) {
  return HIP(a, b); }
dd_complex HIP(const GTO<dd_complex>& gto, const STO<dd_complex>& sto){
  return conj(HIP(sto, gto));
}

 
// *  functions, linear combination  
// ** lcao
// *** c-product

template<class AO1, class AO2, class F>
F CIP(const LCAO<AO1, F>& a, const LCAO<AO2, F>& b) {

  unsigned int num_a = a.Get_num();
  unsigned int num_b = b.Get_num();
  F zero = value_zero<F>();

  if(num_a == 0 || num_b ==0)
    return zero;

  F acc = value_zero<F>();
  for(unsigned int i = 0; i < num_a; i++)
    for(unsigned int j = 0; j < num_b; j++) {

      acc += a.Get_cs_i(i) * b.Get_cs_i(j) *
	CIP(a.Get_aos_i(i), b.Get_aos_i(j));

    }

  return acc;
}

// *** h-product

template<class AO1, class AO2>
double HIP(const LCAO<AO1, double>& a,
	   const LCAO<AO2, double>& b) {
  return CIP(a, b);
}

template<class AO1, class AO2>
dd_real HIP(const LCAO<AO1, dd_real>& a,
	   const LCAO<AO2, dd_real>& b) {
  return CIP(a, b);
}

template<class AO1, class AO2>
dd_complex HIP(const LCAO<AO1, dd_complex>& a,
	       const LCAO<AO2, dd_complex>& b) {
  unsigned int num_a = a.Get_num();
  unsigned int num_b = b.Get_num();
  dd_complex zero(0,0);

  if(num_a == 0 || num_b ==0)
    return zero;

  dd_complex acc = zero;
  for(unsigned int i = 0; i < num_a; i++)
    for(unsigned int j = 0; j < num_b; j++) 
      acc += conj(a.Get_cs_i(i)) * b.Get_cs_i(j) *
	HIP(a.Get_aos_i(i), b.Get_aos_i(j));
  
  return acc;
}

// ** <lcao, AO>
// *** dd_real

template<class AO1, class AO2>
dd_real HIP(const LCAO<AO1, dd_real>& a,
	    const AO2& b) {

  unsigned int num_a = a.Get_num();
  dd_real zero(0);

  if(num_a == 0)
    return zero;

  dd_real acc = zero;
  for(unsigned int i = 0; i < num_a; i++)
    acc += a.Get_cs_i(i) * HIP(a.Get_aos_i(i), b);
  
  return acc;

}

template<class AO1, class AO2>
dd_real HIP(const AO2& b, const LCAO<AO1, dd_real>& a){
  return HIP(a, b);
}

// *** dd_complex

template<class AO1, class AO2>
dd_complex HIP(const LCAO<AO1, dd_complex>& a,
	       const AO2& b) {
  unsigned int num_a = a.Get_num();
  dd_complex zero(0,0);

  if(num_a == 0)
    return zero;

  dd_complex acc = zero;
  for(unsigned int i = 0; i < num_a; i++)
    acc += conj(a.Get_cs_i(i)) * HIP(a.Get_aos_i(i), b);
  
  return acc;
}
template<class AO1, class AO2>
dd_complex HIP(const AO2& b, const LCAO<AO1, dd_complex>& a){
  return conj(HIP(a, b));
}


// *  norm                           
// ** norm
template<class L2, class F>
F CNorm2(const L2& a)
{
  return CIP(a, a);
}

template<class L2, class F>
F HNorm2(const L2& a)
{
  return HIP(a, a);
}

// ** distance

template<class L2, class F>
F CDistance2(const L2& a, const L2& b)
{
  return CIP(a, a) + CIP(b, b) - 2 * CIP(a, b);
}

template<class A, class B>
dd_real HDistance2(const LCAO<A, dd_real>& a, 
		   const LCAO<B, dd_real>& b)
{
  return HIP(a,a) + HIP(b,b) - 2 * HIP(a,b);
}

template<class A, class B>
dd_real HDistance2(const LCAO<A, dd_complex>& a, 
		   const LCAO<B, dd_complex>& b)
{
  return (HIP(a,a) + HIP(b,b) - 2 * HIP(a,b).real()).real();
}

/*
template<class L2>
double HDistance2<L2, double>(L2 a, L2 b)
{
  return CDistance(a, b);
}

template<class L2>
dd_real HDistance2<L2, dd_real>(L2 a, L2 b)
{
  return CDistance(a, b);
}

template<class L2>
dd_real HDistance2<L2, dd_complex>(L2 a, L2 b)
{
  return HIP(a,a) + HIP(b,b) -2*HIP(a,b).real();
}
*/

// *  Functions, old                 
// ** STO, STO
// *** c-product

/** (STO, STO), c-product of STO and STO */
template<class F>
F CIP_sto_sto(const STO<F>& a, const STO<F>& b) {

  F acc = value_zero<F>();
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc += a.Get_cs_i(i) * b.Get_cs_i(j) *
	a.Get_ns_i(i) * b.Get_cs_i(j) *
	STO_int(a.Get_pns_i(i) + b.Get_pns_i(j),
		a.Get_zs_i(i) + b.Get_zs_i(j));
  return(acc);
}


// *** Hermitian default

/** <STO, STO>, Hermitian-product of STO and STO */
template<class F>
F HIP_sto_sto(const STO<F>& a, const STO<F>& b) {
  return CIP_sto_sto(a, b);
}

// *** Hermitian dd_complex

/*
template<>
dd_complex HIP_sto_sto(const STO<dd_complex>& a, 
		       const STO<dd_complex>& b) {
  dd_complex acc;
  acc.real() = 0; acc.imag() = 0;
  
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc +=a.Get_cs_i(i).conjugate() * b.Get_cs_i(j) *
	a.Get_ns_i(i).conjugate() * b.Get_cs_i(j) *
	STO_int(a.Get_pns_i(i).conjuagate() + b.Get_pns_i(j),
		a.Get_zs_i(i).conjugate() + b.Get_zs_i(j));

  return(acc);
}
*/

// ** GTO, GTO
// *** c-product

/** (GTO, GTO) : c-product of GTO and GTO */
template<class F>
F CIP_gto_gto(const GTO<F>& a, const GTO<F>& b) {

  F acc = value_zero<F>();
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc += a.Get_cs_i(i) * b.Get_cs_i(j) *
	a.Get_ns_i(i) * b.Get_cs_i(j) *
	GTO_int(a.Get_pns_i(i) + b.Get_pns_i(j),
		a.Get_zs_i(i) + b.Get_zs_i(j));
  return(acc);
}


// *** Hermitian product (default)

/** <GTO, GTO>, Hermitian-product of GTO and GTO */
template<class F>
F HIP_gto_gto(const STO<F>& a, const STO<F>& b) {
  return CIP_gto_gto(a, b);
}

// *** Hermitian product (dd_complex)
/*
template<>
dd_complex HIP_gto_gto(const GTO<dd_complex>& a, 
		       const GTO<dd_complex>& b) {
  dd_complex acc = 0;
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc +=a.Get_cs_i(i).conjugate() * b.Get_cs_i(j) *
	a.Get_ns_i(i).conjugate() * b.Get_cs_i(j) *
	GTO_int(a.Get_pns_i(i).conjuagate() + b.Get_pns_i(j),
		a.Get_zs_i(i).conjugate() + b.Get_zs_i(j));

  return(acc);
}
*/




// ** STO, GTO
// *** c-product

/** (STO, GTO) : c-product of STO and GTO */
template<class F>
F CIP_sto_gto(const STO<F>& a, const GTO<F>& b) {
  F acc = value_zero<F>();
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc += a.Get_cs_i(i) * b.Get_cs_i(j) *
	a.Get_ns_i(i) * b.Get_cs_i(j) *
	STO_GTO_int(a.Get_pns_i(i) + b.Get_pns_i(j),
		    a.Get_zs_i(i), b.Get_zs_i(j));
  return(acc);  
}

// *** Hermitian product (default)

/** <STO, GTO>, Hermitian-product of STO and GTO */
/*
template<class F>
F HIP_sto_gto(const STO<F>& a, const GTO<F>& b) {
  return CIP_sto_gto(a, b);
}
*/

// *** Hermitian product (dd_complex)


/*
template<>
dd_complex HIP_sto_gto(const STO<dd_complex>& a, 
		       const GTO<dd_complex>& b) {
  dd_complex acc = 0;
  for(unsigned int i = 0; i < a.Get_num(); i++)
    for(unsigned int j = 0; j < b.Get_num(); j++) 
      acc +=a.Get_cs_i(i).conjugate() * b.Get_cs_i(j) *
	a.Get_ns_i(i).conjugate() * b.Get_cs_i(j) *
	STO_GTO_int(a.Get_pns_i(i).conjuagate() + b.Get_pns_i(j),
		    a.Get_zs_i(i).conjugate(), b.Get_zs_i(j));

  return(acc);
}
*/


// ** GTO, STO
// *** c-product

template<class F>
F CIP_gto_sto(const GTO<F>& a, const STO<F>& b) {
  return (CIP_sto_gto(b, a));
}

// *** hermitian product (default)

template<class F>
F HIP_gto_sto(const GTO<F>& a, const STO<F>& b) {
  return HIP_sto_gto(b, a);
}

// *** Hermitian product (complex)
/*
template<>
dd_complex HIP_gto_sto(const GTO<dd_complex>& a,
		       const STO<dd_complex>& b) {

		       return conj(HIP_sto_gto(b, a));
}
*/



// *  End               

#endif
