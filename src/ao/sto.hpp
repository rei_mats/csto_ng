// *  Begin       
// ** comment

/**
 * @file sto.hpp
 * @brief Normalized primitive Radial Slater type orbital basis.
 * @date 2014/1/7 new
 * @author Rei Matsuzaki
 */

// ** ifndef

#ifndef STO_HPP
#define STO_HPP


// ** include

#include <src/math/sto_gto_int.hpp>


// ** start class

/**
 * normalized primitive radial slater type orbital basis.
 *
 */
template<class F>
class STO {

// *  Member      
// ** field

private:  
  F           z;     /** orbital exponent*/
  int         pn;    /** principle number  */
  F           n;     /** normalized constant */
  bool        is_zero; /** tur => this object is zero */


// ** constructor/destructor
// *** constructor
  
public:
  /**
   * Constructor
   */
  STO(bool _is_zero) :
    is_zero(_is_zero) {}


// *** Destructor
  
  /** Destructor */
  ~STO() {
  }
  


// ** accessor
// *** getter
  
  /** get zeta */
  const F Get_z() { return z; }
  const F Get_z() const { return z; }

  /** get principle number  */
  const int Get_pn() { return pn; }
  const int Get_pn() const { return pn; }

  /** get normalization constant */
  const F Get_n() { return n; }
  const F Get_n() const { return n; }

  /** get is zero or not */
  const bool Is_zero() { return is_zero; }
  const bool Is_zero() const { return is_zero; }

  /** get function value at r */
  const F At_r(F r)
  {
    return n * pow(r, pn) * exp(-z*r);
  }
  const F At_r(F r) const
  {
    return n * pow(r, pn) * exp(-z*r);
  }
  
// *** setter
  
public:  
  //** set z without normalization*/  
  void Set_z_without_normalization(F _zs) {
    z = _zs;
    }

  //** set pns without normalization*/
  void Set_pn_without_normalization(int _pn) {
    pn = _pn;
  }
  
  //** set zs with normalization*/
  void Set_z(F _z) {
    z = _z;
    Normalize();
  }

  //** set pn with normalization*/
  void Set_pn(int _pn) {
    pn = _pn;
    Normalize();
  }
  
// ** action
  
   //** compute and store normalization constant */
  void Normalize() { 
    F norm2 = STO_int(2 * pn, 2 * z);
    n = 1/sqrt(norm2);
  }
  
// ** IO
// *** display

  /** display internal state of this object */
  void Display() {

    std::cout << std::endl;
    std::cout << "==========================" << std::endl;
    std::cout << "===STO.Display============" << std::endl;
    std::cout << std::endl;
    std::cout << "z, c, pn, n " << std::endl;
    std::cout << z << " ";
    std::cout << pn << " " << n << std::endl;
    std::cout << std::endl;
    std::cout << "==========================" << std::endl;

  }
  
// ** Factory (static)
// *** Normalized
  
  /** compute normalized STO */
  static STO* New_Normalized(int _pn, F _z) {

    STO* sto = new STO(false);
    //    std::cout << "1" << std::endl;
    sto->Set_z_without_normalization(_z);
    //    std::cout << "1" << std::endl;
    sto->Set_pn_without_normalization(_pn);
    //    std::cout << "1" << std::endl;
    sto->Normalize();
    //    std::cout << "1" << std::endl;
    return sto;
  }

// *** zero

  /** zero vector */
  static STO* New_zero() {
    STO* sto = new STO(true);
    return sto;
  }


// *  End         

};
#endif




