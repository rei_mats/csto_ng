#ifndef OLD_HPP
#define OLD_HPP

// *  Class          
// ** begin

/**
 *   RF means Real Field and I assumed RF=double or dd_real.
 */
template<class RF>
class LinearSearch {


// ** field member

private:

  // ----- calculation setting -----
  unsigned int max_iter_;   /** maximum iteration number */
  RF (*f_)(RF*, void*);     /** target function */
  void* ctx_;               /** component for func */
  RF* xs_;                   /** optimization point */
  RF* d_;                    /** update direction */
  unsigned int num_;        /** number of variables(xs) */
  //  RF (*g_)(RF, void*);      /** target function g(a,ctx) := f(x+ad) */
  

  // ----- Armijo's rule -----
  double armijo_zeta_;      /** parameter used in Armijo's rule */
  RF g0_;                   /** g(alpha=0) = f(xs) */
  RF zeta_df_d_;            /** zeta Df d  */

  // ----- calculation status ------
  RF alpha_;                /** current step size */
  unsigned int iter_;       /** iteration count */

  // ----- work space ------
  RF* work;
  
// ** constructor

public:  
  LinearSearch(unsigned int _max_iter, 
	       RF(*_f)(RF*, void*),
	       void* _ctx, 
	       RF* _xs,
	       RF* _d,
	       unsigned int _num,	       
	       double _armijo_zeta, 
	       RF* _df) :
    max_iter_(_max_iter),
    f_(_f),
    ctx_(_ctx),
    xs_(_xs),
    d_(_d),
    num_(_num),
    armijo_zeta_(_armijo_zeta),

    iter_(0) {

    RF zero = value_zero<RF>();
    alpha_ = zero;
    g0_ = f_(xs_, ctx_);
    
    zeta_df_d_ = zero;
    for(unsigned int i = 0; i < num_; i++) 
      zeta_df_d_ += _df[i] * _d[i];
    zeta_df_d_ *= armijo_zeta_;
    
    work = new RF[num_];
    
  }
// ** destructor
  
  ~LinearSearch() {

    delete work;
  }
  

// ** Util

private:

  RF calc_g_of_alpha() {

    
    for(unsigned int i = 0; i < num_; i++) 
      work[i] = xs_[i] + d_[i] * alpha_;

    return f_(work, ctx_);
    
  }
  
// ** Accessor
// *** getter
  
public:
  const RF GetAlpha() {return alpha_; }
  const RF GetAlpha() const {return alpha_; }

  const unsigned int GetIter() {return iter_;}
  const unsigned int GetIter() const {return iter_;}
  
  
// ** Update
// *** TwoPiece

  void UpdateTwoPiece() {
    alpha_ = alpha_ / 2;
  }


// ** Armijo rule
// *** right hand equation
public:
  
  RF ArmijoRightHand() {

    return calc_g_of_alpha();
    
  }

// *** left hand equation
public:
  
  RF ArmijoLeftHand() {

    return g0_ + alpha_ * zeta_df_d_;
    
  }
  
  
// *** satisfy

  bool SatisfyArmijoRule() {

    // right hand equation of Armijo's rule.
    RF lhe = calc_g_of_alpha();

    // left hand equation of Armijo's rule.
    RF rhe = ArmijoLeftHand();
    
    return lhe < rhe;
    
  }
  
  

// ** Status

public:
  
  bool ConvergenceQ() {
    return SatisfyArmijoRule() && iter_ < max_iter_;
  }
  
  bool DivergenceQ() {
    return iter_ >= max_iter_;
  }
  
  
// ** Compute
// *** simple

  void Compute(RF _a0) {

    alpha_ = _a0;
    
    while( true) {

      iter_++;
      
      UpdateTwoPiece();

      if(iter_ >= max_iter_)
	break;

      if(SatisfyArmijoRule())
	break;
    }
  }
  
  
// ** end
};

  

#endif
