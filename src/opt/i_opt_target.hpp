// *  Begin           

#ifndef I_OPT_TARGET_HPP
#define I_OPT_TARGET_HPP

// *  non-constrain   
// ** begin

template<class RF>
class IOptTarget
{

// ** field
  
protected:
  RF val_;
  RF* grad_;
  RF* hess_;
  unsigned int num_;


// ** constructor
  
public:
  IOptTarget() {}
  IOptTarget(unsigned int _num) {
    init(_num);
  }
  void init(unsigned int _num) {
    num_ = _num;
    grad_ = new RF[num_];
    hess_ = new RF[num_ * num_];
  }
  virtual ~IOptTarget() {
    delete[] grad_;
    delete[] hess_;
  }


// ** member
public:
  
  unsigned int GetNum() { return num_; }
  unsigned int GetNum() const { return num_; }
  
  RF  GetValue() { return val_;}
  RF  GetValue() const { return val_;}
  RF* GetGrad() { return grad_; }
  RF* GetGrad() const { return grad_; }
  RF* GetHess() { return hess_; }
  RF* GetHess() const { return hess_; }

  virtual RF GetXs_i(unsigned int) const = 0;
  virtual void CalcValue() =0;
  virtual void CalcGrad() =0;
  virtual void CalcHess() =0;
  virtual void SetUpdate(RF*)=0;

  virtual std::ostream& Display(std::ostream& os)
  {
    os << "Type: non_constraint optimization target";
    return os;
  }


  
// ** end
  
};

// *  With-constraint 
// ** begin

template<class RF>
class IOptTargetConstraint : public IOptTarget<RF>
{

// ** field
  
protected:
  RF lambda_;
  RF val_h_;
  RF* grad_h_;
  RF* hess_h_;
  

// ** constructor

public:
  IOptTargetConstraint(unsigned int _num, RF _lambda) :
    IOptTarget<RF>(_num)
  {
    lambda_ = _lambda;
    grad_h_ = new RF[_num];
    hess_h_ = new RF[_num * _num];
  }
  virtual ~IOptTargetConstraint() {
    delete[] grad_h_;
    delete[] hess_h_;
  }


// ** member
public:
  RF  GetValue_h() const {
    //    std::cout << val_h_ << std::endl;
    return val_h_; }
  RF* GetGrad_h() const { return grad_h_; }
  RF* GetHess_h() const {return hess_h_; }
  RF  GetLambda() const {return lambda_; }
  void SetUpdateLambda(RF dl) {
    lambda_ += dl;
  }
  void SetLambda(RF l) {
    lambda_ = l;
  }

  virtual std::ostream& Display(std::ostream& os)
  {
    os << "Type: constraint optimization target";
    return os;
  }
  

// ** end
  
};


#endif

