// *  Begin             

#ifndef HESS_HPP
#define HESS_HPP

// *  Interface         

template<class RF>
class ICalcHess
{
protected:
  RF* hess_;
  unsigned int num_;
  std::ofstream& ofs_;
  
public:
  ICalcHess(unsigned int _num, std::ofstream& _ofs) :
    ofs_(_ofs)
  {
    num_ = _num;
    hess_ = new RF[_num*_num];
  }
   

  virtual ~ICalcHess() 
  {
    delete[] hess_;
  }
  void WriteLog() {
   ofs_ << "Hess:" << std::endl;
    for(unsigned int i = 0; i < num_; i++)
      {
      for(unsigned int j = 0; j < num_; j++)
	{
	  ofs_ << hess_[i+ num_*j] << ", ";
	}
      ofs_ << std::endl;
      }    
  }
  virtual bool NeedTrueHess() = 0;
  virtual bool NeedTrueGrad() = 0;
  virtual void Init() = 0;
  virtual void UpdateBefore() = 0;
  virtual void UpdateAfter() = 0;
  RF* GetHess() { return hess_; }

};

// *  True Hessian      

template<class RF>
class CalcTrueHess : public ICalcHess<RF>
{
private:
  IOptTarget<RF>* target_;
public:
  CalcTrueHess(IOptTarget<RF>* _target, 
	       std::ofstream& _ofs) : 
    ICalcHess<RF>(_target->GetNum(), _ofs),
    target_(_target)
  {}
  ~CalcTrueHess() {}
  
  virtual bool NeedTrueGrad() { return true; }
  virtual bool NeedTrueHess() { return true; }
  virtual void Init()
  {
    UpdateAfter();
  }
  virtual void UpdateBefore()
  {
  }
  virtual void UpdateAfter()
  {

    RF* true_hess = target_->GetHess();
    for(unsigned int i = 0; i < this->num_ * this->num_; 
	i++)
      {
	this->hess_[i] = true_hess[i];
      }
  }

};

// *  Steepest Descent  

template<class RF>
class CalcHessSteepestDescent : public ICalcHess<RF>
{
public:
  CalcHessSteepestDescent(unsigned int _num, 
			  std::ofstream& _ofs) :
    ICalcHess<RF>(_num, _ofs)
  {}
  virtual bool NeedTrueGrad() { return false; }
  virtual bool NeedTrueHess() { return false; }
  virtual void Init()
  {
    RF one = value_one<RF>();
    RF zero = value_zero<RF>();
    
    for(unsigned int i = 0; i < this->num_; i++) 
      for(unsigned int j = 0; j < this->num_; j++)
      {
	if(i == j)
	  this->hess_[i + this->num_ * j] = one;
	else
	  this->hess_[i + this->num_ * j] = zero;
      }
  }
  virtual void UpdateBefore(){}
  virtual void UpdateAfter(){}

};

// *  BFGS              
// ** begin

template<class RF>
class CalcHessBFGS : public ICalcHess<RF>
{

// ** field

private:
  IOptTarget<RF>* target_;
  RF* xk;
  RF* xkp;
  RF* dfk;
  RF* dfkp;
  RF* sk;
  RF* yk;
  RF* Hs;

// ** constructor

public:
  CalcHessBFGS(IOptTarget<RF>* _target,
	       std::ofstream& _ofs) : 
    ICalcHess<RF>(_target->GetNum(), _ofs),
    target_(_target)
  {
    xk  = new RF[this->num_];
    xkp = new RF[this->num_];
    dfk = new RF[this->num_];
    dfkp = new RF[this->num_];
    sk = new RF[this->num_];
    yk = new RF[this->num_];
    Hs = new RF[this->num_];
  }

// ** destructor

public:
  ~CalcHessBFGS()
  {
    delete[] xk;
    delete[] xkp;
    delete[] dfk;
    delete[] dfkp;
    delete[] yk;
    delete[] sk;
    delete[] Hs;
  }


// ** need

public:
  virtual bool NeedTrueGrad() { return true; }
  virtual bool NeedTrueHess() { return false; }

  
// ** init
  
  virtual void Init()
  {
    for(unsigned int i = 0; i < this->num_; i++)
      {
	xk[i] = target_->GetXs_i(i);
	xkp[i] = xk[i];
	dfk[i] = target_->GetGrad()[i];
	dfkp[i] = dfkp[i];
      }
    
    RF one = value_one<RF>();
    RF zero = value_zero<RF>();

    for(unsigned int i = 0; i < this->num_; i++)
      for(unsigned int j = 0; j < this->num_; j++)
	{
	  if(i == j)
	    this->hess_[i + this->num_ * j] = one;
	  else
	    this->hess_[i + this->num_ * j] = zero;
	}    
  }

// ** before
  virtual void UpdateBefore(){}


// ** after
  
  virtual void UpdateAfter()
  {
    for(unsigned int i = 0; i < this->num_; i++)
      {
	xk[i] = xkp[i];
	dfk[i] = dfkp[i];
	xkp[i] = target_->GetXs_i(i);
	dfkp[i]= target_->GetGrad()[i];
	sk[i] = xkp[i] - xk[i];
	yk[i] = dfkp[i] - dfk[i];
      }
    
    for(unsigned int i = 0; i < this->num_; i++) {
      RF acc = 0;
      for(unsigned int j = 0; j < this->num_; j++) 
	acc += this->hess_[i + this->num_ * j] * sk[j];
      Hs[i] = acc;
    }

    RF yksk =0;
    RF sHs = 0;
    for(unsigned int i = 0; i < this->num_; i++)
      {
	yksk += yk[i] * sk[i];
	sHs += Hs[i] * sk[i];
      }

    for(unsigned int i = 0; i < this->num_; i++)
      for(unsigned int j = 0; j < this->num_; j++)
	{
	  this->hess_[i+this->num_*j] += -Hs[i]*Hs[j]/sHs;
	  this->hess_[i+this->num_*j] += yk[i]*yk[j]/yksk;
	}
  }
  
  
// ** end

};


// *  modified BFGS     
// ** begin

template<class RF>
class CalcHessModBFGS :public ICalcHess<RF>
{

// ** field

private:

  IOptTargetConstraint<RF>* target_;
  RF w_;     // Powell's parameter used in Modified BFGS
  RF b0_;    // initial hessian matrix element.

  RF* xk;
  RF* xkp;
  RF* dfk;
  RF* dfkp;

  RF* sk;
  RF* yk;
  RF* Bksk;
  RF* zk;
  
  
// ** constructors
// *** constructor

public:  
  CalcHessModBFGS(IOptTargetConstraint<RF>* _target,
		  RF _w, RF _b0,
		  std::ofstream& _ofs) :
    ICalcHess<RF>(_target->GetNum(), _ofs),
    target_(_target),
    b0_(_b0),
    w_(_w)
  {
    xk   = new RF[this->num_];
    xkp  = new RF[this->num_];
    dfk  = new RF[this->num_];
    dfkp = new RF[this->num_];
    sk   = new RF[this->num_];
    yk   = new RF[this->num_];
    Bksk = new RF[this->num_];
    zk   = new RF[this->num_];
  }
  

// *** destructor

public:
  ~CalcHessModBFGS()
  {
    delete[] xk;
    delete[] xkp;
    delete[] dfk;
    delete[] dfkp;
    delete[] yk;
    delete[] sk;
    delete[] Bksk;
    delete[] zk;
  }

// ** need

public:
  virtual bool NeedTrueGrad() { return true; }
  virtual bool NeedTrueHess() { return false; }
  
  
// ** init
  
public:
  virtual void Init()
  {
    
    for(unsigned int i = 0; i < this->num_; i++)
      {
	xk[i] = target_->GetXs_i(i);
	xkp[i] = xk[i];
	dfk[i] = target_->GetGrad()[i] + 
	  target_->GetLambda() * target_->GetGrad_h()[i];
	dfk[i] = dfkp[i];
	sk[i] = xkp[i]  - xk[i];
	yk[i] = dfkp[i] - dfkp[i];
      }    

    RF one = value_one<RF>();
    RF zero = value_zero<RF>();

    for(unsigned int i = 0; i < this->num_; i++)
      for(unsigned int j = 0; j < this->num_; j++)
	{
	  if(i == j)
	    this->hess_[i + this->num_ * j] = one * b0_;
	  else
	    this->hess_[i + this->num_ * j] = zero;
	}        
  }
  

// ** update
  
  virtual void UpdateBefore() {}

  virtual void UpdateAfter()
  {
    unsigned int n = this->num_;

    for(unsigned int i = 0; i < this->num_; i++)
      {
	xk[i] = xkp[i];
	xkp[i] = target_->GetXs_i(i);
	dfk[i] = dfkp[i];
	dfkp[i] = target_->GetGrad()[i] + 
	  target_->GetLambda() * target_->GetGrad_h()[i];
	sk[i] = xkp[i]  - xk[i];
	yk[i] = dfkp[i] - dfkp[i];
      }        

    for(unsigned int i = 0; i < n; i++)
      {
	RF acc = 0;
	for(unsigned int j = 0; j < n; j++)
	  acc += this->hess_[i+n*j] * sk[j];
	Bksk[i] = acc;
      }

    RF ys = dot_product(n, yk, sk);
    RF sBs  = dot_product(n, Bksk, sk);

    
    if(ys < w_ * sBs) 
      {
	RF phi = (1 - w_) * sBs / (sBs -ys);
	for(unsigned int i = 0; i < n; i++)
	  zk[i] = phi * yk[i] + (1-phi) * Bksk[i];
      }
    else
      {
	for(unsigned int i = 0; i < n; i++)
	  zk[i] = yk[i];
      }
    RF zs = dot_product(n, zk, sk);
    for(unsigned int i = 0; i < this->num_; i++)
      for(unsigned int j = 0; j < this->num_; j++)
	{
	  this->hess_[i+n*j] += -Bksk[i] * Bksk[j] / sBs;
	  this->hess_[i+n*j] += zk[i]*zk[j] / zs;
	}
  }




// ** end
  
};

// *  End               

#endif
