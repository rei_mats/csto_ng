// *  Begin    
// ** ifndef

#ifndef NEWTON_HPP
#define NEWTON_HPP

// *  Class    
// ** begin

template<class RF>
class Newton 
{

// ** field
  
private:

  // ----- calculation setting ------
  unsigned int max_iter_; /** maximum iteration number */
  double eps_; /** small value for checking convergence */
  unsigned int num_;     /** number of variable */
  IOptTarget<RF>* target_;   /** calculation target */
  ILinearSearch<RF>* linear_search_; /** linear searcher*/
  ICalcHess<RF>* calc_hess_;

  // ----- calculation state ------
  unsigned int iter_;

  // ----- log ------
  std::ofstream& ofs_;

  // ----- work space -----
  mpackint *ipiv_;
  RF* grad_or_update_;

// ** constructor
// *** constructor
  
public:
  Newton(unsigned int _max_iter,
	 double _eps,
	 unsigned int _num,
	 IOptTarget<RF>* _target,
	 ILinearSearch<RF>* _linear_search,
	 ICalcHess<RF>* _calc_hess,
	 std::ofstream& _ofs):
    max_iter_(_max_iter),
    eps_(_eps),
    num_(_num),
    target_(_target),
    linear_search_(_linear_search),
    calc_hess_(_calc_hess),
    iter_(0),
    ofs_(_ofs){
    
    ipiv_ =     new mpackint[_num];
    grad_or_update_ = new RF[_num];
  }
  
// *** destructor

public:
  ~Newton() {
    delete[] ipiv_;
    delete[] grad_or_update_;
  }
  
    
// ** Update
// *** write log
private:
  void writeLog() {
    
    ofs_ << "Newton:iter: " << iter_ << std::endl;
    ofs_ << "Newton:x: ";
    for(unsigned int i = 0; i < num_; i++)
      ofs_ << target_->GetXs_i(i) << " ";
    ofs_ << std::endl;
    ofs_ << "Newton:f: ";
    ofs_ << target_->GetValue();
    ofs_ << std::endl;    
  }

// *** update
  
public:
  bool Update()
  {
    mpackint info;
    
    // ------- step iteration -------------
    iter_++;
    writeLog();
    if(iter_ > max_iter_)
      return false;

    // ------- gradient -------------------    
    for(unsigned int i = 0; i < num_; i++)
      grad_or_update_[i] = target_->GetGrad()[i];
    RF max_grad = max_abs_value(num_, grad_or_update_);
    if(max_grad < eps_)
      return false;

    // ------ solve newton equation ------
    RF* hess_ = calc_hess_->GetHess();
    Rgesv(num_, 1, hess_,
	  num_, ipiv_, grad_or_update_, 
	  num_, &info);
    for(unsigned int i = 0; i < num_; i++)
      grad_or_update_[i] *= -1;

    // ------ linear search --------------
    linear_search_->Compute(grad_or_update_);
    for(unsigned int i = 0; i < num_; i++)
      grad_or_update_[i] *= linear_search_->GetStep();


    // ------ update --------------------      
    target_->SetUpdate(grad_or_update_);

    // ------ calculate next value ------
    if(calc_hess_->NeedTrueHess())
      target_->CalcHess();
    else
      target_->CalcGrad();
    calc_hess_->UpdateAfter();
    
    return true;
  }


// ** compute

public:
  void Compute()
  {
    if(calc_hess_->NeedTrueHess())
      target_->CalcHess();
    else
      target_->CalcGrad();
    
    calc_hess_->Init();
    
    while(Update());
  }

  
// ** State

public:
  bool IsConvergence(){
    return iter_ != 0 && iter_ < max_iter_;
  }

public:
  bool IsDivergence() {
    return iter_ > max_iter_;
  }

// ** end
};

// *  End      
#endif
