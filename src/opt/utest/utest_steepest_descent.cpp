#include <src/opt/steepest_descent.hpp>
#include <gtest/gtest.h>

dd_real hess_x2(dd_real* x) {return 2; }
dd_real grad_x2(dd_real* x) {return 2 * x[0]; }
dd_real func_x2(dd_real* x, void* k) {
  return x[0]*x[0]; }
void calc_grad_x2(dd_real* x, dd_real* y, void* ctx) {
  y[0] = 2 * x[0];
}

TEST(Utils, StepSize) {

  double y = 2.0;
  double x = CalcStepSize<E_Constant>(y);

  EXPECT_DOUBLE_EQ(x, y);

}

TEST(SD, first) {

  dd_real alpha = 3;
  SteepestDescent<dd_real, E_Constant> sd
    (/* max iter = */ 10,
     /* f =        */ func_x2,
     /* g =        */ calc_grad_x2,
     /* ctx =      */ NULL,
     /* num =      */ 1,
     /* alpha =    */ alpha);
}





