#include <iostream>
#include <fstream>

#include <gtest/gtest.h>
#include <qd/dd_real.h>
#include <mpack/mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_qd.hpp>
#include <src/utils/support_mpack.hpp>
#include <src/utils/array.hpp>
#include <src/opt/i_opt_target.hpp>
#include <src/opt/i_opt_for_test.hpp>
#include <src/opt/linear_search.hpp>
#include <src/opt/hess.hpp>
#include <src/opt/sqp.hpp>

TEST(SQP, construct) {

  std::ofstream ofs("log/sqp_newton.log");

  OptTargetExample1* target = new OptTargetExample1(2, 3);
  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Constant<dd_real>(1, ofs);
  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(target, ofs);
  
  SequentialQudraticProgramming<dd_real>* solver =
    new SequentialQudraticProgramming<dd_real>
      (20, 0.000001, target, l_search, calc_hess, ofs);

  solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());
  EXPECT_FALSE(solver->IsDivergence());

  delete target;
  delete l_search;
  delete calc_hess;
  delete solver;

}

