#include <iostream>
#include <fstream>
#include <gtest/gtest.h>

#include <qd/dd_real.h>
#include <mpack/dd_complex.h>

#include <src/utils/support_qd.hpp>

#include <src/utils/array.hpp>
#include <src/opt/i_opt_target.hpp>
#include <src/opt/i_opt_for_test.hpp>
#include <gtest/gtest.h>

TEST(OptTarget, Construct) {

  //  IOptTarget<dd_real>* target = new OptTargetExample(1,2);
  OptTargetExample target(2, 3);

  target.CalcValue();
  
  EXPECT_DOUBLE_EQ(
		   target.GetValue().x[0],
		   60.0);

  //  delete target;

}
TEST(OptTarget, update) {
  OptTargetExample target(2.2, 3.3);
  dd_real d[] = {"1", "2"};
 Test_OptTarget::SetUpdate<dd_real>(&target, d);
}
TEST(OptTarget, Grad) {

  OptTargetExample target(2.2, 3.3);

  dd_real h   = "0.00001";
  dd_real eps = "0.0001";
  Test_OptTarget::Grad_finite_dif(&target, h, eps);
  Test_OptTarget::Hess_finite_dif(&target, h, eps);
				  
  
}
