#include <src/opt/linear_search.hpp>
#include <gtest/gtest.h>

// compute 1st and 2nd derivative of function x^2
dd_real hess_x2(dd_real* x) {return 2; }
dd_real grad_x2(dd_real* x) {return 2 * x[0]; }
dd_real func_x2(dd_real* x, void* k) {
  return x[0]*x[0]; }


TEST(OneVariable, ConvergenceCalculation) {
  
  dd_real a0 = 1;
  dd_real x0 = 2;
  dd_real df_x0 = grad_x2(&x0);
  dd_real direct = -grad_x2(&x0);
  LinearSearch<dd_real> search
    (/* max iter = */ 20,
     /* f        = */ func_x2,
     /* ctx      = */ NULL,
     /* xs       = */ &x0,
     /* d        = */ &direct,
     /* num      = */ 1,
     /* zeta     = */ 0.5,
     /* df       = */ &df_x0);
  
  search.Compute(a0);
  
  EXPECT_TRUE(search.ConvergenceQ());
  EXPECT_DOUBLE_EQ(search.GetAlpha().x[0], 0.25);
  EXPECT_EQ(search.GetIter(), 2);

  dd_real lhe = search.ArmijoRightHand();
  dd_real rhe = search.ArmijoLeftHand();

  EXPECT_DOUBLE_EQ(lhe.x[0], 1.0);
  EXPECT_DOUBLE_EQ(rhe.x[0], 2.0);

}
