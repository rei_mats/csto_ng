#include <iostream>
#include <fstream>
#include <gtest/gtest.h>
#include <qd/dd_real.h>
#include <mpack/mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_qd.hpp>
#include <src/utils/support_mpack.hpp>
#include <src/utils/array.hpp>
#include <src/opt/i_opt_target.hpp>
#include <src/opt/i_opt_for_test.hpp>
#include <src/opt/linear_search.hpp>
#include <src/opt/hess.hpp>
#include <src/opt/newton.hpp>

using namespace std;

TEST(Newton, Newton) {

  std::ofstream ofs("log/newton.log");

  OptTargetExample* target = new OptTargetExample(2, 3);
  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Constant<dd_real>(1, ofs);
  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(target, ofs);
  
  Newton<dd_real>*  solver = new Newton<dd_real>
    (20, 0.0001, 2, target, l_search, calc_hess, ofs);
solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());
  EXPECT_FALSE(solver->IsDivergence());
  EXPECT_NEAR(target->get_x().x[0], -1.3647, 0.0001);
  EXPECT_NEAR(target->get_y().x[0], -0.5344, 0.0001);

  delete target;
  delete solver;
  delete l_search;
  delete calc_hess;
}
TEST(Newton, DampedNewton) {

  std::ofstream ofs("log/damped_newton.log");
  
  OptTargetExample* target = new OptTargetExample(2, 3);
  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Armijo_TwoPiece<dd_real>
    (5, 0.01, 1, target, ofs);
  ICalcHess<dd_real>* calc_hess =
    new CalcTrueHess<dd_real>(target, ofs);
  
  Newton<dd_real>*  solver = new Newton<dd_real>
    (50, 0.0001, 2, target, l_search, calc_hess, ofs);
  solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());
  EXPECT_FALSE(solver->IsDivergence());
  EXPECT_NEAR(target->get_x().x[0], -1.3647, 0.0001);
  EXPECT_NEAR(target->get_y().x[0], -0.5344, 0.0001);

  delete target;
  delete solver;
  delete l_search;
  delete calc_hess;
}
TEST(Newton, QuasiNewton) {

  std::ofstream ofs("log/quasi_newton.log");
  
  OptTargetExample* target = new OptTargetExample(2, 3);

  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Armijo_TwoPiece<dd_real>
      (30, 0.1, 3, target, ofs);
  
  ICalcHess<dd_real>* calc_hess =
    new CalcHessBFGS<dd_real>(target, ofs);
  
  Newton<dd_real>*  solver = new Newton<dd_real>
    (100, 0.0001, 2, target, l_search, calc_hess, ofs);

solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());
  EXPECT_FALSE(solver->IsDivergence());
  EXPECT_NEAR(target->get_x().x[0], -1.3647, 0.0001);
  EXPECT_NEAR(target->get_y().x[0], -0.5344, 0.0001);

  delete target;
  delete solver;
  delete l_search;
  delete calc_hess;
}
TEST(Newton, SteepestDescent) {
  std::ofstream ofs("log/steepest_descent.log");
  
  OptTargetExample* target = new OptTargetExample(2, 3);
  ILinearSearch<dd_real>* l_search =
    new LinearSearch_Armijo_TwoPiece<dd_real>
    (30, 0.1, 10, target, ofs);
  ICalcHess<dd_real>* calc_hess =
    new CalcHessSteepestDescent<dd_real>(2, ofs);
  
  Newton<dd_real>* solver = new Newton<dd_real>
    (100, 0.0001, 2, target, l_search, calc_hess, ofs);

solver->Compute();

  EXPECT_TRUE(solver->IsConvergence());
  EXPECT_FALSE(solver->IsDivergence());
  EXPECT_NEAR(target->get_x().x[0], -1.3647, 0.0001);
  EXPECT_NEAR(target->get_y().x[0], -0.5344, 0.0001);

  delete target;
  delete solver;
  delete l_search;
  delete calc_hess;

}

