// *  Begin    
// ** ifndef

#ifndef SQP_HPP
#define SQP_HPP


// *  Class    
// ** begin

template<class RF>
class SequentialQudraticProgramming
{

// ** field

private:
  unsigned int max_iter_;
  double       eps_;
  unsigned int num_;
  IOptTargetConstraint<RF>* target_;
  ILinearSearch<RF>* linear_search_;
  ICalcHess<RF>* calc_hess_;

  // ----- log ------
  std::ofstream& ofs_;

  // ----- work space -----
  unsigned int iter_;
  mpackint *ipiv_;
  RF* rhe_or_update_;
  RF*          hess_;
  
  
// ** constructor
// *** constructor
  
public:
  SequentialQudraticProgramming
  (unsigned int _max_iter,
   double       _eps,
   IOptTargetConstraint<RF>*       _target,
   ILinearSearch<RF>* _linear_search,
   ICalcHess<RF>*     _calc_hess,
   std::ofstream& _ofs):
    max_iter_(_max_iter),
    eps_(_eps),
    num_(_target->GetNum()),
    target_(_target),
    linear_search_(_linear_search),
    calc_hess_(_calc_hess),
    ofs_(_ofs)
  {
    iter_ = 0;
    ipiv_ = new mpackint[num_ + 1];
    rhe_or_update_ = new RF[num_ + 1];
    hess_ = new RF[(num_+1) * (num_+1)];
  }
  
// *** destructor

public:
  ~SequentialQudraticProgramming() 
  {
    delete[] ipiv_;
    delete[] rhe_or_update_;
    delete[] hess_;
  }

  
// ** Update
// *** write log
private:
  void writeLog() {
    
    ofs_ << std::endl << "SQP:iter: " 
	 << iter_ << std::endl;

    ofs_ << "SQP:x_lambda:";
    for(unsigned int i = 0; i < target_->GetNum(); i++)
      ofs_ << target_->GetXs_i(i) << " ";
    ofs_ << target_->GetLambda() << std::endl;
    
    ofs_ << "SQP:(f,h): ";
    ofs_ << target_->GetValue() << ", ";
    ofs_ << target_->GetValue_h();
    ofs_ << std::endl;

    //    calc_hess_->WriteLog();
    print_matrix("SQP:A:", ofs_, num_+1, hess_);
    print_vector_parallel("SQP:RHE", ofs_, num_+1, 
			  rhe_or_update_);
    ofs_ << std::endl;

  }
  
// *** calculate

private:
  void calculate()
  {
    //   target_->CalcValue();
    //    target_->CalcGrad();
    if(calc_hess_->NeedTrueHess()) 
      target_->CalcHess();
    else
      target_->CalcGrad();
  }
  
  
// *** set grad and hess
  
private:
  void set_grad_hess()
  {
    /**
     * see Chapter 18 (Sequential Quadratic Programming) of
     *  J.Nocedal and S.J.Wright "Numerical Optimization"
    **/
    
    // ------- Right hand of Newton eq. -----------
    /*
    for(unsigned int i = 0; i < num_; i++) {
      rhe_or_update_[i] =  target_->GetGrad()[i];
      rhe_or_update_[i] += target_->GetLambda() * 
 	                   target_->GetGrad_h()[i];
     rhe_or_update_[i] *= -1;
    }
    rhe_or_update_[num_] = -target_->GetValue_h();
    */
    for(std::size_t i = 0; i < num_; i++)
      {
	RF ei = -target_->GetGrad()[i];
	rhe_or_update_[i] = ei;
      }
    rhe_or_update_[num_] = -target_->GetValue_h();
    

    // ------ Matrix of Newton eq. ------------
    for(unsigned int i = 0; i < num_; i++)
      {
	RF gh = target_->GetGrad_h()[i];
	hess_[i + (num_+1)*num_] = -gh;
	hess_[num_ + (num_+1)*i] = gh;
	for(unsigned int j = 0; j < num_; j++)
	  {
	    RF ddf = calc_hess_->GetHess()[i + num_*j];
	    RF l   = target_->GetLambda();
	    RF ddh = target_->GetHess_h()[i + num_ * j];
	    hess_[i+(num_+1)*j] = ddf - l * ddh;
	  }
      }
    hess_[num_ + (num_+1) * num_] = value_zero<RF>();    
  }
  
// *** update
public:
  bool Update()
  {
    mpackint info;

    // ------- step iteration -----
    
    iter_++;    
    if(iter_ > max_iter_)
      return false;

    set_grad_hess();
    writeLog();
    
    // ------- solve KKT equation -----
    solve_linear_eq(num_+1, hess_, rhe_or_update_,
		    ipiv_, &info);
    print_vector_parallel("ipiv:", ofs_,num_+1,ipiv_);
    ofs_ << std::endl;
    print_vector_parallel("SQP:update0", ofs_, num_+1, 
			  rhe_or_update_);
    ofs_ << std::endl;

    // ------- convergence check -----
    // note: max_delta_x is maximum of update vector
    //       without Lagrange multipliner.
    RF max_delta_x = max_abs_value(num_, rhe_or_update_);
    if(max_delta_x < eps_)
      return false;    

    // ------- linear search ---------
    linear_search_->Compute(rhe_or_update_);
    for(unsigned int i = 0; i < num_; i++)
      rhe_or_update_[i] *= linear_search_->GetStep();
    print_vector_parallel("update1: ", ofs_, 
			  num_+1, rhe_or_update_);
    ofs_ << std::endl;

    // ------- update -------------
    /** 
     *   variables are shifted by update direction
     *   lambda is replaced by new value
     */ 
    target_->SetUpdate(rhe_or_update_);
    // target_->SetLambda(rhe_or_update_[num_]);
    target_->SetUpdateLambda(rhe_or_update_[num_]);

    calculate();
    calc_hess_->UpdateAfter();

    return true;
  }

// ** Compute

public:

  void Compute()
  {
    calculate();
    calc_hess_->Init();
    while(Update());
  }

// ** State

public:
  bool IsConvergence(){
    return iter_ != 0 && iter_ < max_iter_;
  }

public:
  bool IsDivergence() {
    return iter_ > max_iter_;
  }
  
// ** end

};


// *  End      


#endif
