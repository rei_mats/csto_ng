// *  Begin          
// ** comment

/**
 * @file linear_search.hpp
 * @brief find minima for one variable function using Armijo method.
 * @date 2014/1/9
 * @author Rei Matsuzaki
 */


// ** ifndef

#ifndef LINEAR_SEARCH_HPP
#define LINEAR_SEARCH_HPP


// *  Enums          

enum LinearSearchRule { Armijo, Wolfe };
enum LinearSearchUpdate { TwoPiece };
enum LinearSearchPenalty { SquaredPenalty, ExactPenalty};

// *  Interface      
// ** begin

template<class RF>
class ILinearSearch
{
protected:
  std::ofstream& ofs_;
  
public:
  ILinearSearch(std::ofstream& _ofs) :
    ofs_(_ofs) {}
  virtual ~ILinearSearch() {}
  virtual void Compute(RF* d) = 0;
  virtual RF GetStep() = 0;
  

};

// *  Constant Step  

template<class RF>
class LinearSearch_Constant : public ILinearSearch<RF>
{
private:
  RF step_;

public:
  LinearSearch_Constant(RF _step, std::ofstream& _ofs):
    ILinearSearch<RF>(_ofs),
    step_(_step) {}

  virtual void Compute(RF* d) {}
  virtual RF GetStep() {
    return step_;
  }
};

// *  non constraint 

template<class RF>
class LinearSearch_Armijo_TwoPiece : public ILinearSearch<RF>
{
private:
  /* ---- calculation setting ---- */
  unsigned int max_iter_;
  double armijo_zeta_;
  RF     alpha0_;
  RF     alpha_;
  
  /* ---- reference ------*/
  IOptTarget<RF>* target_;

  /* ---- work space ---- */
  RF* alpha_d_;
  
public:
  LinearSearch_Armijo_TwoPiece(unsigned int _max_iter,
			       double _armijo_zeta,
			       RF _alpha0,
			       IOptTarget<RF>* t,
			       std::ofstream& _ofs):
    ILinearSearch<RF>(_ofs)
  {
    max_iter_ = _max_iter;
    armijo_zeta_ = _armijo_zeta;
    alpha0_ = _alpha0;
    alpha_  = _alpha0;

    target_ = t;
    
    alpha_d_ = new RF[t->GetNum()];
  }
  ~LinearSearch_Armijo_TwoPiece() {
    delete[] alpha_d_;
  }
  virtual void Compute(RF* d) 
  {
    
    unsigned int n = target_->GetNum();
    alpha_ = alpha0_;

    RF f_x = target_->GetValue();
    RF df_d = 0;
    for(unsigned int i = 0; i < n; i++)
      df_d += d[i] * target_->GetGrad()[i];
    
    for(unsigned int iter = 0; iter < max_iter_; iter++)
      {
	
	this->ofs_ << "LinearSearch(Armijo):iter:";
	this->ofs_ << iter << ", ";
	this->ofs_ << alpha_ << std::endl;
	  
	for(unsigned int i = 0; i < n; i++)
	  alpha_d_[i] = alpha_ * d[i];
	target_->SetUpdate(alpha_d_);

	target_->CalcValue();
	RF f_x_ad = target_->GetValue();

	for(unsigned int i = 0; i < n; i++)
	  alpha_d_[i] = -alpha_ * d[i];
	target_->SetUpdate(alpha_d_);
	//	target->CalcValue();

	// Armijo's rule
	if(f_x_ad < f_x + armijo_zeta_ * df_d * alpha_)
	  break;

	alpha_ /= 2;
      }
    target_->CalcValue();
  }
  virtual RF GetStep() {
    return alpha_;
  }

};

// *  merit function 
// ** begin

template<class RF>
class LinearSearchForMerit : public ILinearSearch<RF>  
{


// ** field

private:
  unsigned int max_iter_;
  RF     armijo_zeta_;
  RF     penalty_rho_;
  RF     alpha0_;
  LinearSearchUpdate update_type_;
  LinearSearchRule   rule_type_;
  LinearSearchPenalty penalty_type_;
  
  RF     alpha_;
  unsigned int iter_;

  RF* alpha_d_;

  /* ---- reference ------*/
  IOptTargetConstraint<RF>* target_;


// ** constructors
// *** constructor
  
public:
  LinearSearchForMerit
  (unsigned int _max_iter,
   RF _armijo_zeta,
   RF _penalty_rho,
   RF _alpha0,
   LinearSearchUpdate _update_type,
   LinearSearchRule   _rule_type,
   LinearSearchPenalty _penalty_type,
   IOptTargetConstraint<RF>* t,
   std::ofstream& _ofs) :
    ILinearSearch<RF>(_ofs),
    max_iter_(_max_iter),
    armijo_zeta_(_armijo_zeta),
    penalty_rho_(_penalty_rho),
    alpha0_(_alpha0),
    update_type_(_update_type),
    rule_type_(_rule_type),
    penalty_type_(_penalty_type),
    alpha_(_alpha0)
  {
    alpha_d_ = new RF[t->GetNum()];
    target_ = t;
    iter_ = 0;
  }

// *** destructor

  ~LinearSearchForMerit()
  {
    delete[] alpha_d_;
  }


// ** merit function
// *** merit

  RF merit()
  {
    RF f = target_->GetValue();
    RF h = target_->GetValue_h();
    if(penalty_type_ == SquaredPenalty)
      {
	return f + penalty_rho_ * h * h;
      }
    else if(penalty_type_ == ExactPenalty)
      {
	return f + penalty_rho_ * abs(h);
      }
    else
      {
	std::cout << "LinearSearchForMerit::penalty_term for this penaltry term is not implemented." << std::endl;
	exit(1);
      }
  }

// *** grad

  RF grad_merit_i(unsigned int i)
  {
    RF df = target_->GetGrad()[i];
    RF dh = target_->GetGrad_h()[i];
    RF h  = target_->GetValue_h();

    switch(penalty_type_)
      {
      case SquaredPenalty:
	return df + penalty_rho_ * 2 * h * dh;
	break;
      case ExactPenalty:
	if(h > 0)
	  return df + penalty_rho_ * dh;
	else
	  return df - penalty_rho_ * dh;
	break;
      default:
	std::cout << "LinearSearchForMerit::grad_merit_i"
		  << std::endl;
	exit(1);
      }
    
  }

// *** direction derivative

  RF direction_derivative(RF* d)
  {
    unsigned int n = target_->GetNum();
    RF df_d = 0;

    for(unsigned int i = 0 ; i < n; i++)
      df_d += d[i] * target_->GetGrad()[i];

    if(penalty_type_ == SquaredPenalty)
      {
	string msg 
	  = "LinearSearchForMerit::direction_derivative.\n";
	msg += "square penalty is not implemented.\n";
	throw msg;
      }
    else
      {

	RF h0 = target_->GetValue_h();
	RF dd = df_d -penalty_rho_ * abs(h0);
	return dd;
      }
  }
 

// ** Compute

public:
  
  virtual void Compute(RF* d)
  {
    unsigned int n = target_->GetNum();
    alpha_ = alpha0_;

    /*
    RF dg_d = 0; RF df_d = 0;
    for(unsigned int i = 0; i < n; i++)
      {
	dg_d += d[i] * grad_merit_i(i);
	df_d += d[i] * target_->GetGrad()[i];
      }
    */
    RF g0  = merit();
    RF dd  = direction_derivative(d);
    RF h0 = abs(target_->GetValue_h());
    
    this->ofs_ << "Start linear search(Merit)" << std::endl;
    this->ofs_ << "rho :" << penalty_rho_ << std::endl;
    this->ofs_ << "zeta :" << armijo_zeta_ << std::endl;
    this->ofs_ << "iter, alpha, g1, g0+az(d,dg)" << std::endl;

    if(dd > 0)
      this->ofs_ << "WARNING: LinearSearch(Merit):direction is not descent direction!" << std::endl;

    for(iter_ = 0; iter_ < max_iter_; iter_++)
      {

	// update donot contain Lagrange multiplinear lambda
	for(unsigned int i = 0; i < n; i++)
	  alpha_d_[i] = alpha_ * d[i];
	target_->SetUpdate(alpha_d_);

	target_->CalcValue();
	RF g1 = merit();
	RF rhs = g0 + armijo_zeta_* dd * alpha_;
	
	for(unsigned int i = 0; i < n; i++)
	  alpha_d_[i] *= -1;
	target_->SetUpdate(alpha_d_);

	// update penalty parameter
	if(penalty_rho_ < dd / (0.2 * abs(h0)))
	  penalty_rho_ *= 2;

	// print to log file
	this->ofs_ << iter_  << ", "
		   << alpha_ << ", "  
		   << g1     << ", "
		   << rhs    << ", "
		   << penalty_rho_ << std::endl;

	// Armijo's rule
	bool satisfied = false;
	switch(rule_type_)
	  {
	  case Armijo:
	    satisfied = g1 < rhs;
	    break;
	  default:
	    std::cout << "LinearSearch::Compute, this rule is not implemented.";
	    exit(1);
	    break;
	  }

	if(satisfied)
	  break;

	/*
	if(satisfied)
	  {
	    if(iter_ == 0)
	      alpha0_ *= 2;
	    else
	      alpha0_ = alpha_;
	    break;
	  }
	*/

	alpha_ /= 2;
      }

    target_->CalcValue();
    if(IsDivergence())
      {
	std::string msg = "FAILED: linear search\n";
	this->ofs_ << msg;
	throw msg;
      }
  }


// ** status  
// *** is_convergence

public:
  bool IsConvergence() { 
    return 0<iter_&&iter_ < max_iter_-1; }
  bool IsDivergence()  { return max_iter_-1 <= iter_; }
  
// ** Get step

public:
  virtual RF GetStep() {
    return alpha_;
  }


// ** end
  
};


// *  End            

#endif
