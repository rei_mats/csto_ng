// *  Begin          
// ** comment

/**
 * @file steepest_descent.hpp
 * @brief find minima for multi variable function by
 * steepest descent method. Step size is choosen from
 * constant or linear search(Armijo's rule)
 */

// ** ifndef

#ifndef STEEPEST_DESCENT_HPP
#define STEEPEST_DESCENT_HPP


// ** include

#include <qd/dd_real.h>
#include <src/utils/support_mpack.hpp>
#include <src/opt/step_size.hpp>
#include <src/opt/linear_search.hpp>



// *  Utils          
// ** step size
// *** template

template<DetermineStepSize SS>
dd_real CalcStepSize(dd_real _a0, 
		     LinearSearch<dd_real>* search) {
  return 0;
}

template<DetermineStepSize SS>
double CalcStepSize(double _a0, 
		    LinearSearch<dd_real>* search) {
  return 0.0;
}

// *** constant

template<>
dd_real CalcStepSize<E_Constant>
(dd_real _a0, LinearSearch<dd_real>* search) {
  return _a0;
}

template<>
double CalcStepSize<E_Constant>
(double _a0, LinearSearch<double>* search) {
  return _a0;
}

// *** linear search

template<>
dd_real CalcStepSize<E_LinearSearch>
(dd_real _a0, LinearSearch<dd_real>* search) {

  search->Compute(_a0);
  return research->GetAlpha();
}

template<>
double CalcStepSize<E_LinearSearch>
(double _a0, LinearSearch<double>* search) {

  search->Compute(_a0);
  return research->GetAlpha();
}


// *  Class General  
// ** begin

template<class RF, DetermineStepSize SS>
class SteepestDescent {

// ** field member

public:

  // ------ calculation settings -------
  unsigned int max_iter_; /** maximum iteration number */
  RF (*f_)(RF*, void*);   /** target function */
  void (*g_)(RF*, RF*, void*); /** gradient of target func.*/
  void* ctx_;             /** component of func */
  unsigned int num_;      /** number of variables */
  double eps_;            /** convergence epsilon */

  // ------ step size determination ------
  LinearSearch<RF>* linearSearch_;

  // ------ calcualtion status -----
  RF* xs_;                 /** variables */
  RF* gs_;                 /** gradient */
  RF* ds_;                 /** update direction */
  RF  alpha_;              /** step size */

// ** constructor

public:  

  SteepestDescent(unsigned int _max_iter,
		  RF(*_f)(RF*,void*),
		  void(*_g)(RF*, RF*, void*),
		  void* _ctx,
		  unsigned int _num,
		  double _eps,
		  LinearSearch<RF>* _linearSearch,
		  RF _alpha) :
    max_iter_(_max_iter),
    f_(_f),
    g_(_g),
    ctx_(_ctx),
    num_(_num),
    eps_(_eps),
    linearSearch_(_linearSearch) {
    
    alpha_ = _alpha;
    xs_ = new RF[_num];
    gs_ = new RF[_num];
    ds_ = new RF[_num];    

    std::cout << "general" << std::endl;
    
    }

// ** destructor

  ~SteepestDescent() {
    delete[] xs_;
    delete[] gs_;
    delete[] ds_;
  }



// ** compute

public:  
  void Compute(RF* _x0s) {

    // set initial guess
    for(unsigned int i = 0; i < num_; i++)
      xs_[i] = _x0s[i];

    // start convergence loop
    for(unsigned int iter = 0; iter < max_iter_; iter++) {

      // gradient and update direction
      g_(xs_, gs_, ctx_);
      for(unsigned int i = 0; iter < num_; iter++)
	ds_[i] = -g_[i];

      // calc step size
      RF alpha = CalcStepSize<SS>(alpha_, linearSearch_);

      // update variables
      for(unsigned int i = 0; i < num_; i++)
	xs_[i] += alpha * ds_[i];
    }

  }
  
// ** end

};


// *  Class (Constant step size)  
// ** begin

template<class RF>
class SteepestDescent<RF, E_Constant> {

// ** field

  // ------ calculation settings -------
  unsigned int max_iter_; /** maximum iteration number */
  RF (*f_)(RF*, void*);   /** target function */
  void (*g_)(RF*, RF*, void*); /** gradient of target func.*/
  void* ctx_;             /** component of func */
  unsigned int num_;      /** number of variables */

  // ------ step size determination ------
  

  // ------ calcualtion status -----
  RF* xs_;                 /** variables */
  RF* gs_;                 /** gradient */
  RF* ds_;                 /** update direction */
  RF  alpha_;              /** step size */  

// ** constructor

public:
  
  SteepestDescent(unsigned int _max_iter,
		  RF(*_f)(RF*,void*),
		  void(*_g)(RF*, RF*, void*),
		  void* _ctx,
		  unsigned int _num,
		  RF _alpha) :
    max_iter_(_max_iter),
    f_(_f),
    g_(_g),
    ctx_(_ctx),
    num_(_num) {

    alpha_ = _alpha;
    xs_ = new RF[_num];
    gs_ = new RF[_num];
    ds_ = new RF[_num];    

    std::cout << "const" << std::endl;
    
    }



// ** destructor

  ~SteepestDescent() {
    delete[] xs_;
    delete[] gs_;
    delete[] ds_;
  }
  

// ** end

};


// *  End          

#endif
