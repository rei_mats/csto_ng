// *  Begin      

#ifndef I_OPT_FOR_TEST_HPP
#define I_OPT_FOR_TEST_HPP

// *  example    

class OptTargetExample : public IOptTarget<dd_real>
{

private:
  dd_real xs_[2];
 
public:
  OptTargetExample(dd_real _x, dd_real _y): 
    IOptTarget<dd_real>(2) {
    xs_[0] = _x;
    xs_[1] = _y;
  }
 
  //  ~OptTargetExample() {}
  virtual void CalcValue() {
    dd_real x = xs_[0];
    dd_real y = xs_[1];
    val_ = x*x*x*x/2 - 2*x*x*y + 4*y*y + 8*x + 8*y;
  }
  virtual void CalcGrad() {
    dd_real x = xs_[0];
    dd_real y = xs_[1];
    grad_[0] = 2*x*x*x -4*x*y + 8;
    grad_[1] = -2*x*x + 8*y + 8;

    CalcValue();
  }
  virtual void CalcHess() {
    dd_real x = xs_[0];
    dd_real y = xs_[1];
    hess_[0] = 6*x*x -4*y;
    hess_[1] = -4*x;
    hess_[2] = -4*x;
    hess_[3] = 8;

    CalcGrad();
  }
  virtual void SetUpdate(dd_real* d) {
    xs_[0] += d[0];
    xs_[1] += d[1];
  }
  dd_real GetXs_i(unsigned i) const { return xs_[i]; }
  dd_real get_x() {return GetXs_i(0); }
  dd_real get_y() {return GetXs_i(1); }
};

// *  example 2  

class OptTargetExample1 : public IOptTargetConstraint<dd_real>
{
private:
  dd_real xs_[2];
public:
  OptTargetExample1(dd_real _x, dd_real _y): 
    IOptTargetConstraint<dd_real>(2, "0") {
    xs_[0] = _x; xs_[1] = _y;
  }
 
  //  ~OptTargetExample() {}
  virtual void CalcValue() {
    dd_real x = xs_[0]; dd_real y = xs_[1];
    val_ = x*x*x*x/2 - 2*x*x*y + 4*y*y + 8*x + 8*y;
    val_h_ = 2 * x + y * y + x * y;
  }
  virtual void CalcGrad() {
    dd_real x = xs_[0]; dd_real y = xs_[1];
    grad_[0] = 2*x*x*x -4*x*y + 8;
    grad_[1] = -2*x*x + 8*y + 8;
    grad_h_[0] = 2 + y; grad_h_[1] = 2 * y + x;
  }
  virtual void CalcHess() {
    dd_real x = xs_[0]; dd_real y = xs_[1];
    hess_[0] = 6*x*x -4*y;
    hess_[1] = -4*x;
    hess_[2] = -4*x;
    hess_[3] = 8;
    hess_h_[0] = "0"; hess_h_[1] = 1;
    hess_h_[2] = 1; hess_h_[3] = 2;
  }
  virtual void SetUpdate(dd_real* d) {
    xs_[0] += d[0]; xs_[1] += d[1];
  }
  dd_real GetXs_i(unsigned i) const { return xs_[i]; }
};


// *  test       
// ** namespace

namespace Test_OptTarget
{

// ** update

  template<class RF>
  void SetUpdate
  (IOptTarget<RF>* target, RF* d, double eps)
  {
    unsigned int n = target->GetNum();
    target->CalcValue();
    RF f0 = target->GetValue();

    target->SetUpdate(d);
    target->CalcValue();
    RF f1 = target->GetValue();
    
    RF neg_one = -value_one<RF>();
    scalar_vec(n, d, neg_one);
    target->SetUpdate(d);
    target->CalcValue();
    RF f2 = target->GetValue();

    EXPECT_TRUE(abs(f0-f2) < eps)
      << "f0 : " << f0 << std::endl
      << "f2 : " << f2 << std::endl;
    
    EXPECT_FALSE(abs(f0-f1) <eps)
      << "f0 : " << f0 << std::endl
      << "f1 : " << f1 << std::endl;

  }

  
// ** grad test

  template<class RF>
  void Grad_finite_dif
  (IOptTarget<RF>* target, RF h, RF eps)
  {
    unsigned int n = target->GetNum();
    RF* d = new RF[n];
    target->CalcGrad();
    RF* df0 = new RF[n];
    copy_array(n, target->GetGrad(), df0);
  
    for(unsigned int i = 0; i < n; i++)
      {
	set_same_value(n, d, value_zero<RF>());
	
	d[i] = h;
	target->SetUpdate(d);
	target->CalcValue();
	RF fp = target->GetValue();
	
	d[i] = -2 * h;
	target->SetUpdate(d);
	target->CalcValue();
	RF fm = target->GetValue();
	
	d[i] = h;
	target->SetUpdate(d);
	
	RF calc = (fp - fm) / (2 * h);
	RF expe = df0[i];
	EXPECT_TRUE(abs((calc - expe)/expe) < eps)
	  << "index:" << i    << std::endl
	  << "calc: " << calc << std::endl
	  << "expe: " << expe << std::endl
	  << "fp:   " << fp << std::endl
	  << "fm:   " << fm << std::endl;
      }

    delete[] d;
    delete[] df0;
  }

// ** hess test

  template<class RF>
  void Hess_finite_dif
  (IOptTarget<RF>* target, RF h, RF eps)
  {
    using std::cout;
    using std::endl;
    
    unsigned int n = target->GetNum();
    
    RF* d = new RF[n];
    target->CalcHess();
    RF* ddf0 = new RF[n*n];
    copy_array(n*n, target->GetHess(), ddf0);
    RF f0 = target->GetValue();

    RF z = value_zero<RF>();

    for(unsigned int i = 0; i < n; i++)
      for(unsigned int j = 0; j < n; j++)
	{

	  set_same_value(n, d, value_zero<RF>());

	  d[i] = z; d[j] = z; d[i] += h; d[j] += h;
	  target->SetUpdate(d); target->CalcValue();
	  RF f12 = target->GetValue();

	  d[i] = z; d[j] = z; d[i] -= h;
	  target->SetUpdate(d); target->CalcValue();
	  RF f2 = target->GetValue();

	  d[i] = z; d[j] = z; d[i] += h; d[j] -= h;	  
	  target->SetUpdate(d); target->CalcValue();
	  RF f1 = target->GetValue();

	  d[i] = z; d[j] = z; d[i] -= h;
	  target->SetUpdate(d);

	  RF calc = (f12 + f0 - f1 - f2) / (h * h);
	  RF expe = ddf0[i + n * j];


	  if(abs(expe) < eps)
	    EXPECT_TRUE(abs(calc) < eps)
	      << " Hess finite diff test" << std::endl
	      << "index: " << i << ", " << j << std::endl
	      << "calc:  " << calc << std:: endl
	      << "expe:  " << expe;
	  else
	    EXPECT_TRUE(abs(calc-expe)/expe < eps)
	      << " Hess finite diff test" << std::endl
	      << "index: " << i << ", " << j << std::endl
	      << "calc:  " << calc << std:: endl
	      << "expe:  " << expe;      
	}
    
    delete[] d;
    delete[] ddf0;
  }



// ** grad constraint test

  template<class RF>
  void GradConstraint_finite_dif
  (IOptTargetConstraint<RF>* target, RF h, RF eps)
  {
    unsigned int n = target->GetNum();
    RF* d = new RF[n];
    target->CalcGrad();
    RF* df0 = new RF[n];
    copy_array(n, target->GetGrad_h(), df0);
  
    for(unsigned int i = 0; i < n; i++)
      {
	set_same_value(n, d, value_zero<RF>());
	
	d[i] = h;
	target->SetUpdate(d);
	target->CalcValue();
	RF fp = target->GetValue_h();
	
	d[i] = -2 * h;
	target->SetUpdate(d);
	target->CalcValue();
	RF fm = target->GetValue_h();
	
	d[i] = h;
	target->SetUpdate(d);
	
	RF calc = (fp - fm) / (2 * h);
	RF expe = df0[i];
	EXPECT_TRUE(abs((calc - expe)/expe) < eps)
	  << "index:" << i    << std::endl
	  << "calc: " << calc << std::endl
	  << "expe: " << expe;
      }

    delete[] d;
    delete[] df0;
    
  }

// ** hess constraint test

  template<class RF>
  void HessConstraint_finite_dif
  (IOptTargetConstraint<RF>* target, RF h, RF eps)
  {
    unsigned int n = target->GetNum();
    RF* d = new RF[n];
    target->CalcHess();
    RF* ddf0 = new RF[n*n];
    copy_array(n*n, target->GetHess_h(), ddf0);
    RF f0 = target->GetValue_h();

    for(unsigned int i = 0; i < n; i++)
      for(unsigned int j = 0; j < n; j++)
	{

	  set_same_value(n, d, value_zero<RF>());
	  d[i] += h; d[j] += h;
	  target->SetUpdate(d); target->CalcValue();
	  RF f12 = target->GetValue_h();

	  set_same_value(n, d, value_zero<RF>());
	  d[i] -= h;
	  target->SetUpdate(d); target->CalcValue();
	  RF f2 = target->GetValue_h();

	  set_same_value(n, d, value_zero<RF>());	  
	  d[i] += h; d[j] -= h;	  
	  target->SetUpdate(d);
	  target->CalcValue();
	  RF f1 = target->GetValue_h();

	  set_same_value(n, d, value_zero<RF>());
	  d[i] -= h;
	  target->SetUpdate(d);

	  RF calc = (f12 + f0 - f1 - f2) / (h * h);
	  RF expe = ddf0[i + n * j];
	  //	  std::cout << i << " "<< j << " " <<expe << std::endl; 
	  
	  EXPECT_TRUE(abs((calc-expe)/expe) < eps)
	    << " hess of constraint test" << std::endl
	    << "index     : " << i << ", " << j << std::endl
	    << "f12       : " << f12 << std:: endl
	    << "f2        : " << f2  << std::endl
	    << "f1        : " << f1  << std::endl
	    << "f0        : " << f0  << std::endl
	    << "fini diff : " << calc << std::endl
	    << "anlytic   : " << expe << std::endl;
	  

	  
	}
    
    delete[] d;
    delete[] ddf0;
  }

  
// ** hess symmetric test
// *** non constraint

  template<class RF>
  void HessSymmetric(IOptTarget<RF>* target)
  {

    unsigned int n = target->GetNum();
    target->CalcHess();
    RF* hess = target->GetHess();

    for(unsigned int i = 0; i < n; i++)
      for(unsigned int j = 0; j < i; j++)
	{
	  EXPECT_EQ(hess[i + n * j],
		    hess[j + n * i]) <<
	    "HessSymmetricTest : " <<
	    i << ", " << j;
	}
  }


// *** with constraint

  template<class RF>
  void HessConstraintSymmetric(IOptTargetConstraint<RF>* target)
  {
    unsigned int n = target->GetNum();
    target->CalcHess();
    RF* hess_h = target->GetHess_h();

    for(unsigned int i = 0; i < n; i++)
      for(unsigned int j = 0; j < i; j++)
	{
	  EXPECT_EQ(value_zero<RF>(),
		    hess_h[i+n*j]-hess_h[j+n*i]) <<
	    "HessConstraint : " <<
	    i << ", " << j << std::endl <<
	    "h_ij-h_ji : " << hess_h[i+n*j]-hess_h[j+n*i];
	  
	}    
  }
  
  
// ** end
};
#endif
