// *  Begin   

#include <qd/dd_real.h>
#include <dd_complex.h>
#include <mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_string.hpp>
#include <src/utils/support_qd.hpp>
#include <src/utils/array.hpp>
#include <src/utils/key_val_reader.hpp>
#include <src/math/factorial.hpp>
#include <src/math/complex_jacob.hpp>
#include <src/math/erfc.hpp>
#include <src/math/sto_gto_int_minor.hpp>
#include <src/math/sto_gto_int.hpp>
#include <src/ao/lcao.hpp>

using std::cout;
using std::endl;
using std::cerr;


// *  Main    
// ** read

void read_file(KeyValReader&  kv,
	       LCAO<STO<dd_complex>, dd_complex>** sto,
	       LCAO<GTO<dd_complex>, dd_complex>** gto)
{
  
  unsigned int numGto = kv.CheckGet<int>("numGto");
  int          pnGto  = kv.CheckGet<int>("nGto");
  int          pnSto  = kv.CheckGet<int>("nSto");
  dd_complex   zSto   = kv.CheckGet<dd_complex>("zSto");
  
  dd_complex* zs = new dd_complex[numGto];
  dd_complex* cs = new dd_complex[numGto];
  
  for(unsigned int i = 0; i < numGto; i++)
    {
      string key;
      
      key = "z" + lexical_cast<string>(i);
      zs[i] = kv.CheckGet<dd_complex>(key);

      key = "c" + lexical_cast<string>(i);
      cs[i] = kv.CheckGet<dd_complex>(key);
    }

  *gto = New_LC_NormalizedGTOs(pnGto, numGto, cs, zs);
  *sto = New_LC_MonoSTO(pnSto, zSto);

  delete[] zs;
  delete[] cs;
}


// ** write

void write_stong(LCAO<STO<dd_complex>, dd_complex>& sto,
		 LCAO<GTO<dd_complex>, dd_complex>& gto, 
		 dd_real r0, dd_real r1, dd_real dr)
{
  cout << "# r, Re[STO], Im[STO], Re[STO-NG], Im[STO-NG]"; 
  cout << endl;

  for(dd_real r = r0; r < r1; r += dr)
    {
      dd_complex s = sto.At_r(r);
      dd_complex g = gto.At_r(r);
      cout << r   << " " << s.real() << " " << s.imag();
      cout <<        " " << g.real() << " " << g.imag();
      cout << endl;
    }
}


// ** main

int main (int argc, char** argv)
{

  // ------- check option number -----
  if(argc != 5)
    {
      cout << "USAGE: write_ao file r0 r1 dr" << endl;
      exit(1);
    }
  
  string file_name;
  dd_real r0, r1, dr;

  // ------- read option------------
  bool correct_opt;
  file_name = argv[1];
  correct_opt  = string_to<dd_real>(argv[2], &r0);
  correct_opt &= string_to<dd_real>(argv[3], &r1);
  correct_opt &= string_to<dd_real>(argv[4], &dr);

  if(not correct_opt)
    {
      cerr << "Error: invalid option" << endl;
      exit(1);
    }

  LCAO<STO<dd_complex>, dd_complex> *sto;
  LCAO<GTO<dd_complex>, dd_complex> *gto;
  KeyValReader reader(':');
  ifstream ifs(argv[1]);
  try
    {
      reader.ReadFromStream(ifs);
      read_file(reader, &sto, &gto);
    }
  catch(string& e)
    {
      cerr << "Error occured reading file : ";
      cerr << file_name << endl;
      cerr << e;
      exit(1);
    }

  write_stong(*sto, *gto, r0, r1, dr);
  delete sto;
  delete gto;
}


