// *  Begin      
// ** include

#include <iostream>
#include <fstream>

#include <boost/lexical_cast.hpp>

#include <qd/dd_real.h>
#include <dd_complex.h>
#include <mblas_dd.h>
#include <mlapack_dd.h>

#include <src/utils/support_string.hpp>
#include <src/utils/support_qd.hpp>
#include <src/utils/array.hpp>
#include <src/utils/key_val_reader.hpp>
#include <src/math/log_scale.hpp>
#include <src/math/factorial.hpp>
#include <src/math/complex_jacob.hpp>
#include <src/math/erfc.hpp>
#include <src/math/sto_gto_int_minor.hpp>
#include <src/math/sto_gto_int.hpp>
#include <src/ao/lcao.hpp>
#include <src/ao/d_ao.hpp>
#include <src/ao/inner_prod.hpp>
#include <src/opt/i_opt_target.hpp>
 // #include <src/opt/i_opt_for_test.hpp>
#include <src/opt/linear_search.hpp>
#include <src/opt/hess.hpp>
#include <src/opt/sqp.hpp>
#include <src/opt/newton.hpp>

#include <src/stong/stong.hpp>
#include <src/stong/opt_full_matrix.hpp>


//#include <src/stong/calc_stong.hpp>
#include <src/stong/calc_template_stong.hpp>

// ** using
// *** fff

using boost::algorithm::split;
using boost::algorithm::is_any_of;
using boost::algorithm::trim_copy;
using boost::lexical_cast;


// *  Main       

int main(int argc, char** argv)
{

  /* aaa */
  
  if(argc != 2)
    {
      std::cout << "number of command line option must be one.";
      std::cout << std::endl;
      exit(1);
    }
  
  KeyValReader reader(':');
  std::ifstream ifs(argv[1]);
  try
    {
      reader.ReadFromStream(ifs);
    }
  catch(string& e)
    {
      cerr << "Error occured while reading file :";
      cerr << argv[1] << endl;
      cerr << e;
      exit(1);
    }

  string sto_type, gto_type;
  try
    {
      sto_type = reader.CheckGet<string>("sto_type");
      gto_type = reader.CheckGet<string>("gto_type");
    }
  catch (string& e)
    {
      cerr << "Error occured.";
      cerr << "sto_type and and gto_type are necessary.";
      cerr << endl;
      cerr << e;
      exit(1);
    }
  
  if(sto_type == "dd_real" && sto_type == "dd_real")
    {
      CalcStong<dd_real, dd_real, dd_real> stong(&reader);
      stong.Run();
    }
  else if(sto_type == "dd_complex" && sto_type == "dd_complex")
    {
      CalcStong<dd_complex, dd_complex, dd_real> stong(&reader);
      stong.Run();
    }
  else
    {
      cerr << "invalid value for sto_type or gto_type.";
      cerr << endl;
    }

  return(0);
}



