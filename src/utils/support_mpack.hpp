// * Begin       
// ** ifndef

#ifndef SUPPORT_MPACK_HPP
#define SUPPORT_MPACK_HPP

// *  Lapack     

void solve_linear_eq(unsigned int n, 
		     dd_real* A,
		     dd_real* x_or_b,
		     mpackint* ipiv,
		     mpackint* info) {
  Rgesv(n, 1, A, n, ipiv, x_or_b, n, info);
}


// *  End        

#endif
