
#include <src/utils/support_string.hpp>
#include <src/utils/key_val_reader.hpp>
#include <gtest/gtest.h>


TEST(KeyValReader, split) {
  string line = "aaa:bbb:ccc";
  vector<string> line_list = split_string(line, ':');
  EXPECT_EQ(line_list[0], "aaa");
  EXPECT_EQ(line_list[1], "bbb");
  EXPECT_EQ(line_list[2], "ccc");
  EXPECT_EQ(line_list.size(), (unsigned int)3);
  EXPECT_EQ(line, "aaa:bbb:ccc");
}
TEST(KeyValReader, remove) {
  string line("aa bb  ff     f");
  string removed = remove_char(line, ' ');
  EXPECT_EQ(removed, "aabbfff");

  line = "aabb";
  removed = remove_char(line, ' ');
  EXPECT_EQ(line, "aabb");
}
TEST(KeyValReader, is_integer) {
  EXPECT_TRUE(is_integer("123"));
  EXPECT_FALSE(is_integer("123a"));
}
TEST(KeyValReader, is_number) {
  EXPECT_TRUE(is_number("1.23"));
  EXPECT_TRUE(is_number("123"));
  EXPECT_TRUE(is_number("-12.3"));
  EXPECT_TRUE(is_number("+12.3"));
  EXPECT_FALSE(is_number("+12.3s"));
}
TEST(KeyValReader, to_string) {
  
  string str = to_string(11);
  EXPECT_EQ(str, "11");
}
TEST(KeyValReader, reader) {

  KeyValReader reader(':');
  ifstream ifs("dummy.dat");
  reader.ReadFromStream(ifs);
  EXPECT_EQ(reader.Get("key1"), "123");
  EXPECT_EQ(reader.Get("key2"), "44");
  EXPECT_EQ(reader.Get("kkk34"), "bbbb");
  EXPECT_TRUE(reader.Exist("key1"));
  EXPECT_FALSE(reader.Exist("dkey1"));

  EXPECT_TRUE(reader.IsNumberData("key1"));
  EXPECT_TRUE(reader.IsIntegerData("key1"));
}
TEST(KeyValReader, check) {

  ifstream ifs("dummy.dat");
  KeyValReader reader(':', ifs);
  EXPECT_THROW(reader.CheckExist("key3"), string);

  try
    {
      reader.CheckExist("key3");
    }
  catch(string& e)
    {
      cout << e << endl;
    }
  
}
TEST(KeyValReader, OneOf) {

  ifstream ifs("dummy.dat");
  KeyValReader reader(':', ifs);
  vector<string> v;
  
  reader.Display();
  
  v.push_back("123");
  v.push_back("12");
  EXPECT_TRUE(reader.OneOf("key1", v));

  vector<string> vv;
  vv.push_back("a");
  vv.push_back("b");
  EXPECT_FALSE(reader.OneOf("key1", vv));

  try
    {
      reader.CheckOneOf("key1", vv);
    }
  catch(string& e)
    {
      cout << e << endl;
    }
    

}
TEST(KeyValReader, GetAs) {

  KeyValReader reader(':');
  ifstream ifs("dummy.dat");
  reader.ReadFromStream(ifs);

  int n = reader.CheckGet<int>("key1");
  EXPECT_EQ(123, n);

  try
    {
      reader.CheckGet<int>("kkk34");
    }
  catch(string& e)
    {
      cout << e << endl;
    }

}



