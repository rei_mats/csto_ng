#include <gtest/gtest.h>

#include <src/utils/support_string.hpp>
#include <src/utils/support_qd.hpp>
#include <src/utils/key_val_reader.hpp>

TEST(sqrt, dd_complex)
{
  dd_complex z(1, -1);
  dd_complex sq  = sqrt(z);
  dd_complex ssq = sqrt(sq);
  dd_complex pow_sq_7 = pow(sqrt(z), 7);
  dd_complex sq_pow_7 = sqrt(pow(z, 7));

  EXPECT_DOUBLE_EQ(sq.real().x[0], 1.098684113467809966039801);
  EXPECT_DOUBLE_EQ(sq.imag().x[0],-0.455089860562227341304357);

  EXPECT_DOUBLE_EQ(ssq.real().x[0], 1.069553932363985802375679);
  EXPECT_DOUBLE_EQ(ssq.imag().x[0],-0.212747504726743035750713);

  cout << pow_sq_7 << endl;
  cout << sq_pow_7 << endl;

}
TEST(conversion, int)
{
  int i;
  EXPECT_TRUE(string_to<int>("123", &i));
  EXPECT_EQ(123, i);

  double eps = 0.0000001;
  dd_real x;
  EXPECT_TRUE(string_to<dd_real>("12.3", &x));
  EXPECT_TRUE(abs(x-12.3) < eps);
  EXPECT_TRUE(string_to<dd_real>("-12.3", &x));
  EXPECT_TRUE(abs(x+12.3) < eps);
  EXPECT_TRUE(string_to<dd_real>("+12.3", &x));
  EXPECT_TRUE(abs(x-12.3) < eps);

  EXPECT_FALSE(string_to<dd_real>("a3.21", &x));

  dd_complex y;
  EXPECT_TRUE(string_to<dd_complex>("1.2,3.1", &y));
  EXPECT_TRUE(abs(y-dd_complex("1.2", "3.1")) < eps);

  EXPECT_TRUE(string_to<dd_complex>("1.2,-3.1", &y));
  EXPECT_TRUE(abs(y-dd_complex("1.2", "-3.1")) < eps);

  EXPECT_TRUE(string_to<dd_complex>(" 1.2, -3.1", &y));
  EXPECT_TRUE(abs(y-dd_complex("1.2", "-3.1")) < eps);

  EXPECT_TRUE(string_to<dd_complex>(" 1.2", &y));
  EXPECT_TRUE(abs(y-dd_complex("1.2", "0")) < eps);

  EXPECT_TRUE(string_to<dd_complex>("1.5162e-01", &y));
  EXPECT_TRUE(abs(y-dd_complex("0.15162", "0")) < eps);

  EXPECT_FALSE(string_to<dd_complex>(" 1.2, -3.a1", &y));

}


