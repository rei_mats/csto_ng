#ifndef ARRAY_HPP
#define ARRAY_HPP


// ** max value

template<class RF>
RF max_abs_value(unsigned int num, RF* xs)
{
  RF x = abs(xs[0]);

  for(unsigned int i = 1; i < num; i++)
    if (x < abs(xs[i]))
      x = abs(xs[i]);

  return x;
}


// ** dot product

template<class F>
F dot_product(unsigned int num, F* a, F* b)
{
  F acc = value_zero<F>();
  for(unsigned int i = 0; i < num; i++)
    acc += a[i] * b[i];
  return acc;
}

// ** mat_vec

template<class F>
void mat_vec(unsigned int n, F* A, F* x, F* a)
{
  for(unsigned int i = 0; i < n; i++)
    {
      F acc = value_zero<F>();
      for(unsigned int j = 0; j < n; j++)
	{
	  acc += A[i + n * j] * x[j];
	}
      a[i] = acc;
    }
}

// ** scalr_vec

template<class F>
void scalar_vec(unsigned int n, F* a, F b)
{
  for(unsigned int i = 0; i < n; i++)
    a[i] *= b;
}

// ** copy

template<class F>
void copy_array(unsigned int n, F* a, F* b)
{
  for(unsigned int i = 0; i < n; i++)
    b[i] = a[i];
}


#endif

// ** set same value

template<class F>
void set_same_value(unsigned int n, F* a, F x)
{
  for(unsigned int i = 0; i < n; i++)
    a[i] = x;
}

// ** print vector

template<class F>
void print_vector_perpendicular(std::string title,
				std::ofstream& ofs,
				unsigned int n, F* a)
{
  ofs << title << std::endl;
  for(unsigned int i = 0; i < n; i++)
    ofs << a[i] << std::endl;
}

template<class F>
void print_vector_parallel(std::string title,
			   std::ofstream& ofs,
			   unsigned int n, F* a)

{
  ofs << title << " ";
  for(unsigned int i = 0; i < n; i++)
    ofs << a[i] << " ";
}

template<class F>
void print_matrix(std::string title,
		  std::ofstream& ofs,
		  unsigned int n, F* A)
{
  ofs << title << std::endl;
  for(unsigned int i = 0; i < n; i++)
    {
      for(unsigned int j = 0; j < n; j++)
	{
	  ofs << A[i + n * j] << " ";
	}
      ofs << std::endl;
    }
}



