// ** begin

#ifndef SUPPORT_STRING_HPP
#define SUPPORT_STRING_HPP

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using boost::algorithm::split;
using boost::algorithm::is_any_of;
using boost::algorithm::trim_copy;
using boost::lexical_cast;


// ** is integer

bool is_integer(const string& str)
{
  bool acc = true;
  for(string::const_iterator it = str.begin();
      it != str.end(); ++it)
    acc = acc & isdigit(*it);
  
  return acc;
}


// ** is_number

bool is_number(const string& str)
{
  bool acc = true;
  for(string::const_iterator it = str.begin();
      it != str.end(); ++it)
    {
      char c = *it;
      acc = acc & (isdigit(c) ||c=='-'||c=='+'||c=='.'||c=='e');
    }
  return acc;
}



// ** string to data

template<class T>
bool string_to(const string& str, T* t)
{
  try
    {
      *t = lexical_cast<T>(str);
    }
  catch(std::exception& e)
    {
      return false;
    }
  return true;
}


// ** data to string

template<class T>
const string to_string(const T& t)
{
  return t.to_string();
}


// ** end

#endif
