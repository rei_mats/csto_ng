// *  Begin      

#ifndef SUPPORT_QD_HPP
#define SUPPORT_QD_HPP

#include <qd/dd_real.h>
#include <dd_complex.h>
#include <string>

#include <string>
#include <vector>

using std::string;


// *  function   
// ** exp for dd complex

  inline dd_complex exp(dd_complex z) {
    dd_real zr, zi, exp_zr;
    dd_complex exp_z;
    zr = z.real();
    zi = z.imag();
    exp_zr = exp(zr);
    exp_z.real() = exp_zr * cos(zi);
    exp_z.imag() = exp_zr * sin(zi);
    return exp_z;
  }

// ** pow for dd complex

inline dd_complex pow(dd_complex z, int n) {

  dd_complex y = z;
  unsigned int nn = (unsigned int)n;
    for(unsigned int i = 1; i < nn; i++) 
    y *= z;

  return y;
}

// ** pow for double

/*
inline double pow(double x, int n) {
  double y = x;
  unsigned int nn = (unsigned int)n;
  for(unsigned int i = 1; i < nn; i++)
    y *= x;
  return y;
}
*/

// ** pow for dd_complex

inline dd_complex pow_dd_complex(dd_complex z, int n)
{
  dd_complex one(1, 0);
  if(n == 0)
    return one;
  else if(n > 0)
    return pow(z, n);
  else
    return one/pow(z, -n);
}


// ** real part and imaginary part

double take_real_part(std::complex<double> z) { return z.real(); }
double take_real_part(double x) { return x; }
dd_real take_real_part(dd_complex z) { return z.real(); }
dd_real take_real_part(dd_real x) { return x; }

double take_imag_part(std::complex<double> z) { return z.imag(); }
double take_imag_part(double x) { return 0.0; }
dd_real take_imag_part(dd_complex z) { return z.imag(); }
dd_real take_imag_part(dd_real x) { return dd_real(0); }



// *  conversion 
// ** stream

std::ostream& operator<<(std::ostream& os, const dd_complex& z)
{
  os << "(" << z.real() << ", " << z.imag() << ")";
  return os;
}

// ** string to
// *** dd_real

template<>
bool string_to<dd_real>(const string& str, dd_real* x)
{

  if(is_number(str))
    {
      *x = str.c_str();
      return true;
    }
  else
    {
      return false;
    }
}

// *** dd_complex

template<>
bool string_to<dd_complex>(const string& str, dd_complex* x)
{

  if(string::npos == str.find(",", 0))
    {
      // str do not contain ",".
      
      string re = trim_copy(str);
      if(! is_number(re))
	return false;

      *x = dd_complex(re.c_str(), "0");
      return true;
    }
  else
    {
      // str contain ",".
      
      vector<string> str_vec;
      split(str_vec, str, is_any_of(","));

      if(str_vec.size() != 2)
	return false;

      string re = trim_copy(str_vec[0]);
      string im = trim_copy(str_vec[1]);

      if(!is_number(re) || !is_number(im))
	return false;

      *x = dd_complex(re.c_str(), im.c_str());
      return true;
    }
}


// ** to_string

template<>
const string to_string<dd_complex>(const dd_complex& z)
{
  dd_real x = z.real();
  dd_real y = z.imag();

  string str = x.to_string() + "," + y.to_string();
  return str;
}


// *  basic algebra 

dd_complex operator+(const dd_complex& a, int i)
{
  dd_real j(i);
  return a + j;
}
dd_complex operator+(int i, const dd_complex& a)
{
  return a + i;
}

dd_real operator+(const dd_real& a,  int i)
{
  dd_real j(i);
  return a + j;
}
dd_real operator+(int i, const dd_real& a)
{
  return a + i;
}

dd_complex operator+(double d, const dd_complex& a)
{
  dd_complex b(a.real() + d, a.imag());
  return b;
}
dd_complex operator+(const dd_complex& a, double d)
{
  return d + a;
}

// *  Constant   
// ** one

template<class F> F value_one() {}
template<> int value_one() {return 1;}
template<> double value_one() {return 1.0;}
template<> dd_real value_one() {
  dd_real one = 1;
  return one;
}
template<> dd_complex value_one() {
  dd_complex one = dd_complex(1, 0);
  return one;
}

// ** zero
  
template<class F> F value_zero() {}
template<> int value_zero() {return 0;}
template<> double value_zero() {return 0.0;}
template<> dd_real value_zero() {
  dd_real zero = 0;
  return zero;
}
template<> dd_complex value_zero() {
  dd_complex zero;
  dd_real r_zero = value_zero<dd_real>();
  zero.real() = r_zero;
  zero.imag() = r_zero;
  return zero;
}

// ** pi
template<class F> F value_pi (){
  return 1;}
template<> double value_pi() {return M_PI;}
template<> dd_real value_pi() {
  dd_real one = value_one<dd_real>();
  dd_real four =4;
  dd_real pi = four * atan(one);
  return pi;
}
template<> dd_complex value_pi() {
  dd_real pi = value_pi<dd_real>();
  return pi;
}

// ** machine epsilon
template<class F> double value_machine_eps() {
  return 1.0;}
template<> double value_machine_eps<double>() {
  double eps = 2.22 * pow(10.0, -16.0);
  return eps;
}
template<> double value_machine_eps<dd_real>() {
  double eps = 2.46519 * pow(10.0, -32.0);
  return eps;
}
template<> double value_machine_eps<dd_complex>() {
  double eps = value_machine_eps<dd_real>();
  return eps;
}


#endif
