// *  Begin   
// ** comment

/**
 *  read file and store its information as string type.
 */



// ** ifndef

#ifndef SIMPLE_READER_HPP
#define SIMPLE_READER_HPP

// ** include

#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

// ** using

using namespace std;
using boost::algorithm::split;
using boost::algorithm::is_any_of;
using boost::algorithm::trim_copy;
using boost::lexical_cast;


// *  Utils   
// ** split string

/**
 *  split str with sep.
 *  "abc:key:xxx" -> ["abc", "key", "xxx"]
 */
vector<string> split_string(const string& str, char sep)
{
  vector<string> v;
  string::const_iterator first = str.begin();

  
  while(first != str.end())
    {
      string::const_iterator last = first;
      while(last != str.end() && *last != sep)
	++last;

      v.push_back(string(first, last));

      if(last != str.end())
	++last;

      first = last;
      
    }

  return v;
}


// ** remove char

/**
 * remove specific char from string
 */
string remove_char(const string& str, char sep)
{

  string res;
  string::const_iterator it = str.begin();

  while(it != str.end())
    {
      if(*it != sep)
	res.push_back(*it);
      it++;
    }

  return res;
}

// ** remove \n and blank

string remove_blank(const string& str)
{
  return remove_char(remove_char(str, ' '), '\n');
}

       
// *  class   
// ** begin

class KeyValReader
{

// ** field

private:

  std::map<std::string, std::string> dict_;
  char sep_;


// ** constructor

public:
  KeyValReader(char _sep): sep_(_sep) {}
  KeyValReader(char _sep, ifstream& _ifs ): sep_(_sep)
  {
    ReadFromStream(_ifs);
  }

// ** safty
// *** get

  const string Get(const string& key)  {
    return dict_[key];
  }

// *** exist

  bool Exist(string key)
  {
    return dict_.find(key) != dict_.end();
  }

// *** one of them

  bool OneOf(const string& key, const vector<string>& vec)
  {
    string val = Get(key);
    for(vector<string>::const_iterator it = vec.begin();
	it != vec.end(); ++it)
      {
	if(val == *it)
	  return true;
      }

    return false;
  }

// ** non-safe
// *** read file

public:
  void ReadFromStream(ifstream& ifs) {

    if(ifs.fail())
      {
	string msg = "Error: KeyValReader::ReadFromStream";
	msg += "\n";
	msg += "failed to open file\n";
	throw msg;
      }

    string line;
    while(getline(ifs, line))
      {
	if(line == "")
	  continue;
	
	vector<string> str_vec;
	split(str_vec, line, is_any_of(":"));

	if(str_vec.size() != 2)
	  {
	    string msg;
	    msg = "Error: KeyValReader::ReadFromStream\n";
	    msg += "input file must be constructed from";
	    msg += " key: value style.\n";
	    msg += "error line is : " + line + "\n";
	    throw msg;
	  }

	string key = trim_copy(str_vec[0]);
	string val = trim_copy(str_vec[1]);

	dict_[key] = val;
	
      }
  }

// *** check exist

  void CheckExist(string k)
  {
    if(!Exist(k))
      {
	string msg = "Error: KeyValReader::CheckExist";
	msg += "\n";
	msg += "key:" + k  + " is not exist.\n";
	throw msg;
      }
  }
  
// *** Check and Get

public:

  template<class T>
  const T CheckGet(const string& key)
  {
    CheckExist(key);
    
    T t;
    bool b = string_to<T>(Get(key), &t);

    if(!b)
      {
	string msg;
	msg  = "Error: KeyValReader::CheckGet. ";
	msg += "failed to convert to type T=";
	msg += typeid (T).name();
	msg += "\nkey = " + key + "\n";
	msg += "val = " + Get(key) + "\n";
	throw msg;
      }
    
    return t;
  }

// *** check one of them

public:
  void CheckOneOf(const string& k, const vector<string>& v)
  {
    if(!OneOf(k, v))
      {
	string msg;
	msg += "Error: KeyValReader::CheckOneOf. ";
	msg += "the value correspond to the key is not in ";
	msg += "the vector.\n";
	msg += "key = " + k + "\n";
	msg += "val = " + Get(k) + "\n";
      }
  }
  
// *** valid data (should not use)


  bool IsNumberData(string key)
  {
    return Exist(key) && is_number(dict_[key]);
  }

  bool IsIntegerData(string key)
  {
    return Exist(key) && is_integer(Get(key));
  }


  void CheckIsNumberData(string k)
  {
    CheckExist(k);
    
    if(!IsNumberData(k))
      {
	cerr << "key:" << k << " is not number." << endl;
	exit(1);
      }
  }

  void CheckIsIntegerData(string k)
  {
    CheckExist(k);

    if(!IsIntegerData(k))
      {
	cerr << "key:" << k << " is not integer" << endl;
	exit(1);
      }
  }
  
  
// *** display

public:
  void Display()
  {

    for(map<string, string>::iterator it = dict_.begin();
	it != dict_.end();
	++it)
      {
	cout << it->first << ": " << it->second << endl;
      }

  }
  
// ** end

};



// *  End     

#endif
