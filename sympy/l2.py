


from sympy import *
init_printing(use_unicode=False, wrap_line=False,no_global=True)
r=Symbol('r')

# complex inner product
def cip(f, g):
    return integrate(f*g,(r,0,oo))

# complex norm
def cnorm2(f):
    return cip(f, f)

# normalize function
def normalize(f):
    return f/sqrt(cnorm2(f))

def n_normalize(f):
    return f*N(1/sqrt(cnorm2(f)))

def n_normalizedSTO(n, z):
    return n_normalize(r**n*exp(-z*r))
    
def n_normalizedGTO(n, z):
    return n_normalize(r**n*exp(-z*r*r))

# normalized STO
def normalizedSTO(n, z):
    return normalize(r**n*exp(-z*r))

# normalized GTO
def normalizedGTO(n, z):
    return normalize(r**n*exp(-z*r*r))
