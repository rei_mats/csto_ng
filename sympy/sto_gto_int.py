from l2 import *

prec = 100


sto2 =  r*r*exp(-r*Rational(11, 10))
gto2 =  r*r*exp(-r*r*Rational(13, 10))
rip_s2_g2 = N(cip(sto2, gto2), prec)

f = open("result/ref_sto_gto.h", 'w')

f.write("""

// produced by sym/sto_gto_int.py

char* rip_s2_g2 = (char*)"%s";

""" % (str(rip_s2_g2)))

f.close()






