# compute exp(z^2)erfc(z) for complex z number.

from sympy import *


def Exp2_Erfc(z) :
    return exp(z * z) * erfc(z)
    
def PrintComplex(f, z) :
    f.write("(char*)\"" + str(re(z)) + "\",\n(char*)\"" + str(im(z)) + "\"")

    
# init
CF = ComplexField(200)
f = open('result/ref_complex200_exp2_erfc.h', 'w')

# print
f.write("char* z_w_exp2_erf2[] = {\n")

z = CF(1.2, -0.2)
w = Exp2_Erfc(z)

PrintComplex(f, z)
f.write(",\n")
PrintComplex(f, w)

f.write("};\n")
f.write("unsigned int num_z_w_exp2_erf2 = " + str(3) + "\n")
f.close()

