from l2 import *

r31=Rational(31, 10)
r21=Rational(21, 10)
r11=Rational(11, 10)
r12=Rational(12, 10)
r13=Rational(13, 10)
r23=Rational(23, 10)

prec = 100

sto1 = normalizedSTO(2, r11)
sto2 = normalizedSTO(2, r13)
gto1 = normalizedGTO(2, r13)
gto2 = normalizedGTO(2, r23)
cip_sto_gto = N(cip(sto1, gto1), prec)
cip_sto_sto = N(cip(sto1, sto2), prec)
cip_gto_gto = N(cip(gto1, gto2), prec)

sto1 = normalizedSTO(2, r11)
stong= r21 * normalizedGTO(2, r12) + \
       r31 * normalizedGTO(2, r13) + \
       r31 * normalizedGTO(2, r11)
cip_r = N(cip(sto1,stong), prec)

CF=ComplexField(200)
sto1 = normalizedSTO(2, r11)
stong= r21 * normalizedGTO(2, r12) + \
       (r31 + I*Rational(3,2))*normalizedGTO(2,r13)
cip_c = N(cip(sto1,stong), prec)

f = open("result/ref_stong.h", "w");
f.write("""

// produced by sympy/stong_int.py script.
// calculation is
// SG = 21/10 G_2(12/10) + 31/10 G_2(13/10) + (31/10+3/2I)G_2(13/10)
// (S_2(11/10), SG) etc.

char* sto_gto_r  = (char*)"%s";
char* sto_sto_r  = (char*)"%s";
char* gto_gto_r  = (char*)"%s";
char* stong_r    = (char*)"%s";
char* stong_c_re = (char*)"%s";
char* stong_c_im = (char*)"%s";

""" % (str(cip_sto_gto), str(cip_sto_sto),str(cip_gto_gto),str(cip_r), str(re(cip_c)), str(im(cip_c))))
f.close()
